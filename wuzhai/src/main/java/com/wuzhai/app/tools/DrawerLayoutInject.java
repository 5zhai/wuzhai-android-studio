package com.wuzhai.app.tools;

import com.wuzhai.app.R;
import com.wuzhai.app.main.widget.NavigationMenuView;
import android.app.Activity;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;

public class DrawerLayoutInject {

	public static void setupDrawer(Activity activity,Toolbar toolbar) {
	    NavigationMenuView menuView = new NavigationMenuView(activity);
//	    menuView.setOnHeaderClickListener(activity);
	  
	    DrawerLayout drawerLayout = DrawerLayoutInstaller.from(activity)
	            .drawerRoot(R.layout.navigation_menu_layout)
	            .drawerLeftView(menuView)
	            .drawerLeftWidth(300)//Utils.dpToPx(300)
	            .withNavigationIconToggler(toolbar)
	            .build();
	    
//	    addDrawerToActivity(drawerLayout,activity);
	    
	}
	
	private static void addDrawerToActivity(DrawerLayout drawerLayout,Activity activity) {
	    ViewGroup rootView = (ViewGroup) activity.findViewById(android.R.id.content);
	    ViewGroup drawerContentRoot = (ViewGroup) drawerLayout.getChildAt(0);
	    View contentView = rootView.getChildAt(0);
	  
	    rootView.removeView(contentView);
	  
	    drawerContentRoot.addView(contentView, new ViewGroup.LayoutParams(
	            ViewGroup.LayoutParams.MATCH_PARENT,
	            ViewGroup.LayoutParams.MATCH_PARENT
	    ));
	  
	    rootView.addView(drawerLayout, new ViewGroup.LayoutParams(
	            ViewGroup.LayoutParams.MATCH_PARENT,
	            ViewGroup.LayoutParams.MATCH_PARENT
	    ));
	}
}
