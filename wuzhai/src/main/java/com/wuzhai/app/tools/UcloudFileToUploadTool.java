package com.wuzhai.app.tools;

import java.io.File;
import org.json.JSONObject;
import cn.ucloud.ufilesdk.Callback;
import cn.ucloud.ufilesdk.UFileRequest;
import cn.ucloud.ufilesdk.UFileSDK;
import cn.ucloud.ufilesdk.UFileUtils;
import cn.ucloud.ufilesdk.task.HttpAsyncTask;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

public class UcloudFileToUploadTool {

	// private static final String publicKey =
	// "Dvg+O47vDLy+E2ybLHLW6x2g6JF18n0RGh2NXEGM2DKpU7Kq2OCZbA==";
	private static final String privatekey = "23d39e7bacccdd265a915322509d6a5e800e46ba";

	private static final String bucket = "cdv";
	private static final String proxySuffix = ".ufile.ucloud.cn";

	private static final String TAG = "yue.huang";
	private Context mContext;
	private UFileSDK uFileSDK;
	private ProgressDialog progressDialog;

	public UcloudFileToUploadTool(Context context){
		mContext = context;
        progressDialog = new ProgressDialog(context);
        uFileSDK = new UFileSDK(bucket, proxySuffix);
	}

    public void putFile(String filePath,String authorization,final Handler handler,final int what) {
    	final File targetFile = new File(filePath);
        String http_method = "PUT";
        String content_md5 = UFileUtils.getFileMD5(targetFile);
        String content_type = "text/plain";
        String date = "";
        final String key_name = targetFile.getName();

//        String authorization = getAuthorization(http_method, content_md5, content_type, date, bucket, key_name);

        final UFileRequest request = new UFileRequest();
        request.setHttpMethod(http_method);
        request.setAuthorization(authorization);
        request.setContentMD5(content_md5);
        request.setContentType(content_type);

        final ProgressDialog dialog = new ProgressDialog(mContext);
        final HttpAsyncTask httpAsyncTask = uFileSDK.putFile(request, targetFile, key_name, new Callback() {
            @Override
            public void onSuccess(JSONObject response) {
                Log.i(TAG, "onSuccess " + response);
                dialog.dismiss();
//                showDialog("success " + response.toString());
                String videoUrl = "http://cdv.ufile.ucloud.cn/"+key_name;
                String screenShot = videoUrl.replace(videoUrl.substring(videoUrl.lastIndexOf(".")), ".jpg");
                Message msg = Message.obtain();
                Bundle bundle = new Bundle();
                bundle.putString("videoUrl", videoUrl);
                bundle.putString("screenShot", screenShot);
                msg.setData(bundle);
                msg.what = what;
                handler.sendMessage(msg);
            }

            @Override
            public void onProcess(long len) {
                int value = (int) (len * 100 / targetFile.length());
                dialog.setProgress(value);
            }

            @Override
            public void onFail(JSONObject response) {
                Log.i(TAG, "onFail " + response);
                dialog.dismiss();
                showDialog(response.toString());
            }
        });

        dialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setMax(100);
        dialog.setTitle("上传视频到ucloud");
        dialog.setButton(DialogInterface.BUTTON_NEUTRAL, "取消", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                httpAsyncTask.cancel();
                dialog.dismiss();
            }
        });
        dialog.show();
    }

//    private String getAuthorization(String http_method, String content_md5, String content_type, String date, String bucket, String key) {
//        String signature = "";
//        try {
//            String strToSign = http_method + "\n" + content_md5 + "\n" + content_type + "\n" + date + "\n" + "/" + bucket + "/" + key;
//            byte[] hmac = UFileUtils.hmacSha1(privatekey, strToSign);
//            signature = Base64.encodeToString(hmac, Base64.DEFAULT);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        String auth = "UCloud" + " " + publicKey + ":" + signature;
//        Log.e(TAG, "getAuthorization " + auth);
//        return auth;
//    }

    private void showDialog(String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setTitle("提示");
        builder.setMessage(message);
        builder.setPositiveButton("确认", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.create().show();
    }

	public String getStrToSign(String filePath) {
		final File targetFile = new File(filePath);
		String http_method = "PUT";
		String content_md5 = UFileUtils.getFileMD5(targetFile);
		String content_type = "text/plain";
		String date = "";
		String key_name = targetFile.getName();
		String strToSign = http_method + "\n" + content_md5 + "\n"+ content_type + "\n" + date + "\n" + "/" + bucket + "/"+ key_name;
		return strToSign;
	}

//    public void createSnapShotTask(){
//
//    	String path = "http://api.ucloud.cn/?Action=CreateSnapShotTask&SrcUrl=http://test.ufile.ucloud.com/myvideo.flv&ShotTime=10&ImageName=my.jpg&DestBucket=test" +
//    			"&Signature=sy+0KEtfDjylZWfN2bY1dliuBfI=";
//		Log.d("yue.huang", "location path:"+path);
//		String authorizationString = getAuthorization("GET", "", "", "", "", "");
//		HttpURLConnection connection = null;
//
//
//
//
//		try {
//
//	        String strToSign = "ActionCreateSnapShotTaskDestBucketcdvImageNameVID20160312145937.jpgPublicKeyDvg+O47vDLy+E2ybLHLW6x2g6JF18n0RGh2NXEGM2DKpU7Kq2OCZbA==ShotTime1SrcUrlhttp://cdv.ufile.ucloud.cn/VID20160312145937.mp423d39e7bacccdd265a915322509d6a5e800e46ba";
//	        byte[] hmac = UFileUtils.hmacSha1(privatekey, strToSign);
//	        String signature = Base64.encodeToString(hmac, Base64.DEFAULT);
//	        Log.d("yue.huang", "signaturesignature:"+signature);
//
//			URL url = new URL(path);
//			connection = (HttpURLConnection)url.openConnection();
////			connection = Utils.getHttpsConnection(url);
//			connection.setConnectTimeout(5000);
//			connection.setRequestMethod("GET");
////			connection.setUseCaches(false);
////			connection.setDoOutput(true);
////			connection.setDoInput(fa);
////			connection.setRequestProperty("UserAgent", "UFile Android/" + UFileSDK.VERSION_NAME);
//			connection.setRequestProperty("User-Agent","Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.31 (KHTML, like Gecko) Chrome/26.0.1410.43 Safari/537.31"); 
//			connection.setRequestProperty("Content-Type", ""); 
//			connection.setRequestProperty("Content-MD5", "");
//			connection.setRequestProperty("Authorization", "UCloud 1ZjyNm38EjFoq6XILiwYcTAJy+Y8UxCnWm1o6j/bjTrpHKoCkI1B1A==:sy+0KEtfDjylZWfN2bY1dliuBfI=");
//			Log.d("yue.huang", "getResponseCode:"+connection.getResponseCode());
//			if(connection.getResponseCode() == 200){
//                InputStream inputStream = connection.getInputStream();
//                byte[] bytes = Utils.readStream(inputStream);
//                String jsonString = new String(bytes);
//                JSONObject resultJson = new JSONObject(jsonString);
//                Log.d(TAG, "添加截图任务:"+resultJson.toString());
//			}
//		} catch (Exception e) {
//			Log.d(TAG, "createSnapShotTask:"+e.toString());
//			e.printStackTrace();
//		}finally{
//			if(null!=connection){
//				connection.disconnect();
//			}
//		}
////        String http_method = "GET";
////        String content_md5 = "";
////        String content_type = "text/plain";
////        String date = "";
////        String key_name = "";
////
////        String authorization = getAuthorization(http_method, content_md5, content_type, date, bucket, key_name);
////
////        final UFileRequest request = new UFileRequest();
////        request.setHttpMethod(http_method);
////        request.setAuthorization(authorization);
////        request.setContentMD5(content_md5);
////        request.setContentType(content_type);
////		HttpAsyncTask task = new HttpAsyncTask(path, request, new HttpCallback() {
////
////			@Override
////			public void onProgressUpdate(Object... progress) {
////				// TODO Auto-generated method stub
////
////			}
////
////			@Override
////			public void onPostExecute(JSONObject response) {
////				// TODO Auto-generated method stub
////				Log.d("yue.huang", "responseresponseresponse:"+request.toString());
////			}
////		});
////		task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
//    }

}
