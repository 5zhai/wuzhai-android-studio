package com.wuzhai.app.push;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.wuzhai.app.R;
import com.wuzhai.app.widget.TitleToolbarActivity;

/**
 * Created by huangyue on 2016/10/28.
 * 推送消息(不是聊天消息)点击后打开的activity
 */
public class PushMessageActivity extends TitleToolbarActivity{
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pushmessage);
    }
}
