package com.wuzhai.app.login;

import cn.sharesdk.sina.weibo.SinaWeibo;
import cn.sharesdk.tencent.qq.QQ;
import cn.sharesdk.wechat.friends.Wechat;
import com.wuzhai.app.R;
import com.wuzhai.app.application.WuZhaiApplication;
import com.wuzhai.app.main.MainActivity;
import com.wuzhai.app.main.WuzhaiService;
import com.wuzhai.app.main.WuzhaiServiceCallbackAdapter;
import com.wuzhai.app.register.RegisterActivity;
import com.wuzhai.app.tools.Utils;
import com.wuzhai.app.widget.TitleToolbarActivity;
import android.os.Bundle;
import android.content.Intent;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class LoginActivity extends TitleToolbarActivity implements OnClickListener{

	private EditText usernameEditText;
	private EditText passwordEditText;
	private Button loginButton;
	private Button weibo_loginButton;
	private Button wechat_loginButton;
	private Button douban_loginButton;
	private TextView forgetpasswordTextView;
	private LinearLayout progressView;
	private ImageButton enableSwitcher;
	private String TAG = "LoginActivity";
	private boolean isPasswordEnable = false;
	private ImageView bigIcon;
	private LinearLayout toolbarBack;

	private WuzhaiService service;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
        setTitle("登录");
		setTextMenuString("注册");
		setTextMenuClickListener(this);
		setContentView(R.layout.activity_login);
		toolbarBack = (LinearLayout)findViewById(R.id.toolbar_back);
		toolbarBack.setVisibility(View.GONE);
		usernameEditText = (EditText) findViewById(R.id.username_edit);
		passwordEditText = (EditText) findViewById(R.id.password_edit);
		loginButton = (Button) findViewById(R.id.login_btn);
		weibo_loginButton = (Button) findViewById(R.id.weibo_login_btn);
		wechat_loginButton = (Button) findViewById(R.id.wechat_login_btn);
		douban_loginButton = (Button) findViewById(R.id.douban_login_btn);
		forgetpasswordTextView = (TextView) findViewById(R.id.forgetpassword);
		progressView = (LinearLayout)findViewById(R.id.progressView);
		enableSwitcher = (ImageButton)findViewById(R.id.enable_switcher);
		bigIcon = (ImageView)findViewById(R.id.big_icon);

		usernameEditText.setOnFocusChangeListener(focusChangeListener);
		passwordEditText.setOnFocusChangeListener(focusChangeListener);
		loginButton.setOnClickListener(this);
		weibo_loginButton.setOnClickListener(this);
		wechat_loginButton.setOnClickListener(this);
		douban_loginButton.setOnClickListener(this);
		forgetpasswordTextView.setOnClickListener(this);
		enableSwitcher.setOnClickListener(this);

        service = ((WuZhaiApplication)getApplication()).getService();
        service.setCallBack(callbackAdapter);
	}

	@Override
	protected void onStart() {
		super.onStart();
	}

    @Override
    protected void onResume() {
        super.onResume();
        doSomethingBeforeLoginCompleted();
    }

	@Override
	protected void onDestroy() {
		super.onDestroy();
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.login_btn: login();break;
		case R.id.weibo_login_btn: thirdPartyLogin(SinaWeibo.NAME);break;
		case R.id.wechat_login_btn: thirdPartyLogin(Wechat.NAME);break;
		case R.id.douban_login_btn: thirdPartyLogin(QQ.NAME);break;
		case R.id.forgetpassword:findPassword();break;
		case R.id.toolbar_text_menu:goToRegister();break;
		case R.id.enable_switcher:switchPasswordEnable();break;
		}
	}
	
	private void login(){
		String username = usernameEditText.getText().toString();
		String password = passwordEditText.getText().toString();
		if("".equals(username) || "".equals(password)){
			Toast.makeText(this, getString(R.string.login_info_not_complete), Toast.LENGTH_SHORT).show();
			return;
		}
		if(!Utils.isNetworkAvailable(this)){
			Toast.makeText(this, getString(R.string.no_network), Toast.LENGTH_SHORT).show();
			return;
		}
		doSomethingBeforeLogin();
		service.login(username, password);
	}
//----------第三方登录相关----------------	
	private void thirdPartyLogin(String platformName){
        doSomethingBeforeLogin();
		service.thirdPartyLogin(platformName);
	}

    private WuzhaiServiceCallbackAdapter callbackAdapter = new WuzhaiServiceCallbackAdapter() {
        @Override
        public void onCompleted(int result) {
            doSomethingBeforeLoginCompleted();
            switch (result) {
                case WuzhaiService.LOGIN_SUCCESSFUL:
                    /*登录成功后保存accesskey*/
                    Utils.saveAccessKeyString(LoginActivity.this, ((WuZhaiApplication) getApplication()).getUser().getAccessKey());
                    startActivity(new Intent(LoginActivity.this, MainActivity.class));
                    LoginActivity.this.finish();
                    break;
                case WuzhaiService.ACCOUND_ERR:
                    Toast.makeText(LoginActivity.this, getString(R.string.username_password_err),
                            Toast.LENGTH_LONG).show();
                    break;
                case WuzhaiService.NETWORK_ERR:
                    Toast.makeText(LoginActivity.this, getString(R.string.network_err),
                            Toast.LENGTH_LONG).show();
                    break;
            }
        }
    };
	
	private void doSomethingBeforeLogin(){
		progressView.setVisibility(View.VISIBLE);
		usernameEditText.setCursorVisible(false);
		passwordEditText.setCursorVisible(false);
//		InputMethodManager imm = (InputMethodManager)getSystemService(INPUT_METHOD_SERVICE);
//		imm.toggleSoftInput(0, InputMethodManager.HIDE_NOT_ALWAYS);
	}
	private void doSomethingBeforeLoginCompleted(){
		usernameEditText.setCursorVisible(true);
		passwordEditText.setCursorVisible(true);
		progressView.setVisibility(View.GONE);
	}
	
	private void goToRegister(){
		startActivity(new Intent(this,RegisterActivity.class));
	}

	private void findPassword(){
		startActivity(new Intent(this,FindPasswordActivity.class));
	}

	private void switchPasswordEnable(){
		if(isPasswordEnable){
			passwordEditText.setTransformationMethod(PasswordTransformationMethod.getInstance());
			isPasswordEnable = false;
			enableSwitcher.setBackgroundResource(R.drawable.icon_ciphertext);
		}else {
			passwordEditText.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
			isPasswordEnable = true;
			enableSwitcher.setBackgroundResource(R.drawable.icon_plaintext);
		}
	}

	private OnFocusChangeListener focusChangeListener = new OnFocusChangeListener() {

		@Override
		public void onFocusChange(View v, boolean hasFocus) {
			if(v.getId() == R.id.username_edit && hasFocus){
				bigIcon.setImageResource(R.drawable.login_bg1);
			}else {
				bigIcon.setImageResource(R.drawable.login_bg2);
			}
		}
	};
}
