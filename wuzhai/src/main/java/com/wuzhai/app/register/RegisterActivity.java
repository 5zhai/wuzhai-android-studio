package com.wuzhai.app.register;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import com.wuzhai.app.R;
import com.wuzhai.app.application.WuZhaiApplication;
import com.wuzhai.app.login.LoginActivity;
import com.wuzhai.app.main.WuzhaiService;
import com.wuzhai.app.main.WuzhaiServiceCallbackAdapter;
import com.wuzhai.app.tools.Utils;
import com.wuzhai.app.widget.TitleToolbarActivity;

public class RegisterActivity extends TitleToolbarActivity implements OnClickListener{
	private WuzhaiService service;
	private EditText phoneNumEditText;
	private EditText verCodeEditText;
	private Button getVerCodeBtn;
	private EditText passwordEditText;
	private EditText passwordConfirmEditText;
	private Button registerBtn;
	private Handler handler;

	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setTitle("注册");
		setContentView(R.layout.activity_register);
		initView();
        service = ((WuZhaiApplication)getApplication()).getService();
        service.setCallBack(callbackAdapter);
		handler = new Handler(){
			@Override
			public void handleMessage(Message msg) {
				if(msg.what!=0){
                    getVerCodeBtn.setEnabled(false);
					getVerCodeBtn.setText(""+msg.what);
				}else {
                    getVerCodeBtn.setEnabled(true);
					getVerCodeBtn.setText("发送验证码");
				}
			}
		};
	}

	private void initView(){
		phoneNumEditText = (EditText)findViewById(R.id.phone_number);
		verCodeEditText = (EditText)findViewById(R.id.verification_code);
		passwordEditText = (EditText)findViewById(R.id.password);
		passwordConfirmEditText = (EditText)findViewById(R.id.password_confirm);
		getVerCodeBtn = (Button)findViewById(R.id.get_verification_code);
		registerBtn = (Button)findViewById(R.id.register);
		getVerCodeBtn.setOnClickListener(this);
		registerBtn.setOnClickListener(this);
	}

    private WuzhaiServiceCallbackAdapter callbackAdapter = new WuzhaiServiceCallbackAdapter(){
        @Override
        public void onCompleted(int result) {
            switch (result) {
                case WuzhaiService.VERIFICATION_CODE_ERR:
                    Toast.makeText(RegisterActivity.this, getString(R.string.verification_code_err), Toast.LENGTH_SHORT).show();
                    break;
                case WuzhaiService.NETWORK_ERR:
                    Toast.makeText(RegisterActivity.this, getString(R.string.network_err), Toast.LENGTH_SHORT).show();
                    break;
                case WuzhaiService.REGISTER_SUCCESSFUL:
                    Toast.makeText(RegisterActivity.this, getString(R.string.register_successful), Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(RegisterActivity.this, LoginActivity.class));
                    break;
            }
        }

        @Override
        public void onGetVerificationCodeDone(boolean isSucc, String msg) {
            if(isSucc){
                startCountdown();
                Toast.makeText(RegisterActivity.this, "验证码发送成功，请注意查收", Toast.LENGTH_SHORT).show();
            }else {
                Toast.makeText(RegisterActivity.this, msg, Toast.LENGTH_SHORT).show();
            }
        }
    };

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.get_verification_code:
			getVerificationCode();
			break;

		case R.id.register:
			gotoRegister();
			break;
		}
	}

	private void getVerificationCode(){
		String phoneNum = phoneNumEditText.getText().toString();
		if(TextUtils.isEmpty(phoneNum)){
			Toast.makeText(this, "请输入电话号码", Toast.LENGTH_SHORT).show();
			return;
		}
		if(!Utils.isPhoneNumber(phoneNum)){
			Toast.makeText(this, "请输入正确的电话号码", Toast.LENGTH_SHORT).show();
			return;
		}
		if(!Utils.isNetworkAvailable(this)){
			Toast.makeText(this, "无网络连接", Toast.LENGTH_SHORT).show();
			return;
		}
		service.getVerificationCode(WuzhaiService.CODE_TYPE_RES,phoneNum);
	}

	private void gotoRegister(){
		String phoneNum = phoneNumEditText.getText().toString();
		String verCode = verCodeEditText.getText().toString();
		String password = passwordEditText.getText().toString();
		String passwordConfirm = passwordConfirmEditText.getText().toString();

		if(TextUtils.isEmpty(phoneNum) || TextUtils.isEmpty(verCode) ||
				TextUtils.isEmpty(password) || TextUtils.isEmpty(passwordConfirm)){
			Toast.makeText(this, "信息不完整！", Toast.LENGTH_SHORT).show();
			return;
		}
		if(!password.equals(passwordConfirm)){
			Toast.makeText(this, "密码不一致，请重新输入", Toast.LENGTH_SHORT).show();
			return;
		}
		if(!Utils.isNetworkAvailable(this)){
			Toast.makeText(this, "无网络连接", Toast.LENGTH_SHORT).show();
			return;
		}

		service.gotoRegister(phoneNum, verCode, password, passwordConfirm);
	}

	private void startCountdown(){
		new Thread(new Runnable() {
			@Override
			public void run() {
				for(int n=30;n>=0;n--){
					try {
						handler.sendEmptyMessage(n);
						Thread.sleep(1000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
		}).start();
	}
}
