package com.wuzhai.app.objects;

import java.util.ArrayList;

public class UserCenterInfo {

	private User user;
	private int collectionsCount;
	private int commoditiesCount;
	private int photosCount;
	private ArrayList<Photo> photoList;
	private int videosCount;
	private ArrayList<Video> videoList;

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public int getCollectionsCount() {
		return collectionsCount;
	}

	public void setCollectionsCount(int collectionsCount) {
		this.collectionsCount = collectionsCount;
	}

	public int getCommoditiesCount() {
		return commoditiesCount;
	}

	public void setCommoditiesCount(int commoditiesCount) {
		this.commoditiesCount = commoditiesCount;
	}

	public int getPhotosCount() {
		return photosCount;
	}

	public void setPhotosCount(int photosCount) {
		this.photosCount = photosCount;
	}

	public ArrayList<Photo> getPhotoList() {
		return photoList;
	}

	public void setPhotoList(ArrayList<Photo> photoList) {
		this.photoList = photoList;
	}

	public int getVideosCount() {
		return videosCount;
	}

	public void setVideosCount(int videosCount) {
		this.videosCount = videosCount;
	}

	public ArrayList<Video> getVideoList() {
		return videoList;
	}

	public void setVideoList(ArrayList<Video> videoList) {
		this.videoList = videoList;
	}

	public class Media{
		private int id;
		private String image;
		public int getId() {
			return id;
		}
		public void setId(int id) {
			this.id = id;
		}
		public String getImage() {
			return image;
		}
		public void setImage(String image) {
			this.image = image;
		}
	}
	public class Photo extends Media{
	}

	public class Video extends Media{
		private String path;

		public String getPath() {
			return path;
		}
		public void setPath(String path) {
			this.path = path;
		}
	}
}
