package com.wuzhai.app.objects;

public class Follower {

	private int userId;
	private String username;
	private String avatar;

	public Follower(int userId,String username,String avatar){
		this.userId = userId;
		this.username = username;
		this.avatar = avatar;
	}

	public int getUserId() {
		return userId;
	}

	public String getUsername() {
		return username;
	}

	public String getAvatar() {
		return avatar;
	}
}
