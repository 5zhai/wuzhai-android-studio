package com.wuzhai.app.objects;

public class GiftType {
//这个类是礼物种类，比如恋爱，节日，奢侈品等
	private String id;
	private String name;
	private String icon;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getIcon() {
		return icon;
	}
	public void setIcon(String icon) {
		this.icon = icon;
	}
}
