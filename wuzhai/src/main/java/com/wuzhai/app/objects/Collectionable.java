package com.wuzhai.app.objects;

public class Collectionable {

	/*收藏项在收藏列表中的id*/
	private int id;
	/*被收藏者的id*/
	private int collectionableId;
	private String collectionableType;
	private String collectionableIcon;

	public String getCollectionableType() {
		return collectionableType;
	}
	public void setCollectionableType(String collectionableType) {
		this.collectionableType = collectionableType;
	}
	public int getCollectionableId() {
		return collectionableId;
	}
	public void setCollectionableId(int collectionableId) {
		this.collectionableId = collectionableId;
	}

	public String getCollectionableIcon() {
		return collectionableIcon;
	}

	public void setCollectionableIcon(String collectionableIcon) {
		this.collectionableIcon = collectionableIcon;
	}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
