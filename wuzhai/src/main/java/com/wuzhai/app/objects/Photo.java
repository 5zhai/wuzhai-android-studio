package com.wuzhai.app.objects;

public class Photo extends MediaObject{
	private String photoCategory;
	private String tags[];

	public String getPhotoCategory() {
		return photoCategory;
	}
	public void setPhotoCategory(String photoCategory) {
		this.photoCategory = photoCategory;
	}
	public String[] getTags() {
		return tags;
	}
	public void setTags(String tags) {
		this.tags = tags.split(",");
	}
	public String[] getPicturePaths() {
		return picturePath.split(",");
	}
}
