package com.wuzhai.app.person.activity;

import java.util.ArrayList;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.widget.TextView;
import android.widget.Toast;

import com.wuzhai.app.R;
import com.wuzhai.app.application.WuZhaiApplication;
import com.wuzhai.app.main.WuzhaiService;
import com.wuzhai.app.main.WuzhaiServiceCallbackAdapter;
import com.wuzhai.app.main.widget.LoadMoreRecyclerView;
import com.wuzhai.app.main.widget.LoadMoreRecyclerView.LoadMoreCallBack;
import com.wuzhai.app.objects.Gift;
import com.wuzhai.app.objects.User;
import com.wuzhai.app.person.widget.GiftAdapter;
import com.wuzhai.app.tools.Utils;
import com.wuzhai.app.widget.TitleToolbarActivity;

public class MyGift extends TitleToolbarActivity{

	private WuzhaiService service;
	private TextView level;
	private TextView experience;
	private TextView giftCount;
	private LoadMoreRecyclerView recyclerView;
	private int nextPage = 1;
	private ArrayList<Gift> giftList = new ArrayList<Gift>();
	private GiftAdapter adapter;
	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setTitle("我的礼物");
		service = ((WuZhaiApplication)getApplication()).getService();
		service.setCallBack(callback);
		service.getGift(1, nextPage);
		setContentView(R.layout.activity_mygift);
		initView();
	}

	private void initView(){
		level = (TextView)findViewById(R.id.level);
		experience = (TextView)findViewById(R.id.experience);
		giftCount = (TextView)findViewById(R.id.gift_count);
		recyclerView = (LoadMoreRecyclerView)findViewById(R.id.gift_list);
		recyclerView.setLoadMoreCallBack(loadMoreCallBack);

		User user = ((WuZhaiApplication)getApplication()).getUser();
		level.setText(user.getLevel()+"");
		experience.setText(user.getCurrentExperience()+"");
		GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 2);
		recyclerView.setLayoutManager(gridLayoutManager);
		adapter = new GiftAdapter(this);
		adapter.setGiftList(giftList);
		recyclerView.setAdapter(adapter);
	}

	private WuzhaiServiceCallbackAdapter callback = new WuzhaiServiceCallbackAdapter(){
		@Override
		public void onGetGiftListDone(ArrayList<Gift> gifts,int next_page) {
			if(gifts!=null){
				nextPage = next_page;
				giftList.addAll(gifts);
				//WrapRecyclerView对RecyclerView进行了继承，当有header或footerview时内部对adapter进行了包装替换，
				//所以要不能直接使用adapter的notifyDataSetChanged()
				recyclerView.getAdapter().notifyDataSetChanged();
				recyclerView.removeFooterView();
			}else {
				Toast.makeText(MyGift.this, "获取数据失败", Toast.LENGTH_SHORT).show();
			}
		}
	};

	private LoadMoreCallBack loadMoreCallBack = new LoadMoreCallBack() {
		@Override
		public void loadMore() {
			if(nextPage!=0){
				if(Utils.isNetworkAvailable(MyGift.this)){
					recyclerView.setFooterViewLoaderMore();
					service.getGift(1, nextPage);
				}else {
					Toast.makeText(MyGift.this, "无网络连接", Toast.LENGTH_SHORT).show();
				}
			}else {
				recyclerView.setFooterNoMoreToLoad();
			}
		}
	};
}
