package com.wuzhai.app.person.activity;

import java.util.ArrayList;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.RelativeLayout;
import com.wuzhai.app.R;
import com.wuzhai.app.application.WuZhaiApplication;
import com.wuzhai.app.main.WuzhaiService;
import com.wuzhai.app.main.WuzhaiServiceCallbackAdapter;
import com.wuzhai.app.main.widget.LoadMoreRecyclerView;
import com.wuzhai.app.objects.GiftCategory;
import com.wuzhai.app.objects.User;
import com.wuzhai.app.person.widget.GiftAdapter;
import com.wuzhai.app.widget.TitleToolbarActivity;

public class GiftCenterActivity extends TitleToolbarActivity {
	private WuzhaiService service;
	private LoadMoreRecyclerView hotGiftList;
	private RelativeLayout dailyAttendance;
	private ArrayList<GiftCategory> giftCategoryList = new ArrayList<GiftCategory>();
	private GiftAdapter adapter;
	private TextView level;
	private TextView experience;
	private LinearLayout allGiftBtn;
	private LinearLayout loveGiftBtn;
	private LinearLayout luxuryGoodsGiftBtn;
	private LinearLayout festivalGiftBtn;
	private LinearLayout kusoGiftBtn;
	private LinearLayout adorableGiftBtn;
	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setTitle("礼物中心");
		setTextMenuString("我的礼物");
		setTextMenuClickListener(clickListener);
		service = ((WuZhaiApplication)getApplication()).getService();
		service.setCallBack(callback);
		service.getGiftCategory();
		setContentView(R.layout.activity_gift_center);
		initView();
	}
	private void initView(){
		hotGiftList = (LoadMoreRecyclerView)findViewById(R.id.hot_gift_list);
		dailyAttendance = (RelativeLayout)findViewById(R.id.daily_attendance);
		dailyAttendance.setOnClickListener(clickListener);
		level = (TextView)findViewById(R.id.level);
		experience = (TextView)findViewById(R.id.experience);
		allGiftBtn = (LinearLayout)findViewById(R.id.all_gift);
		loveGiftBtn = (LinearLayout)findViewById(R.id.love);
		luxuryGoodsGiftBtn = (LinearLayout)findViewById(R.id.luxury_goods);
		festivalGiftBtn = (LinearLayout)findViewById(R.id.festival);
		kusoGiftBtn = (LinearLayout)findViewById(R.id.kuso);
		adorableGiftBtn = (LinearLayout)findViewById(R.id.adorable_pet);

		User user = ((WuZhaiApplication)getApplication()).getUser();
		level.setText(user.getLevel()+"");
		experience.setText(user.getCurrentExperience()+"");
		GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 2);
		hotGiftList.setLayoutManager(gridLayoutManager);
		adapter = new GiftAdapter(this);

		adapter.setGiftList(giftCategoryList);
		hotGiftList.setAdapter(adapter);
		allGiftBtn.setOnClickListener(clickListener);
		loveGiftBtn.setOnClickListener(clickListener);
		luxuryGoodsGiftBtn.setOnClickListener(clickListener);
		festivalGiftBtn.setOnClickListener(clickListener);
		kusoGiftBtn.setOnClickListener(clickListener);
		adorableGiftBtn.setOnClickListener(clickListener);
	}

	private OnClickListener clickListener = new OnClickListener() {
		Intent gitListIntent;
		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.daily_attendance:
				startActivity(new Intent(GiftCenterActivity.this, DailyAttendanceActivity.class));
				break;
			case R.id.toolbar_text_menu:
				startActivity(new Intent(GiftCenterActivity.this, MyGift.class));
				break;
			case R.id.all_gift:
				gitListIntent = new Intent(GiftCenterActivity.this,GiftTypeListActivity.class);
				gitListIntent.putExtra("type_id", 6);
				gitListIntent.putExtra("title", "全部");
				startActivity(gitListIntent);
				break;
			case R.id.love:
				gitListIntent = new Intent(GiftCenterActivity.this,GiftTypeListActivity.class);
				gitListIntent.putExtra("type_id", 4);
				gitListIntent.putExtra("title", "恋爱");
				startActivity(gitListIntent);
				break;
			case R.id.luxury_goods:
				gitListIntent = new Intent(GiftCenterActivity.this,GiftTypeListActivity.class);
				gitListIntent.putExtra("type_id", 3);
				gitListIntent.putExtra("title", "奢侈品");
				startActivity(gitListIntent);
				break;
			case R.id.festival:
				gitListIntent = new Intent(GiftCenterActivity.this,GiftTypeListActivity.class);
				gitListIntent.putExtra("type_id", 2);
				gitListIntent.putExtra("title", "节日");
				startActivity(gitListIntent);
				break;
			case R.id.kuso:
				gitListIntent = new Intent(GiftCenterActivity.this,GiftTypeListActivity.class);
				gitListIntent.putExtra("type_id", 1);
				gitListIntent.putExtra("title", "恶搞");
				startActivity(gitListIntent);
				break;
			case R.id.adorable_pet:
				gitListIntent = new Intent(GiftCenterActivity.this,GiftTypeListActivity.class);
				gitListIntent.putExtra("type_id", 5);
				gitListIntent.putExtra("title", "萌宠");
				startActivity(gitListIntent);
				break;
			}
		}
	};

	private WuzhaiServiceCallbackAdapter callback = new WuzhaiServiceCallbackAdapter(){
		@Override
		public void onGetGiftCategoryListDone(ArrayList<GiftCategory> giftCategorys) {
			if(giftCategorys!=null){
				//因为此处显示的礼物种类，不会太多，所以取消分页，一次全部加载
//				nextPage = next_page;
				giftCategoryList.addAll(giftCategorys);
				//WrapRecyclerView对RecyclerView进行了继承，当有header或footerview时内部对adapter进行了包装替换，
				//所以要不能直接使用adapter的notifyDataSetChanged()
				hotGiftList.getAdapter().notifyDataSetChanged();
//				if(next_page==0){
					hotGiftList.setFooterNoMoreToLoad();
//				}else {
//					hotGiftList.setFooterViewLoaderMore();
//				}
			}else {
				Toast.makeText(GiftCenterActivity.this, "获取数据失败", Toast.LENGTH_SHORT).show();
			}
		}
	};
}
