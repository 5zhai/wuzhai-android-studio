package com.wuzhai.app.person.activity;

import java.util.ArrayList;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;
import android.widget.Toast;
import com.wuzhai.app.R;
import com.wuzhai.app.application.WuZhaiApplication;
import com.wuzhai.app.main.WuzhaiService;
import com.wuzhai.app.main.WuzhaiServiceCallbackAdapter;
import com.wuzhai.app.main.widget.LoadMoreRecyclerView;
import com.wuzhai.app.main.widget.LoadMoreRecyclerView.LoadMoreCallBack;
import com.wuzhai.app.objects.Gift;
import com.wuzhai.app.objects.User;
import com.wuzhai.app.person.widget.GiftAdapter;
import com.wuzhai.app.tools.Utils;
import com.wuzhai.app.widget.TitleToolbarActivity;

public class GiftTypeListActivity extends TitleToolbarActivity {

	private WuzhaiService service;
	private TextView level;
	private TextView experience;
	private LoadMoreRecyclerView recyclerView;
	private int type;// 礼物类型
	private int nextPage = 1;
	private ArrayList<Gift> giftList = new ArrayList<Gift>();
	private GiftAdapter adapter;

	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Intent intentData = getIntent();
		String title = intentData.getStringExtra("title");
		type = intentData.getIntExtra("type_id", 0);
		setTitle(title);
		setTextMenuString("我的礼物");
		setTextMenuClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				startActivity(new Intent(GiftTypeListActivity.this,
						MyGift.class));
			}
		});
		service = ((WuZhaiApplication) getApplication()).getService();
		service.setCallBack(callback);
		service.getGiftTypeList(type, nextPage);
		setContentView(R.layout.activity_giftlist);
		initView();
	}

	private void initView() {
		level = (TextView) findViewById(R.id.level);
		experience = (TextView) findViewById(R.id.experience);
		recyclerView = (LoadMoreRecyclerView) findViewById(R.id.gift_list);

		User user = ((WuZhaiApplication) getApplication()).getUser();
		level.setText(user.getLevel() + "");
		experience.setText(user.getCurrentExperience() + "");
		GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 2);
		recyclerView.setLayoutManager(gridLayoutManager);
		recyclerView.setLoadMoreCallBack(loadMoreCallBack);
		adapter = new GiftAdapter(this);
		adapter.setGiftList(giftList);
		recyclerView.setAdapter(adapter);
	}

	private WuzhaiServiceCallbackAdapter callback = new WuzhaiServiceCallbackAdapter() {
		@Override
		public void onGetGiftListDone(ArrayList<Gift> gifts, int next_page) {
			if (gifts != null) {
				nextPage = next_page;
				giftList.addAll(gifts);
				// WrapRecyclerView对RecyclerView进行了继承，当有header或footerview时内部对adapter进行了包装替换，
				// 所以要不能直接使用adapter的notifyDataSetChanged()
				recyclerView.getAdapter().notifyDataSetChanged();
				recyclerView.removeFooterView();
			} else {
				Toast.makeText(GiftTypeListActivity.this, "获取数据失败",
						Toast.LENGTH_SHORT).show();
			}
		}
	};

	private LoadMoreCallBack loadMoreCallBack = new LoadMoreCallBack() {
		@Override
		public void loadMore() {
			if (nextPage != 0) {
				if (Utils.isNetworkAvailable(GiftTypeListActivity.this)) {
					recyclerView.setFooterViewLoaderMore();
					service.getGiftTypeList(type, nextPage);
				} else {
					Toast.makeText(GiftTypeListActivity.this, "无网络连接",
							Toast.LENGTH_SHORT).show();
				}
			} else {
				recyclerView.setFooterNoMoreToLoad();
				// Toast.makeText(ComicConActivity.this, "无更多数据",
				// Toast.LENGTH_SHORT).show();
			}
		}
	};
}
