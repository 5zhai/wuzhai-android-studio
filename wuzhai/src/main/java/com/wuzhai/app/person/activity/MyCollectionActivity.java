package com.wuzhai.app.person.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.widget.LinearLayout;

import com.viewpagerindicator.TabPageIndicator;
import com.wuzhai.app.R;
import com.wuzhai.app.main.WuzhaiService;
import com.wuzhai.app.main.widget.ViewPagerAdapter;
import com.wuzhai.app.person.fragments.CollectionListFragment;
import com.wuzhai.app.widget.TitleToolbarActivity;

import java.util.ArrayList;

/**
 * Created by huangyue on 2016/11/10.
 */

public class MyCollectionActivity extends TitleToolbarActivity {
    private TabPageIndicator indicator;
    private ViewPager viewPager;
    private LinearLayout progressbarLayout;
    private ArrayList<Fragment> pageList = new ArrayList<Fragment>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle("我的收藏");
        /*相同布局重用*/
        setContentView(R.layout.activity_dance);

        progressbarLayout = (LinearLayout)findViewById(R.id.progressbar_layout);
        indicator = (TabPageIndicator)findViewById(R.id.dance_indicator);
        viewPager = (ViewPager)findViewById(R.id.dance_viewpager);

        initPageList();
        ViewPagerAdapter vpAdapter= new ViewPagerAdapter(getSupportFragmentManager(), pageList);
        vpAdapter.setPageTitle(new String[]{"集市收藏","视频收藏","图片收藏"});
        viewPager.setAdapter(vpAdapter);
        indicator.setViewPager(viewPager);

    }

    private void initPageList(){
        //因为收藏列表页的三个page布局都一样，只是内容不一样，所以在activity中使用了三个
        //CollectionListFragment对象，用Argument区分向服务器请求的数据分类
        CollectionListFragment matket = new CollectionListFragment();
        Bundle mBudle = new Bundle();
        mBudle.putString("collection_type",WuzhaiService.TAG_MARKET);
        matket.setArguments(mBudle);

        CollectionListFragment video = new CollectionListFragment();
        Bundle vBudle = new Bundle();
        vBudle.putString("collection_type",WuzhaiService.TAG_VIDEO);
        video.setArguments(vBudle);

        CollectionListFragment photo = new CollectionListFragment();
        Bundle pBudle = new Bundle();
        pBudle.putString("collection_type",WuzhaiService.TAG_PHOTO);
        photo.setArguments(pBudle);

        pageList.add(matket);
        pageList.add(video);
        pageList.add(photo);
    }

    public void setProgressbarLayoutVisibility(int visibility){
        progressbarLayout.setVisibility(visibility);
    }
}
