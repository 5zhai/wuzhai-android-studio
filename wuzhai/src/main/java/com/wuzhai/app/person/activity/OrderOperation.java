package com.wuzhai.app.person.activity;

import com.wuzhai.app.R;
import com.wuzhai.app.application.WuZhaiApplication;
import com.wuzhai.app.main.WuzhaiService;
import com.wuzhai.app.main.WuzhaiServiceCallbackAdapter;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class OrderOperation extends Activity{
	private WuzhaiService service;
	private int orderId;
	private String orderTitle;
	private TextView orderIdTV;
	private TextView orderTitleTV;
	private EditText scoreET;
	private EditText commentET;
	private Button cancleBtn;
	private Button scoreBtn;
	private Button receiveBtn;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_orderoperation);
		service = ((WuZhaiApplication)getApplication()).getService();
		service.setCallBack(callbackAdapter);
		initView();
	}

	private void initView(){
		orderIdTV = (TextView)findViewById(R.id.order_id);
		orderTitleTV = (TextView)findViewById(R.id.order_title);
		scoreET = (EditText)findViewById(R.id.score);
		commentET = (EditText)findViewById(R.id.comment);
		cancleBtn = (Button)findViewById(R.id.cancle_btn);
		cancleBtn.setOnClickListener(clickListener);
		scoreBtn = (Button)findViewById(R.id.score_btn);
		scoreBtn.setOnClickListener(clickListener);
		receiveBtn = (Button)findViewById(R.id.receive_btn);
		receiveBtn.setOnClickListener(clickListener);

		Intent intent = getIntent();
		orderId = intent.getIntExtra("order_id", 0);
		orderTitle = intent.getStringExtra("order_title");
		orderIdTV.setText(""+orderId);
		orderTitleTV.setText(orderTitle);
	}

	private OnClickListener clickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.cancle_btn:
				service.cancleOrder(orderId);
				break;
			case R.id.score_btn:
				service.commentOrder(orderId, 5, "好牛逼");
				break;
			case R.id.receive_btn:
				service.receiveOrder(orderId);
				break;
			}
		}
	};

	private WuzhaiServiceCallbackAdapter callbackAdapter = new WuzhaiServiceCallbackAdapter(){

		@Override
		public void onCompleted(int result) {
			switch (result) {
			case WuzhaiService.CANCLE_ORDER_SUCC:
				Toast.makeText(OrderOperation.this, "取消订单成功", Toast.LENGTH_SHORT).show();
				break;

			case WuzhaiService.CANCLE_ORDER_FAL:
				Toast.makeText(OrderOperation.this, "取消订单失败", Toast.LENGTH_SHORT).show();
				break;
			case WuzhaiService.RECEIVE_ORDER_SUCC:
				Toast.makeText(OrderOperation.this, "确认收货成功", Toast.LENGTH_SHORT).show();
				break;
			case WuzhaiService.RECEIVE_ORDER_FAL:
				Toast.makeText(OrderOperation.this, "确认收货失败", Toast.LENGTH_SHORT).show();
				break;
			}
		}
	};
}
