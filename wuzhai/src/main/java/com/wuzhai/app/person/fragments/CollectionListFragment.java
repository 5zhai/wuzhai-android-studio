package com.wuzhai.app.person.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.wuzhai.app.R;
import com.wuzhai.app.application.WuZhaiApplication;
import com.wuzhai.app.main.WuzhaiService;
import com.wuzhai.app.main.WuzhaiServiceCallbackAdapter;
import com.wuzhai.app.main.market.activity.AftercareDetialsActivity;
import com.wuzhai.app.main.market.activity.GoodsDetialActivity;
import com.wuzhai.app.main.video.activity.DanceDetailActivity;
import com.wuzhai.app.main.welfare.WelfareDetialActivity;
import com.wuzhai.app.main.widget.LoadMoreRecyclerView;
import com.wuzhai.app.main.widget.OnRecyclerViewItemClickListener;
import com.wuzhai.app.objects.Collectionable;
import com.wuzhai.app.person.activity.MyCollectionActivity;
import com.wuzhai.app.person.widget.CollectionAdapter;

import java.util.ArrayList;

/**
 * Created by huangyue on 2016/11/10.
 */

public class CollectionListFragment extends Fragment {
    private WuzhaiService service;
    /*显示的收藏类型*/
    private String type;
    private LoadMoreRecyclerView recyclerView;
    private CollectionAdapter adapter;
    private ArrayList<Collectionable> collectionList = new ArrayList<>();
    private int nextPage = 1;
    private boolean isVisibleToUser = false;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        /*view重用*/
        View view = inflater.inflate(R.layout.fragment_dancelist, null);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        /*获取view*/
        recyclerView = (LoadMoreRecyclerView)view.findViewById(R.id.rvFeed_dance);
        /*view设置data*/
        adapter = new CollectionAdapter(getContext(),collectionList);
        adapter.setOnRecyclerViewItemClickListener(recyclerViewItemClickListener);
        GridLayoutManager layoutManager = new GridLayoutManager(getContext(),2);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setLoadMoreCallBack(loadMoreCallBack);
        recyclerView.setAdapter(adapter);
        /*获取data*/
        Bundle bundle = getArguments();
        type = (String)bundle.get("collection_type");
        Log.d("yue.huang","typetypetype:"+type);
        service = ((WuZhaiApplication)(getActivity().getApplicationContext())).getService();
        if(isVisibleToUser && collectionList.size() == 0){
            service.setCallBack(callbackAdapter);
            service.getCollectionableList(type,nextPage);
            ((MyCollectionActivity)getContext()).setProgressbarLayoutVisibility(View.VISIBLE);
        }
    }

//    @Override
//    public void onResume() {
//        super.onResume();
//        if(getUserVisibleHint() && collectionList.size()==0){
//            service.setCallBack(callbackAdapter);
//            service.getCollectionableList(type,nextPage);
//        }
//    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        this.isVisibleToUser = isVisibleToUser;
        if(isVisibleToUser && service!=null){
            /*只要显示就重新设置回调，防止回调混乱*/
            service.setCallBack(callbackAdapter);
            if(collectionList.size()==0){
                service.getCollectionableList(type,nextPage);
                ((MyCollectionActivity)getContext()).setProgressbarLayoutVisibility(View.VISIBLE);
            }
        }
    }

    private WuzhaiServiceCallbackAdapter callbackAdapter = new WuzhaiServiceCallbackAdapter(){
        @Override
        public void onGetCollectionableListCompleted(ArrayList<Collectionable> collectionable_List, int next_page) {
            if(collectionable_List.size()!=0){
                ((MyCollectionActivity)getContext()).setProgressbarLayoutVisibility(View.GONE);
                recyclerView.removeFooterView();
                collectionList.addAll(collectionable_List);
                recyclerView.getAdapter().notifyDataSetChanged();
                nextPage = next_page;
            }
        }
    };

    private LoadMoreRecyclerView.LoadMoreCallBack loadMoreCallBack = new LoadMoreRecyclerView.LoadMoreCallBack() {
        @Override
        public void loadMore() {
            if(nextPage!=0){
                recyclerView.setFooterViewLoaderMore();
                service.getCollectionableList(type,nextPage);
            }else {
                recyclerView.setFooterNoMoreToLoad();
            }

        }
    };

    private OnRecyclerViewItemClickListener recyclerViewItemClickListener = new OnRecyclerViewItemClickListener() {
        @Override
        public void onItemClick(View view, int position) {
            Intent intent = null;
            Collectionable collectionable = collectionList.get(position);
            switch (collectionable.getCollectionableType()){
                case WuzhaiService.TYPE_PHOTO:
                    intent = new Intent(getContext(), WelfareDetialActivity.class);
                    break;
                case WuzhaiService.TYPE_VIDEO:
                    intent = new Intent(getContext(), DanceDetailActivity.class);
                    break;
                case WuzhaiService.TYPE_COMMODITY:
                    intent = new Intent(getContext(), GoodsDetialActivity.class);
                    break;
                case WuzhaiService.TYPE_PHOTOGRAPHY:
                    intent = new Intent(getContext(), AftercareDetialsActivity.class);
                    intent.putExtra("type",WuzhaiService.TYPE_PHOTOGRAPHY);
                    intent.putExtra("isShowLable", true);
                    break;
                case WuzhaiService.TYPE_PROCESSING:
                    intent = new Intent(getContext(),AftercareDetialsActivity.class);
                    intent.putExtra("type", WuzhaiService.TYPE_PROCESSING);
                    intent.putExtra("isShowLable", true);
                    break;
            }
            intent.putExtra("id",collectionList.get(position).getCollectionableId());
            startActivity(intent);
        }
    };
}
