package com.wuzhai.app.person.activity;

import java.util.ArrayList;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import com.wuzhai.app.R;
import com.wuzhai.app.person.widget.Message;
import com.wuzhai.app.person.widget.MessageAdapter;
import com.wuzhai.app.widget.TitleToolbarActivity;

public class MessageCenterActivity extends TitleToolbarActivity {

	private RecyclerView rvFeedRecycler;
	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setTitle("消息中心");
		setImgMenuResource(R.drawable.icon_buddy_list);
		setContentView(R.layout.activity_recyclerview);
		initView();
	}
	
	private void initView(){
		rvFeedRecycler = (RecyclerView)findViewById(R.id.rvFeed_recycler);
		Message message = new Message(""+R.drawable.person_avatar, "卡卡西", "天冷了，多穿点衣服～", "14:12", "天真的冷了，必须多穿点衣服～");
		ArrayList<Message> messages = new ArrayList<Message>();
		for (int i = 0; i < 6; i++) {
			messages.add(message);
		}
		MessageAdapter adapter = new MessageAdapter(this, messages);
		rvFeedRecycler.setAdapter(adapter);
		rvFeedRecycler.setLayoutManager(new LinearLayoutManager(this));
	}
}
