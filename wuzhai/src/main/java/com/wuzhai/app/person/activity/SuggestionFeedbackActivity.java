package com.wuzhai.app.person.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.wuzhai.app.R;
import com.wuzhai.app.application.WuZhaiApplication;
import com.wuzhai.app.main.WuzhaiService;
import com.wuzhai.app.main.WuzhaiServiceCallbackAdapter;
import com.wuzhai.app.widget.TitleToolbarActivity;

public class SuggestionFeedbackActivity extends TitleToolbarActivity {

	private WuzhaiService service;
	private EditText suggestionET;
	private Button submitBtn;
	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setTitle("意见反馈");
		service = ((WuZhaiApplication)getApplication()).getService();
		service.setCallBack(callbackAdapter);
		setContentView(R.layout.activity_suggestion_feedback);
		initView();
	}

	private void initView(){
		suggestionET = (EditText)findViewById(R.id.suggestionET);
		submitBtn = (Button)findViewById(R.id.submitBtn);
		submitBtn.setOnClickListener(clickListener);
	}

	private OnClickListener clickListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			submit(suggestionET.getText().toString());
		}
	};

	private void submit(String suggestions){
		if(!suggestions.equals("")){
			service.suggestionsFeedback(suggestions);
		}
	}

	private WuzhaiServiceCallbackAdapter callbackAdapter = new WuzhaiServiceCallbackAdapter(){
		@Override
		public void onCompleted(int result) {
			switch (result) {
			case WuzhaiService.SUGGESTIONS_FEEDBACK_SUCC:
				Toast.makeText(SuggestionFeedbackActivity.this, "提交成功", Toast.LENGTH_SHORT).show();
				break;
			case WuzhaiService.SUGGESTIONS_FEEDBACK_FAL:
				Toast.makeText(SuggestionFeedbackActivity.this, "提交失败", Toast.LENGTH_SHORT).show();
				break;
			}
		}
	};
}
