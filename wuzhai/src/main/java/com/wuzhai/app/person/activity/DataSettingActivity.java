package com.wuzhai.app.person.activity;

import java.io.File;
import java.util.ArrayList;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
import com.wuzhai.app.R;
import com.wuzhai.app.application.WuZhaiApplication;
import com.wuzhai.app.main.WuzhaiService;
import com.wuzhai.app.main.WuzhaiServiceCallbackAdapter;
import com.wuzhai.app.objects.User;
import com.wuzhai.app.tools.Utils;
import com.wuzhai.app.widget.TitleToolbarActivity;

public class DataSettingActivity extends TitleToolbarActivity {

	private RelativeLayout avatarLauout;
	private ImageView avatarImageView;
	private LinearLayout usernameLauout;
	private TextView usernameTextView;
	private LinearLayout sexLauout;
	private TextView sexTextView;
	private LinearLayout birthdayLauout;
	private TextView birthdayTextView;
	private LinearLayout autographLauout;
	private TextView autographTextView;
	private DatePickerDialog datePickerDialog;
    private final int PHOTO_PICKED_WITH_DATA = 1881;
    private final int CAMERA_WITH_DATA = 1882;
    private final int PIC_CROP_RESOULT = 1884;
    private final int SET_AUTOGRAPH = 1883;
    private int ICON_SIZE = 150;
    private WuzhaiService service;
    private Bitmap avatarBitmap;
    private ProgressDialog progressDialog;

	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setTitle("编辑资料");
		setTextMenuString("保存");
		setTextMenuClickListener(clickListener);
		setContentView(R.layout.activity_data_setting);
		service = ((WuZhaiApplication)getApplication()).getService();
		service.setCallBack(callbackAdapter);
		initView();
		initPicUri();
	}
	private void initView(){
		datePickerDialog = new DatePickerDialog(this, new OnDateSetListener() {
			@Override
			public void onDateSet(DatePicker view, int year, int monthOfYear,
					int dayOfMonth) {
				String birthdayString = year+"-"+(monthOfYear+1)+"-"+dayOfMonth;
				birthdayTextView.setText(birthdayString);
			}
		}, 1970, 1, 1);
		avatarLauout = (RelativeLayout)findViewById(R.id.avatar_lauout);
		avatarImageView = (ImageView)findViewById(R.id.avatar);
		usernameLauout = (LinearLayout)findViewById(R.id.username_lauout);
		usernameTextView = (TextView)findViewById(R.id.username);
		sexLauout = (LinearLayout)findViewById(R.id.sex_lauout);
		sexTextView = (TextView)findViewById(R.id.sex);
		birthdayLauout = (LinearLayout)findViewById(R.id.birthday_lauout);
		birthdayTextView = (TextView)findViewById(R.id.birthday);
		autographLauout = (LinearLayout)findViewById(R.id.autograph_lauout);
		autographTextView = (TextView)findViewById(R.id.autograph);

		avatarLauout.setOnClickListener(clickListener);
		usernameLauout.setOnClickListener(clickListener);
		autographLauout.setOnClickListener(clickListener);
		sexLauout.setOnClickListener(clickListener);
		birthdayLauout.setOnClickListener(clickListener);

		User user = ((WuZhaiApplication) getApplication()).getUser();
		Picasso.with(this).load(user.getAvatarUrl()).resize(150, 150).centerInside().into(avatarImageView);
		usernameTextView.setText(user.getName());
		birthdayTextView.setText(user.getBirthday());
		autographTextView.setText(user.getSignature());
		sexTextView.setText(user.getSexString());

		progressDialog = new ProgressDialog(DataSettingActivity.this);
		progressDialog.setCanceledOnTouchOutside(false);
		progressDialog.setMessage("数据上传中。。。");
	}

	private OnClickListener clickListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.toolbar_text_menu:
				progressDialog.show();
				if (avatarUri != null) {
					ArrayList<String> bitmapList = new ArrayList<String>();
					bitmapList.add(Utils.uriToPath(DataSettingActivity.this, avatarUri));
					service.publishPic(3,null, null,
							bitmapList,null,null);
				}else {
					uploadUserInfo(null);
				}
				break;
			case R.id.avatar_lauout:
//				selectPic();
				showPop();
				break;
			case R.id.username_lauout:
				showEditDialog(usernameTextView,"更改昵称");
				break;
			case R.id.autograph_lauout:
//				showEditDialog(autographTextView,"更改签名");
				Intent intent = new Intent(DataSettingActivity.this, AutographEditActivity.class);
				intent.putExtra("autograph", autographTextView.getText());
				startActivityForResult(intent, SET_AUTOGRAPH);
				break;
			case R.id.sex_lauout:
				int checkedItem = -1;
				String currentSex = sexTextView.getText().toString();
				String[] sexs = getResources().getStringArray(R.array.sex);
				for(int n=0;n<sexs.length;n++){
					if(sexs[n].equals(currentSex)){
						checkedItem = n;
						break;
					}
				}
				showSingleChoiceDialog(sexTextView, R.array.sex, checkedItem);
				break;
			case R.id.birthday_lauout:
				if(!TextUtils.isEmpty(birthdayTextView.getText().toString()) && !"null".equals(birthdayTextView.getText().toString())){
					String[] dates = birthdayTextView.getText().toString().split("-");
					datePickerDialog.updateDate(Integer.parseInt(dates[0]), Integer.parseInt(dates[1])-1, Integer.parseInt(dates[2]));
				}
				datePickerDialog.show();
				break;
			}
		}
	};

	private void showEditDialog(final TextView textView,String title){
		final EditText inputEdit = new EditText(this);
		AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
		dialogBuilder.setView(inputEdit);
		dialogBuilder.setTitle(title);
		dialogBuilder.setNegativeButton("取消", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
			}
		});
		dialogBuilder.setPositiveButton("确定", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				textView.setText(inputEdit.getText().toString());
			}
		});
		dialogBuilder.show();
	}

	private void showSingleChoiceDialog(final TextView textview,int arrayId,int checkedItem){
		AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
		dialogBuilder.setTitle("性别");
		dialogBuilder.setSingleChoiceItems(arrayId, checkedItem, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				textview.setText(getResources().getStringArray(R.array.sex)[which]);
				dialog.dismiss();
			}
		});
		dialogBuilder.show();
	}

    private void uploadUserInfo(String avatarPath){
		String username = usernameTextView.getText().toString();
		String birthday = birthdayTextView.getText().toString();
		String signature = autographTextView.getText().toString();
		String sex = sexTextView.getText().toString();

		if(TextUtils.isEmpty(username) || TextUtils.isEmpty(birthday) ||
				TextUtils.isEmpty(signature) || TextUtils.isEmpty(sex)){
			Toast.makeText(this, "信息不完整！", Toast.LENGTH_SHORT).show();
			return;
		}
		if(!Utils.isNetworkAvailable(this)){
			Toast.makeText(this, "无网络连接", Toast.LENGTH_SHORT).show();
			return;
		}
		service.saveUserInfo(avatarPath, username, birthday, signature, sex);
    }
    private WuzhaiServiceCallbackAdapter callbackAdapter = new WuzhaiServiceCallbackAdapter(){

		@Override
		public void onUploadFileCompleted(ArrayList<String> picPathList) {
			if(picPathList.size()!=0){
				uploadUserInfo(picPathList.get(0));
			}else {
				progressDialog.dismiss();
				Toast.makeText(DataSettingActivity.this, "上传头像失败，请重试", Toast.LENGTH_SHORT).show();
			}
		}
		public void onCompleted(int result) {
			progressDialog.dismiss();
			switch (result) {
			case WuzhaiService.SAVE_USER_INFO_SUCC:
				Toast.makeText(DataSettingActivity.this, "保存成功", Toast.LENGTH_SHORT).show();
				break;

			case WuzhaiService.SAVE_USER_INFO_FAL:
				Toast.makeText(DataSettingActivity.this, "保存失败，请重试", Toast.LENGTH_SHORT).show();
				break;
			}
		};
    };

	//---------------头像相关------------------
    private PopupWindow mSetPhotoPop;
    private Uri avatarUri;
    private Uri photoUri;
    private void initPicUri(){
        File file = new File(Environment.getExternalStorageDirectory() + "/DCIM/Photo");
        if (!file.exists()) {
            file.mkdirs();
        }
        File mAvatarFile = new File(file, "avatar");
        avatarUri = Uri.fromFile(mAvatarFile);
        File mPhotoFile = new File(file, "photo");
        photoUri = Uri.fromFile(mPhotoFile);
    }
    public void showPop(){
        View mainView = LayoutInflater.from(this).inflate(R.layout.alert_setphoto_menu_layout, null);
        Button btnTakePhoto = (Button) mainView.findViewById(R.id.btn_take_photo);
        btnTakePhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSetPhotoPop.dismiss();
                doTakePhoto();
            }
        });
        Button btnCheckFromGallery = (Button) mainView.findViewById(R.id.btn_check_from_gallery);
        btnCheckFromGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSetPhotoPop.dismiss();
                doPickPhotoFromGallery();
            }
        });
        Button btnCancle = (Button) mainView.findViewById(R.id.btn_cancel);
        btnCancle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSetPhotoPop.dismiss();
            }
        });
        mSetPhotoPop = new PopupWindow(this);
        mSetPhotoPop.setBackgroundDrawable(new BitmapDrawable());
        mSetPhotoPop.setFocusable(true);
        mSetPhotoPop.setTouchable(true);
        mSetPhotoPop.setOutsideTouchable(true);
        mSetPhotoPop.setContentView(mainView);
        mSetPhotoPop.setWidth(ViewGroup.LayoutParams.MATCH_PARENT);
        mSetPhotoPop.setHeight(ViewGroup.LayoutParams.WRAP_CONTENT);
        mSetPhotoPop.setAnimationStyle(R.style.bottomStyle);
        mSetPhotoPop.showAtLocation(findViewById(android.R.id.content), Gravity.BOTTOM, 0, 0);
        mSetPhotoPop.update();
    }

    protected void doTakePhoto() {
        try {
            // Launch camera to take photo for selected contact
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);
            startActivityForResult(intent, CAMERA_WITH_DATA);
        } catch (ActivityNotFoundException e) {
			e.printStackTrace();
        }
    }
    protected void doPickPhotoFromGallery() {
        try {
            // Launch picker to choose photo for selected contact
            final Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(intent, PHOTO_PICKED_WITH_DATA);
        } catch (ActivityNotFoundException e) {
			e.printStackTrace();
        }
    }
    public Intent getCropImageIntent(Uri photoUri) {
        Intent intent = new Intent("com.android.camera.action.CROP");
        intent.setDataAndType(photoUri, "image/*");
        intent.putExtra("crop", "true");
        intent.putExtra("aspectX", 1);
        intent.putExtra("aspectY", 1);
        intent.putExtra("outputX", ICON_SIZE);
        intent.putExtra("outputY", ICON_SIZE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, avatarUri);
        intent.putExtra("outputFormat", Bitmap.CompressFormat.JPEG.toString());
        intent.putExtra("noFaceDetection", true);
        intent.putExtra("return-data", false);
        return intent;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
			switch (requestCode) {
			case CAMERA_WITH_DATA:
				final Intent intent_camera = getCropImageIntent(photoUri);
				startActivityForResult(intent_camera, PIC_CROP_RESOULT);
				break;
			case PIC_CROP_RESOULT:
				avatarBitmap = Utils.decodeUriAsBitmap(this, avatarUri);
				avatarImageView.setImageBitmap(avatarBitmap);
				break;
			case PHOTO_PICKED_WITH_DATA:
				final Intent intent_photo = getCropImageIntent(data.getData());
				startActivityForResult(intent_photo, PIC_CROP_RESOULT);
				break;
			case SET_AUTOGRAPH:
				autographTextView.setText(data.getStringExtra("autograph"));
				break;
			}
        }
    }
}
