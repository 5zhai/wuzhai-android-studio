package com.wuzhai.app.person.widget;

import java.util.ArrayList;
import android.content.Context;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.squareup.picasso.Picasso;
import com.wuzhai.app.R;
import com.wuzhai.app.main.widget.HyRecyclerViewAdapter;
import com.wuzhai.app.objects.Gift;

public class GiftAdapter extends HyRecyclerViewAdapter{

	private ArrayList<? extends Gift> giftList;
	private Context context;

	public GiftAdapter(Context context){
		this.context = context;
	}

	@Override
	public int getItemCount() {
		// TODO Auto-generated method stub
		return giftList.size();
	}

	@Override
	public void onBindViewHolder(ViewHolder arg0, int arg1) {
		Gift gift = giftList.get(arg1);
		GiftHolder giftHolder = (GiftHolder)arg0;
		giftHolder.experience.setText("经验点:"+gift.getCostCoin());
		giftHolder.giftName.setText(gift.getName());
		giftHolder.price.setText("￥ "+gift.getPrice());
		Picasso.with(context).load(gift.getImagePath()).into(giftHolder.giftPic);

        giftHolder.itemView.setTag(giftHolder.getAdapterPosition());
        giftHolder.itemView.setOnClickListener(clickListener);
	}

	@Override
	public ViewHolder onCreateViewHolder(ViewGroup arg0, int arg1) {
		View view = LayoutInflater.from(context).inflate(R.layout.gift_item_layout, arg0,false);
		return new GiftHolder(view);
	}

	private class GiftHolder extends ViewHolder{
		public ImageView giftPic;
		public TextView giftName;
		public TextView price;
		public TextView experience;

		public GiftHolder(View arg0) {
			super(arg0);
			giftPic = (ImageView)arg0.findViewById(R.id.gift_pic);
			giftName = (TextView)arg0.findViewById(R.id.gift_name);
			price = (TextView)arg0.findViewById(R.id.price);
			experience = (TextView)arg0.findViewById(R.id.experience);
		}
	}
	public void setGiftList(ArrayList<? extends Gift> giftList){
		this.giftList = giftList;
	}

	private View.OnClickListener clickListener = new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			if(recyclerViewItemClickListener!=null){
                recyclerViewItemClickListener.onItemClick(v,(Integer) v.getTag());
            }
		}
	};
}
