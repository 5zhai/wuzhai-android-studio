package com.wuzhai.app.person.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
import com.wuzhai.app.R;
import com.wuzhai.app.application.WuZhaiApplication;
import com.wuzhai.app.main.WuzhaiService;
import com.wuzhai.app.main.WuzhaiServiceCallbackAdapter;
import com.wuzhai.app.main.widget.LoadMoreRecyclerView;
import com.wuzhai.app.main.widget.OnRecyclerViewItemClickListener;
import com.wuzhai.app.objects.Gift;
import com.wuzhai.app.person.widget.GiftAdapter;
import com.wuzhai.app.widget.TitleToolbarActivity;

import java.util.ArrayList;

import static com.wuzhai.app.R.id.gift;
import static com.wuzhai.app.R.id.give;

public class GiftGiveActivity extends TitleToolbarActivity {

	private LoadMoreRecyclerView myGiftList;
	private LoadMoreRecyclerView recommendGiftList;
    private ImageView mainGiftPic;
    private TextView mainGiftName;
    private TextView mainPriceflag;
    private Button changeBtn;
    private Button giveBtn;
    private WuzhaiService service;
    private int nextPage = 1;
    private ArrayList<Gift> giftList = new ArrayList<>();
    private GiftAdapter myAdapter;
    private int receiverId;
    /*当前主礼物在礼物列表中的index*/
    private int currentGiftIndex = -1;
	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

        receiverId = getIntent().getIntExtra("receiver_id",-1);

		setTitle("礼物中心");
		setContentView(R.layout.activity_gift_give);
		initView();
        service = ((WuZhaiApplication)getApplication()).getService();
        service.setCallBack(callback);
        service.getGift(1, nextPage);
	}
	
	private void initView(){
        mainGiftPic = (ImageView)findViewById(R.id.main_pic);
        mainGiftName = (TextView)findViewById(R.id.gift_name);
        mainPriceflag = (TextView)findViewById(R.id.flag);
        changeBtn = (Button)findViewById(R.id.change);
        giveBtn = (Button)findViewById(R.id.give);
        changeBtn.setOnClickListener(clickListener);
        giveBtn.setOnClickListener(clickListener);


		myGiftList = (LoadMoreRecyclerView)findViewById(R.id.my_gift_list);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 2);
        myGiftList.setLayoutManager(gridLayoutManager);
        myAdapter = new GiftAdapter(this);
        myAdapter.setGiftList(giftList);
        myAdapter.setOnRecyclerViewItemClickListener(onRecyclerViewItemClickListener);
        myGiftList.setAdapter(myAdapter);
		//初始化推荐礼物
		recommendGiftList = (LoadMoreRecyclerView)findViewById(R.id.recommend_gift_list);
	}

    private WuzhaiServiceCallbackAdapter callback = new WuzhaiServiceCallbackAdapter(){
        @Override
        public void onGetGiftListDone(ArrayList<Gift> gifts, int next_page) {
            if(gifts!=null){
                nextPage = next_page;
                giftList.addAll(gifts);
                //WrapRecyclerView对RecyclerView进行了继承，当有header或footerview时内部对adapter进行了包装替换，
                //所以要不能直接使用adapter的notifyDataSetChanged()
                myGiftList.getAdapter().notifyDataSetChanged();
                myGiftList.removeFooterView();
                updateMainGift();
            }else {
                Toast.makeText(GiftGiveActivity.this, "获取数据失败", Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        public void onSendGiftDone(boolean result) {
            if(result){
                Toast.makeText(GiftGiveActivity.this,"赠送成功",Toast.LENGTH_SHORT).show();
                giftList.clear();
                service.getGift(1, nextPage);
            }else {
                Toast.makeText(GiftGiveActivity.this,"赠送失败",Toast.LENGTH_SHORT).show();
            }
        }
    };

    private void updateMainGift(){
        if(++currentGiftIndex>=giftList.size()){
            currentGiftIndex = 0;
        }
        Gift gift = giftList.get(currentGiftIndex);
        Picasso.with(this).load(gift.getImagePath()).resize(100,100).centerCrop().into(mainGiftPic);
        mainGiftName.setText(gift.getName());
        mainPriceflag.setText("￥ "+gift.getPrice());
    }

    private void updateMainGift(int index){
        Gift gift = giftList.get(index);
        Picasso.with(this).load(gift.getImagePath()).resize(100,100).centerCrop().into(mainGiftPic);
        mainGiftName.setText(gift.getName());
        mainPriceflag.setText("￥ "+gift.getPrice());
    }

    private View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.change:
                    updateMainGift();
                    break;
                case R.id.give:
                    service.sendGift(receiverId,giftList.get(currentGiftIndex).getId(),WuzhaiService.TAG_PHOTO,0);
                    break;
            }
        }
    };

    private OnRecyclerViewItemClickListener onRecyclerViewItemClickListener = new OnRecyclerViewItemClickListener() {
        @Override
        public void onItemClick(View view, int position) {
            currentGiftIndex = position;
            updateMainGift(position);
        }
    };
}
