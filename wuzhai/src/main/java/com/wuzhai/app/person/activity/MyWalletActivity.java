package com.wuzhai.app.person.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

import com.wuzhai.app.R;
import com.wuzhai.app.application.WuZhaiApplication;
import com.wuzhai.app.widget.TitleToolbarActivity;

public class MyWalletActivity extends TitleToolbarActivity {

	private Button rechargeBtn;
	private Button withdrawBtn;
	private TextView balance;
	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setTitle("我的钱包");
		setContentView(R.layout.activity_mywallet);
		 initView();
	}

	private void initView(){
		rechargeBtn = (Button)findViewById(R.id.recharge);
		rechargeBtn.setOnClickListener(listener);
		withdrawBtn = (Button)findViewById(R.id.withdraw);
		withdrawBtn.setOnClickListener(listener);
		balance = (TextView)findViewById(R.id.balance);
		balance.setText(((WuZhaiApplication)getApplication()).getUser().getBalance()+"宅币");
	}

	private OnClickListener listener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.recharge:
				MyWalletActivity.this.startActivity(new Intent(MyWalletActivity.this, RechargeActivity.class));
				break;

			case R.id.withdraw:
				MyWalletActivity.this.startActivity(new Intent(MyWalletActivity.this, WithdrawActivity.class));
				break;
			}
		}
	};
}
