package com.wuzhai.app.person.widget;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;
import com.wuzhai.app.main.widget.HyRecyclerViewAdapter;
import com.wuzhai.app.objects.Collectionable;

import java.util.ArrayList;

/**
 * Created by huangyue on 2016/11/10.
 */

public class CollectionAdapter extends HyRecyclerViewAdapter{

    private Context context;
    private ArrayList<Collectionable> collections = new ArrayList<>();

    public CollectionAdapter(Context context,ArrayList<Collectionable> collections){
        this.context = context;
        this.collections = collections;
    }
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ImageView view = new ImageView(context);
        RecyclerView.LayoutParams layoutParams = new RecyclerView.LayoutParams(RecyclerView.LayoutParams.MATCH_PARENT, parent.getWidth()/5*2);
        layoutParams.topMargin = 4;
        layoutParams.leftMargin=2;
        layoutParams.rightMargin=2;
        view.setLayoutParams(layoutParams);
        view.setScaleType(ImageView.ScaleType.CENTER_CROP);
        return new CollectionViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        CollectionViewHolder collectionHolder = (CollectionViewHolder)holder;
        if(!TextUtils.isEmpty(collections.get(position).getCollectionableIcon())){
            Picasso.with(context).load(collections.get(position).getCollectionableIcon()).centerInside().resize(300,200).into(collectionHolder.pic);
            collectionHolder.pic.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(recyclerViewItemClickListener!=null){
                        recyclerViewItemClickListener.onItemClick(v,position);
                    }
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return collections.size();
    }

    private class CollectionViewHolder extends RecyclerView.ViewHolder{
        public ImageView pic;

        public CollectionViewHolder(View itemView) {
            super(itemView);
            pic = (ImageView)itemView;
        }
    }
}
