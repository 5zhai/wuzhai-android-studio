package com.wuzhai.app.chat;

import java.util.Locale;
import io.rong.imkit.RongIM;
import io.rong.imkit.fragment.ConversationFragment;
import io.rong.imlib.RongIMClient;
import io.rong.imlib.model.Conversation;
import io.rong.imlib.model.Message;
import io.rong.imlib.model.MessageContent;
import io.rong.message.ImageMessage;
import io.rong.message.RichContentMessage;
import io.rong.message.TextMessage;
import io.rong.message.VoiceMessage;

import com.wuzhai.app.R;
import com.wuzhai.app.application.WuZhaiApplication;
import com.wuzhai.app.widget.TitleToolbarActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;

public class ConversationActivity extends TitleToolbarActivity {

	private String mTargetId;
	private String mTargetIds;
	private Conversation.ConversationType mConversationType;

	@Override
	protected void onCreate(@Nullable Bundle arg0) {
		super.onCreate(arg0);
		setContentView(R.layout.conversation);

        Intent intent = getIntent();
        getIntentDate(intent);
        isReconnect(intent);
	}

    private void getIntentDate(Intent intent) {

        mTargetId = intent.getData().getQueryParameter("targetId");
        mTargetIds = intent.getData().getQueryParameter("targetIds");
        Log.d("yue.huang", "type:"+intent.getData().getLastPathSegment());//获得当前会话类型
        mConversationType = Conversation.ConversationType.valueOf(intent.getData().getLastPathSegment().toUpperCase(Locale.getDefault()));
        enterFragment(mConversationType, mTargetId);
        RongIM.getInstance().setSendMessageListener(new MySendMessageListener());
    }

    private void enterFragment(Conversation.ConversationType mConversationType, String mTargetId) {
        ConversationFragment fragment = (ConversationFragment) getSupportFragmentManager().findFragmentById(R.id.conversation);
        Uri uri = Uri.parse("rong://" + getApplicationInfo().packageName).buildUpon()
                .appendPath("conversation").appendPath(mConversationType.getName().toLowerCase())
                .appendQueryParameter("targetId", mTargetId).build();

        fragment.setUri(uri);
    }

    /**
     * 判断消息是否是 push 消息
     */
    private void isReconnect(Intent intent) {

        String token = ((WuZhaiApplication)getApplication()).getUser().getImToken();

        //push或通知过来
        if (intent != null && intent.getData() != null && intent.getData().getScheme().equals("rong")) {

            //通过intent.getData().getQueryParameter("push") 为true，判断是否是push消息
            if (intent.getData().getQueryParameter("push") != null
                    && intent.getData().getQueryParameter("push").equals("true")) {

                reconnect(token);
            } else {
                //程序切到后台，收到消息后点击进入,会执行这里
                if (RongIM.getInstance() == null || RongIM.getInstance().getRongIMClient() == null) {

                    reconnect(token);
                } else {
                    enterFragment(mConversationType, mTargetId);
                }
            }
        }
    }

    /**
     * 重连
     *
     * @param token
     */
    @SuppressLint("NewApi")
	private void reconnect(String token) {

        if (getApplicationInfo().packageName.equals(WuZhaiApplication.getCurProcessName(getApplicationContext()))) {

            RongIM.connect(token, new RongIMClient.ConnectCallback() {
                @Override
                public void onTokenIncorrect() {

                }

                @Override
                public void onSuccess(String s) {

                    enterFragment(mConversationType, mTargetId);
                }

                @Override
                public void onError(RongIMClient.ErrorCode errorCode) {

                }
            });
        }
    }

	private class MySendMessageListener implements RongIM.OnSendMessageListener {

		/**
		 * 消息发送前监听器处理接口（是否发送成功可以从 SentStatus 属性获取）。
		 * 
		 * @param message
		 *            发送的消息实例。
		 * @return 处理后的消息实例。
		 */
		@Override
		public Message onSend(Message message) {
			// 开发者根据自己需求自行处理逻辑
			return message;
		}

		/**
		 * 消息在 UI 展示后执行/自己的消息发出后执行,无论成功或失败。
		 * 
		 * @param message
		 *            消息实例。
		 * @param sentMessageErrorCode
		 *            发送消息失败的状态码，消息发送成功 SentMessageErrorCode 为 null。
		 * @return true 表示走自已的处理方式，false 走融云默认处理方式。
		 */
		@Override
		public boolean onSent(Message message,
				RongIM.SentMessageErrorCode sentMessageErrorCode) {

			if (message.getSentStatus() == Message.SentStatus.FAILED) {
				if (sentMessageErrorCode == RongIM.SentMessageErrorCode.NOT_IN_CHATROOM) {
					Log.d("yue.huang", "1");
					// 不在聊天室
				} else if (sentMessageErrorCode == RongIM.SentMessageErrorCode.NOT_IN_DISCUSSION) {
					Log.d("yue.huang", "2");
					// 不在讨论组
				} else if (sentMessageErrorCode == RongIM.SentMessageErrorCode.NOT_IN_GROUP) {
					Log.d("yue.huang", "3");
					// 不在群组
				} else if (sentMessageErrorCode == RongIM.SentMessageErrorCode.REJECTED_BY_BLACKLIST) {
					Log.d("yue.huang", "4");
					// 你在他的黑名单中
				}
			}

			MessageContent messageContent = message.getContent();

			if (messageContent instanceof TextMessage) {// 文本消息
				TextMessage textMessage = (TextMessage) messageContent;
				Log.d("yue.huang",
						"onSent-TextMessage:" + textMessage.getContent());
			} else if (messageContent instanceof ImageMessage) {// 图片消息
				ImageMessage imageMessage = (ImageMessage) messageContent;
				Log.d("yue.huang",
						"onSent-ImageMessage:" + imageMessage.getRemoteUri());
			} else if (messageContent instanceof VoiceMessage) {// 语音消息
				VoiceMessage voiceMessage = (VoiceMessage) messageContent;
				Log.d("yue.huang", "onSent-voiceMessage:"
						+ voiceMessage.getUri().toString());
			} else if (messageContent instanceof RichContentMessage) {// 图文消息
				RichContentMessage richContentMessage = (RichContentMessage) messageContent;
				Log.d("yue.huang", "onSent-RichContentMessage:"
						+ richContentMessage.getContent());
			} else {
				Log.d("yue.huang", "onSent-其他消息，自己来判断处理");
			}

			return false;
		}
	}
}
