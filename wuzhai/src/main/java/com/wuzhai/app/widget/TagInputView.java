package com.wuzhai.app.widget;

import java.util.ArrayList;
import java.util.List;
import com.wuzhai.app.R;
import com.wuzhai.app.objects.Tag;
import com.wuzhai.app.tools.Utils;
import android.content.Context;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

public class TagInputView extends LinearLayout {

	private EditText inputEdit;
	private StringBuilder tagsString = new StringBuilder("");

	public TagInputView(Context context,ArrayList<Tag> tagsList) {
		super(context);
		initViwe(context,tagsList);
	}

	private void initViwe(Context context,ArrayList<Tag> tagsList){
		setOrientation(LinearLayout.VERTICAL);
		addTagLayout(context,tagsList);
		addEditText(context);
	}

	private void addTagLayout(Context context,List<Tag> tagsList){
		if (!tagsList.isEmpty()) {
			LinearLayout tagLayout = new LinearLayout(context);
			tagLayout.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
			tagLayout.setOrientation(LinearLayout.HORIZONTAL);
			for (int n = 0; n < tagsList.size(); n++) {
				tagLayout.addView(createTag(context, tagsList.get(n).getDesc()));
				if (tagLayout.getChildCount() == 7) {
					addTagLayout(context,tagsList.subList(7, tagsList.size() - 1));
					break;
				}
			}
			addView(tagLayout);
		}
	}

	private TextView createTag(Context context,String tag){
		TextView tagTextView = new TextView(context);
		LayoutParams layoutParams = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		layoutParams.setMargins(Utils.dpToPx(8),Utils.dpToPx(4),0,0);
		tagTextView.setLayoutParams(layoutParams);
		tagTextView.setText(tag);
		int padding = Utils.dpToPx(4);
		tagTextView.setPadding(padding, padding, padding, padding);
		tagTextView.setTextColor(0xffff7e9c);
		tagTextView.setTextSize(14);
		tagTextView.setBackgroundResource(R.drawable.photographer_lable_bg);
		tagTextView.setOnClickListener(tagClickListener);
		return tagTextView;
	}

	private void addEditText(Context context){
		inputEdit = new EditText(context);
		addView(inputEdit);
	}

	private OnClickListener tagClickListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			if(v.getTag()==null){
				((TextView)v).setTextColor(0xff626f85);
				((TextView)v).setTag(true);
				tagsString.append(((TextView)v).getText()+",");
				inputEdit.setText(tagsString);
			}else {
				((TextView)v).setTextColor(0xffff7e9c);
				((TextView)v).setTag(null);
				String text = ((TextView)v).getText().toString();
				int index = tagsString.indexOf(text);
				tagsString.delete(index, index+text.length()+1);
				inputEdit.setText(tagsString);
			}
		}
	};

	public String getInputString(){
		return inputEdit.getText().toString();
	}
}
