package com.wuzhai.app.widget;

import com.wuzhai.app.R;

import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class TitleToolbarActivity extends AppCompatActivity {
	private LinearLayout parentLinearLayout;
	private Toolbar toolbar;
	private TextView title;
	private LinearLayout toolbarBack;
	private TextView toolbarBackText;
	private TextView menuTextView;
	private ImageView menuImageView;

	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		initContentView(R.layout.activity_title_toolbar);
		initToolbar();
	}

	private void initContentView(int layoutResID) {
		ViewGroup viewGroup = (ViewGroup) findViewById(android.R.id.content);
		viewGroup.removeAllViews();
		parentLinearLayout = (LinearLayout) LayoutInflater.from(this).inflate(
				layoutResID, null);
		viewGroup.addView(parentLinearLayout);
	}

	@Override
	public void setContentView(@LayoutRes int layoutResID) {
		LayoutInflater.from(this)
				.inflate(layoutResID, parentLinearLayout, true);
	}

	@Override
	public void setContentView(View view, LayoutParams params) {
		parentLinearLayout.addView(view, params);
	}

	private void initToolbar() {
		toolbar = (Toolbar)findViewById(R.id.toolbar);
		title = (TextView) findViewById(R.id.toolbar_title);
		toolbarBackText = (TextView) findViewById(R.id.toolbar_back_text);
		menuTextView = (TextView) findViewById(R.id.toolbar_text_menu);
		menuImageView = (ImageView) findViewById(R.id.toolbar_img_menu);
		toolbarBack = (LinearLayout) findViewById(R.id.toolbar_back);
		toolbarBack.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				TitleToolbarActivity.this.onBackPressed();
			}
		});
	}

	public void setTitle(String titleString) {
		title.setText(titleString);
	}

	public void setBackText(String backString) {
		toolbarBackText.setText(backString);
	}

	public void setTextMenuString(String textString) {
		menuTextView.setVisibility(View.VISIBLE);
		menuTextView.setText(textString);
	}

	public void setTextMenuClickListener(OnClickListener listener) {
		menuTextView.setOnClickListener(listener);
	}

	public void setImgMenuResource(int ResourcesId) {
		menuImageView.setVisibility(View.VISIBLE);
		menuImageView.setImageResource(ResourcesId);
	}

	public void setImgMenuClickListener(OnClickListener listener) {
		menuImageView.setOnClickListener(listener);
	}

	public void setToolbarVisibility(int visibility){
		toolbar.setVisibility(visibility);
	}
}
