package com.wuzhai.app.main.publish.activity;

import java.util.ArrayList;
import java.util.HashMap;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import cn.sharesdk.framework.Platform;
import cn.sharesdk.framework.PlatformActionListener;
import cn.sharesdk.sina.weibo.SinaWeibo;
import cn.sharesdk.tencent.qq.QQ;
import cn.sharesdk.tencent.qzone.QZone;
import cn.sharesdk.wechat.friends.Wechat;
import com.wuzhai.app.R;
import com.wuzhai.app.application.WuZhaiApplication;
import com.wuzhai.app.main.WuzhaiService;
import com.wuzhai.app.main.WuzhaiServiceCallbackAdapter;
import com.wuzhai.app.objects.Tag;
import com.wuzhai.app.tools.Utils;
import com.wuzhai.app.widget.TitleToolbarActivity;

public class ReleaseActivity extends TitleToolbarActivity implements OnClickListener{
	private WuzhaiService service;
	private LinearLayout picAddedLayout;
	private ImageView addPicImageView;
	private RelativeLayout pohotTypeRelativeLayout;
	private TextView photoTypeTextView;
	private EditText publishTitleEditText;
	private RelativeLayout lableRelativeLayout;
	private TextView photoLableTextView;
	private EditText pictureInfoEditText;
	private Button publishBtn;
	private Button publishShareBtn;
	private ImageView shareQQ;
	private ImageView shareQZONE;
	private ImageView shareSINA;
	private ImageView shareWECHAT;
	private LinearLayout progressView;
	private ArrayList<String> bitmapPathList = new ArrayList<String>();
	private ArrayList<Tag> photoTagsList;
	private HashMap<Integer, String> photoTypeMap;
	private String[] photoTypes;
	private ArrayList<String> sharePlatFormList = new ArrayList<String>();
	private String publishType;
	private String videoPathToRelease;

	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		publishType = getIntent().getStringExtra("type");
		
		setContentView(R.layout.activity_releasepicture);
		
		service = ((WuZhaiApplication)getApplication()).getService();
		service.setCallBack(callbackAdapter);
		picAddedLayout = (LinearLayout)findViewById(R.id.pic_added_layout);
		addPicImageView = (ImageView)findViewById(R.id.addMedia);
		pohotTypeRelativeLayout = (RelativeLayout)findViewById(R.id.photo_type_layout);
		pohotTypeRelativeLayout.setOnClickListener(this);
		photoTypeTextView = (TextView)findViewById(R.id.photo_type);
		publishTitleEditText = (EditText)findViewById(R.id.publish_title);
		lableRelativeLayout = (RelativeLayout)findViewById(R.id.picture_lable_layout);
		lableRelativeLayout.setOnClickListener(this);
		photoLableTextView = (TextView)findViewById(R.id.picture_lable);
		pictureInfoEditText = (EditText)findViewById(R.id.picture_info);
		publishBtn = (Button)findViewById(R.id.publish_btn);
		publishShareBtn = (Button)findViewById(R.id.publish_share_btn);
		shareQQ = (ImageView)findViewById(R.id.share_qq);
		shareQZONE = (ImageView)findViewById(R.id.share_qzone);
		shareSINA = (ImageView)findViewById(R.id.share_sina);
		shareWECHAT = (ImageView)findViewById(R.id.share_wechat);
		progressView = (LinearLayout)findViewById(R.id.progressView);
		
		shareQQ.setOnClickListener(this);
		shareQZONE.setOnClickListener(this);
		shareSINA.setOnClickListener(this);
		shareWECHAT.setOnClickListener(this);
		addPicImageView.setOnClickListener(this);
		publishBtn.setOnClickListener(this);
		publishShareBtn.setOnClickListener(this);
		
		if("photo".equals(publishType)){
			setTitle("发布照片");
			addPicImageView.setImageResource(R.drawable.add_photos);
			service.getType(WuzhaiService.PHOTO_TYPE);
			service.getTags(WuzhaiService.TAG_PHOTO);
		}else {
			setTitle("发布视频");
			addPicImageView.setImageResource(R.drawable.add_videos);
			pohotTypeRelativeLayout.setVisibility(View.GONE);
			service.getTags(WuzhaiService.TAG_VIDEO);
		}

	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.addMedia:
			if("photo".equals(publishType)){
				selectPictures();
			}else {
				selectVideo();
			}
			break;
		case R.id.photo_type_layout:
			int checkedItem = 0;
			if (photoTypeMap != null) {
				for (int n = 0; n < photoTypes.length; n++) {
					if (photoTypes[n].equals(photoTypeTextView.getText()
							.toString())) {
						checkedItem = n;
					}
				}
				Utils.showSingleChoiceDialog(this, "图片类型", photoTypeTextView,photoTypes, checkedItem);
			}
			break;
		case R.id.picture_lable_layout:
			Utils.showTagInputDialog(this, photoTagsList, photoLableTextView, "标签");
			break;
		case R.id.publish_btn:
			publish();
			break;
		case R.id.publish_share_btn:
			if(publish()){
				shareOperation();
			}
			break;
		case R.id.share_qq:
//			if(shareQQ.getTag() == null){
//				shareQQ.setImageResource(R.drawable.share_qq_press);
//				shareQQ.setTag(1);
//				sharePlatFormList.add(QQ.NAME);
//			}else {
//				shareQQ.setImageResource(R.drawable.share_qq_normal);
//				shareQQ.setTag(null);
//				sharePlatFormList.remove(QQ.NAME);
//			}
			setShareBtnSelected(R.id.share_qq);
			break;
		case R.id.share_qzone:
//			if(shareQZONE.getTag() == null){
//				shareQZONE.setImageResource(R.drawable.share_qzone_press);
//				shareQZONE.setTag(1);
//				sharePlatFormList.add(QZone.NAME);
//			}else {
//				shareQZONE.setImageResource(R.drawable.share_qzone_normal);
//				shareQZONE.setTag(null);
//				sharePlatFormList.remove(QZone.NAME);
//			}
			setShareBtnSelected(R.id.share_qzone);
			break;
		case R.id.share_sina:
//			if(shareSINA.getTag() == null){
//				shareSINA.setImageResource(R.drawable.share_sina_press);
//				shareSINA.setTag(1);
//				sharePlatFormList.add(SinaWeibo.NAME);
//			}else {
//				shareSINA.setImageResource(R.drawable.share_sina_normal);
//				shareSINA.setTag(null);
//				sharePlatFormList.remove(SinaWeibo.NAME);
//			}
			setShareBtnSelected(R.id.share_sina);
			break;
		case R.id.share_wechat:
//			if(shareWECHAT.getTag() == null){
//				shareWECHAT.setImageResource(R.drawable.share_wechat_press);
//				shareWECHAT.setTag(1);
//				sharePlatFormList.add(Wechat.NAME);
//			}else {
//				shareWECHAT.setImageResource(R.drawable.share_wechat_normal);
//				shareWECHAT.setTag(null);
//				sharePlatFormList.remove(Wechat.NAME);
//			}
			setShareBtnSelected(R.id.share_wechat);
			break;
		}
	}

	private void selectPictures(){
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("image/*");
        // intent.putExtra("crop", "circle");
        intent.putExtra("return-data", true);
        startActivityForResult(intent, 1);
	}

	private void selectVideo(){
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("video/*");
        // intent.putExtra("crop", "circle");
        intent.putExtra("return-data", true);
        startActivityForResult(intent, 2);
	}
	
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
		if (resultCode == RESULT_OK) {
			Uri uri = data.getData();
			if (requestCode == 1) {
				Bitmap bitmap = Utils.getBitmapFromURI(this, uri, true);
				addPicImageView.setImageBitmap(bitmap);
				bitmapPathList.add(Utils.uriToPath(this, uri));
			} else {
				Log.d("yue.huang", "path-------:"+Utils.uriToPath(this, uri));
				videoPathToRelease = Utils.uriToPath(this, uri);
				if(!TextUtils.isEmpty(videoPathToRelease)){
					Toast.makeText(this, "添加视频成功", Toast.LENGTH_SHORT).show();
				}
			}

		}
    }

    private boolean publish(){
		String photoType = photoTypeTextView.getText().toString();
		String photoLable = photoLableTextView.getText().toString();
		String title = publishTitleEditText.getText().toString();
		String desc = pictureInfoEditText.getText().toString();
		Log.d("yue.huang", "publish_title:"+publishTitleEditText.getText().toString());
		if (TextUtils.isEmpty(title) || TextUtils.isEmpty(desc) || photoLable.startsWith("请")||(pohotTypeRelativeLayout.getVisibility()==View.VISIBLE && photoType.startsWith("请"))) {
			Toast.makeText(this, "信息不完整", Toast.LENGTH_SHORT).show();
			return false;
		}
		if (!Utils.isNetworkAvailable(this)) {
			Toast.makeText(this, "无网络连接", Toast.LENGTH_SHORT).show();
			return false;
		}
		if ("photo".equals(publishType)) {
            if (pohotTypeRelativeLayout.getVisibility()==View.VISIBLE && bitmapPathList.size() == 0) {
                Toast.makeText(this, "请添加图片", Toast.LENGTH_SHORT).show();
                return false;
            }
//			int type = Utils.getIntTypeFromString(photoTypeMap, photoType);
			//取消图片类型选择
			int type = 1;
			if (type == -1) {
				Toast.makeText(this, "图片类型错误", Toast.LENGTH_SHORT).show();
				return false;
			}
			progressView.setVisibility(View.VISIBLE);
			service.publishPic(type, title, desc, bitmapPathList, photoLable,"35.567,68.678");
		}else {
            if(pohotTypeRelativeLayout.getVisibility()!=View.VISIBLE && TextUtils.isEmpty(videoPathToRelease)){
                Toast.makeText(this, "请添加视频", Toast.LENGTH_SHORT).show();
                return false;
            }
			progressView.setVisibility(View.VISIBLE);
			service.publishVideo(this,videoPathToRelease,title,desc,photoLable,"35.567,68.678");
		}
		return true;
    }

	private WuzhaiServiceCallbackAdapter callbackAdapter = new WuzhaiServiceCallbackAdapter(){

		public void onUploadFileCompleted(java.util.ArrayList<String> picPathList){
			progressView.setVisibility(View.GONE);
			if(picPathList.size()==0){
				Toast.makeText(ReleaseActivity.this, "发布失败，请重试", Toast.LENGTH_SHORT).show();
			}else {
				Toast.makeText(ReleaseActivity.this, "发布成功", Toast.LENGTH_SHORT).show();
			}
		}

		@Override
		public void onGetTagsCompleted(ArrayList<Tag> tagsList) {
			photoTagsList = tagsList;
		}

		@Override
		public void onGetTypeCompleted(HashMap<Integer, String> typeMap) {
			if (typeMap != null) {
				photoTypeMap = typeMap;
				photoTypes = typeMap.values().toArray(new String[typeMap.values().size()]);
			}
		};
	};
private int shareCount = 0;
private int shareIndex = 0;
	private void shareOperation(){
//		if(shareQQ.getTag()!=null){
//			sharePic(QQ.NAME);
//		}
//		if(shareQZONE.getTag()!=null){
//			sharePic(QZone.NAME);
//		}
//		if(shareSINA.getTag()!=null){
//			sharePic(SinaWeibo.NAME);
//		}
//		if(shareWECHAT.getTag()!=null){
//			sharePic(Wechat.NAME);
//		}
		if(sharePlatFormList.size()!=0){
			shareCount = sharePlatFormList.size();
			sharePic(sharePlatFormList.get(shareIndex));
		}

	}

	private PlatformActionListener actionListener = new PlatformActionListener() {

		@Override
		public void onError(Platform arg0, int arg1, Throwable arg2) {
			Toast.makeText(ReleaseActivity.this, "分享到"+arg0.getName()+"失败", Toast.LENGTH_SHORT).show();
		}

		@Override
		public void onComplete(Platform arg0, int arg1, HashMap<String, Object> arg2) {
			Toast.makeText(ReleaseActivity.this, "分享到"+arg0.getName()+"成功", Toast.LENGTH_SHORT).show();
			shareIndex++;
			if(shareCount<=shareIndex){
				shareIndex = 0;
				return;
			}
			Log.d("yue.huang", "shareIndex:"+shareIndex);
			sharePic(sharePlatFormList.get(shareIndex));
		}

		@Override
		public void onCancel(Platform arg0, int arg1) {
			Toast.makeText(ReleaseActivity.this, "分享到"+arg0.getName()+"取消", Toast.LENGTH_SHORT).show();
//			shareIndex++;
//			if(shareCount<=shareIndex){
//				shareIndex = 0;
//				return;
//			}
//			Log.d("yue.huang", "shareIndex:"+shareIndex);
//			sharePic(sharePlatFormList.get(shareIndex));
		}
	};

	private void sharePic(String platformName){
//		QQ空间分享时一定要携带title、titleUrl、site、siteUrl不然没反应
		Utils.onePlatformShare(this,platformName, actionListener, publishTitleEditText.getText().toString(),
				"https://www.5yuzhai.com/", pictureInfoEditText.getText().toString(), "https://www.5yuzhai.com/", bitmapPathList.get(0), "吾宅", "https://www.5yuzhai.com/");
	}

	private void setShareBtnSelected(int btnId) {
		switch (btnId) {
		case R.id.share_wechat:
			shareWECHAT.setImageResource(R.drawable.share_wechat_press);
			shareWECHAT.setTag(1);
			sharePlatFormList.add(Wechat.NAME);

			shareSINA.setImageResource(R.drawable.share_sina_normal);
			shareSINA.setTag(null);
			sharePlatFormList.remove(SinaWeibo.NAME);
			shareQZONE.setImageResource(R.drawable.share_qzone_normal);
			shareQZONE.setTag(null);
			sharePlatFormList.remove(QZone.NAME);
			shareQQ.setImageResource(R.drawable.share_qq_normal);
			shareQQ.setTag(null);
			sharePlatFormList.remove(QQ.NAME);
			break;

		case R.id.share_sina:
			shareSINA.setImageResource(R.drawable.share_sina_press);
			shareSINA.setTag(1);
			sharePlatFormList.add(SinaWeibo.NAME);

			shareWECHAT.setImageResource(R.drawable.share_wechat_normal);
			shareWECHAT.setTag(null);
			sharePlatFormList.remove(Wechat.NAME);
			shareQZONE.setImageResource(R.drawable.share_qzone_normal);
			shareQZONE.setTag(null);
			sharePlatFormList.remove(QZone.NAME);
			shareQQ.setImageResource(R.drawable.share_qq_normal);
			shareQQ.setTag(null);
			sharePlatFormList.remove(QQ.NAME);
			break;
		case R.id.share_qq:
			shareQQ.setImageResource(R.drawable.share_qq_press);
			shareQQ.setTag(1);
			sharePlatFormList.add(QQ.NAME);

			shareSINA.setImageResource(R.drawable.share_sina_normal);
			shareSINA.setTag(null);
			sharePlatFormList.remove(SinaWeibo.NAME);
			shareQZONE.setImageResource(R.drawable.share_qzone_normal);
			shareQZONE.setTag(null);
			sharePlatFormList.remove(QZone.NAME);
			shareWECHAT.setImageResource(R.drawable.share_wechat_normal);
			shareWECHAT.setTag(null);
			sharePlatFormList.remove(Wechat.NAME);
			break;
		case R.id.share_qzone:
			shareQZONE.setImageResource(R.drawable.share_qzone_press);
			shareQZONE.setTag(1);
			sharePlatFormList.add(QZone.NAME);

			shareSINA.setImageResource(R.drawable.share_sina_normal);
			shareSINA.setTag(null);
			sharePlatFormList.remove(SinaWeibo.NAME);
			shareQQ.setImageResource(R.drawable.share_qq_normal);
			shareQQ.setTag(null);
			sharePlatFormList.remove(QQ.NAME);
			shareWECHAT.setImageResource(R.drawable.share_wechat_normal);
			shareWECHAT.setTag(null);
			sharePlatFormList.remove(Wechat.NAME);
			break;
		}
	}
}
