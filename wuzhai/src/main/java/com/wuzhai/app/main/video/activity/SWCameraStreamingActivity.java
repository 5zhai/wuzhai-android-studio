package com.wuzhai.app.main.video.activity;

import org.json.JSONException;
import org.json.JSONObject;

import com.qiniu.pili.droid.streaming.AVCodecType;
import com.qiniu.pili.droid.streaming.CameraStreamingSetting;
import com.qiniu.pili.droid.streaming.MediaStreamingManager;
import com.qiniu.pili.droid.streaming.StreamingEnv;
import com.qiniu.pili.droid.streaming.StreamingProfile;
import com.qiniu.pili.droid.streaming.StreamingState;
import com.qiniu.pili.droid.streaming.StreamingStateChangedListener;
import com.qiniu.pili.droid.streaming.widget.AspectFrameLayout;
import com.wuzhai.app.R;
import com.wuzhai.app.application.WuZhaiApplication;
import com.wuzhai.app.main.WuzhaiService;
import com.wuzhai.app.main.widget.ChatRoomFragment;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.hardware.Camera;
import android.opengl.GLSurfaceView;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.AccelerateInterpolator;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

public class SWCameraStreamingActivity extends AppCompatActivity implements
        StreamingStateChangedListener {
    private WuzhaiService service;
    private JSONObject mJSONObject;
    private MediaStreamingManager mMediaStreamingManager;
    private StreamingProfile mProfile;
    private ImageView closeBtn;
    private ImageView cameraBtn;
    private boolean isMute = false;
    private String roomId;
    private ChatRoomFragment chatRoomFragment;
    private LinearLayout menuLayout;
    private LinearLayout menuContentLayout;
    private ImageView micBtn;
    private ImageView wordsBtn;
    private ImageView chatBtn;
    private TextView faceBeautyBtn;
    private ImageView menuBackBtn;
    private CameraStreamingSetting cameraSetting;

    private RelativeLayout faceBeautyAdjustmentLayout;
    private SeekBar  faceBeautyVBar;
    private SeekBar  faceBeautyV1Bar;
    private SeekBar  faceBeautyV2Bar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        service = ((WuZhaiApplication) getApplication()).getService();
        setContentView(R.layout.activity_swcamera_streaming);
        roomId = getIntent().getStringExtra("roomId");

        //以下为直播相关代码
        StreamingEnv.init(getApplicationContext());
        AspectFrameLayout afl = (AspectFrameLayout) findViewById(R.id.cameraPreview_afl);

        // Decide FULL screen or real size
        afl.setShowMode(AspectFrameLayout.SHOW_MODE.FULL);
        GLSurfaceView glSurfaceView = (GLSurfaceView) findViewById(R.id.cameraPreview_surfaceView);

        String streamJsonStrFromServer = getIntent().getStringExtra(
                "stream_json_str");
        try {
            mJSONObject = new JSONObject(streamJsonStrFromServer);
            StreamingProfile.Stream stream = new StreamingProfile.Stream(
                    mJSONObject);
            mProfile = new StreamingProfile();
            mProfile.setVideoQuality(StreamingProfile.VIDEO_QUALITY_HIGH1)
                    .setAudioQuality(StreamingProfile.AUDIO_QUALITY_MEDIUM2)
                    .setEncodingSizeLevel(
                            StreamingProfile.VIDEO_ENCODING_HEIGHT_480)
                    .setEncoderRCMode(
                            StreamingProfile.EncoderRCModes.BITRATE_PRIORITY)
                    .setAdaptiveBitrateEnable(true)
                    .setFpsControllerEnable(true)
                    .setStreamStatusConfig(new StreamingProfile.StreamStatusConfig(3))
                    .setSendingBufferProfile(new StreamingProfile.SendingBufferProfile(0.2f, 0.8f, 3.0f, 20 * 1000))
                    .setStream(stream); // You can invoke this before
            // startStreaming, but not in
            // initialization phase.

            cameraSetting = new CameraStreamingSetting();
            cameraSetting.setCameraId(Camera.CameraInfo.CAMERA_FACING_BACK)
                    .setContinuousFocusModeEnabled(true)
                    .setCameraPrvSizeLevel(
                            CameraStreamingSetting.PREVIEW_SIZE_LEVEL.MEDIUM)
                    .setCameraPrvSizeRatio(
                            CameraStreamingSetting.PREVIEW_SIZE_RATIO.RATIO_16_9)
                    .setRecordingHint(false)
                    .setBuiltInFaceBeautyEnabled(true)
                    .setFaceBeautySetting(new CameraStreamingSetting.FaceBeautySetting(1.0f, 0.5f, 0.5f))
                    .setVideoFilter(CameraStreamingSetting.VIDEO_FILTER_TYPE.VIDEO_FILTER_BEAUTY);;



            mMediaStreamingManager = new MediaStreamingManager(
                    this,
                    afl,
                    glSurfaceView,
                    AVCodecType.SW_VIDEO_WITH_SW_AUDIO_CODEC); // soft
            // codec
            mMediaStreamingManager.setVideoFilterType(CameraStreamingSetting.VIDEO_FILTER_TYPE.VIDEO_FILTER_BEAUTY);
            mMediaStreamingManager.prepare(cameraSetting, mProfile);
            mMediaStreamingManager.setStreamingStateListener(this);

            initView();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void initView() {
        closeBtn = (ImageView) findViewById(R.id.close_pic);
        cameraBtn = (ImageView) findViewById(R.id.camera_pic);
        menuLayout = (LinearLayout) findViewById(R.id.menu_layout);
        menuContentLayout = (LinearLayout) findViewById(R.id.menu_content_layout);
        micBtn = (ImageView) findViewById(R.id.mic_pic);
        wordsBtn = (ImageView) findViewById(R.id.words_pic);
        chatBtn = (ImageView) findViewById(R.id.chat_pic);
        faceBeautyBtn = (TextView)findViewById(R.id.face_beauty_TV);
        menuBackBtn = (ImageView) findViewById(R.id.menu_back_pic);
        chatRoomFragment = (ChatRoomFragment) getSupportFragmentManager().findFragmentById(R.id.conversation);
        faceBeautyAdjustmentLayout = (RelativeLayout)findViewById(R.id.seekbar_layout);
        faceBeautyVBar = (SeekBar)findViewById(R.id.seekbar_one);
        faceBeautyV1Bar = (SeekBar)findViewById(R.id.seekbar_two);
        faceBeautyV2Bar = (SeekBar)findViewById(R.id.seekbar_three);

        initFaceBeautySeekBarDefaultData();
        faceBeautyVBar.setOnSeekBarChangeListener(seekBarChangeListener);
        faceBeautyV1Bar.setOnSeekBarChangeListener(seekBarChangeListener);
        faceBeautyV2Bar.setOnSeekBarChangeListener(seekBarChangeListener);
        chatRoomFragment.setRoomId(roomId);
        closeBtn.setOnClickListener(clickListener);
        cameraBtn.setOnClickListener(clickListener);
        micBtn.setOnClickListener(clickListener);
        wordsBtn.setOnClickListener(clickListener);
        chatBtn.setOnClickListener(clickListener);
        faceBeautyBtn.setOnClickListener(clickListener);
        menuBackBtn.setOnClickListener(clickListener);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mMediaStreamingManager.resume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        // You must invoke pause here.
        mMediaStreamingManager.pause();
    }

    private OnClickListener clickListener = new OnClickListener() {

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.close_pic:
                    onBackPressed();
                    break;

                case R.id.camera_pic:
                    mMediaStreamingManager.switchCamera();
                    break;

                case R.id.mic_pic:
                    if (isMute) {
                        mMediaStreamingManager.mute(false);
                        micBtn.setImageResource(R.drawable.icon_mic);
                    } else {
                        mMediaStreamingManager.mute(true);
                        micBtn.setImageResource(R.drawable.icon_no_mic);
                    }
                    isMute = !isMute;
                    break;
                case R.id.words_pic:
                    switchChatPanel();
                    break;
                case R.id.chat_pic:
                    switchChatInputET();
                    break;
                case R.id.face_beauty_TV:
                    switchFaceBeautyPanel();
                    break;
                case R.id.menu_back_pic:
                    switchMenuLayout(v);
                    break;
            }
        }
    };


    /**
     * 切换聊天页的显示状态
     */
    private void switchChatPanel() {
        if(faceBeautyAdjustmentLayout.getVisibility() == View.VISIBLE){
            switchFaceBeautyPanel();
        }
        if (chatRoomFragment.isHidden()) {
            getSupportFragmentManager().beginTransaction().show(chatRoomFragment).commit();
            wordsBtn.setImageResource(R.drawable.icon_words);
        } else {
            getSupportFragmentManager().beginTransaction().hide(chatRoomFragment).commit();
            wordsBtn.setImageResource(R.drawable.icon_no_words);
        }
    }

    /**
     * 切换聊天输入框的显示状态
     */
    private void switchChatInputET() {
        if (chatRoomFragment.isEditLayoutShown()) {
            chatRoomFragment.hideEditLayout();
        } else {
            chatRoomFragment.showEditLayout();
        }
    }
    /**
     * 切换美颜调节框的显示状态
     */
    private void switchFaceBeautyPanel(){
        if(faceBeautyAdjustmentLayout.getVisibility() == View.GONE){
            if(chatRoomFragment.isVisible()){
                switchChatPanel();
            }
            faceBeautyAdjustmentLayout.setVisibility(View.VISIBLE);
        }else {
            faceBeautyAdjustmentLayout.setVisibility(View.GONE);
        }
    }
    /**
     * 切换右下角菜单布局的显示状态
     */
    private void switchMenuLayout(View switchBtn) {
        float targetRotation = switchBtn.getRotation();
        float targetY = 0;
        if (targetRotation == 0) {
            targetRotation = 360;
            targetY = menuContentLayout.getHeight();
        } else {
            targetRotation = 0;
            targetY = 0;
        }
        switchBtn.animate().rotation(targetRotation).setDuration(500).start();
        menuContentLayout.animate().translationY(targetY).setInterpolator(new AccelerateInterpolator(1))
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationStart(Animator animation) {
                        menuContentLayout.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onAnimationEnd(Animator animation) {
                        if (menuContentLayout.getTranslationY() != 0) {
                            menuContentLayout.setVisibility(View.GONE);
                        }

                    }
                }).setDuration(500).start();
    }


    private void initFaceBeautySeekBarDefaultData(){
        CameraStreamingSetting.FaceBeautySetting setting = cameraSetting.getFaceBeautySetting();
        faceBeautyVBar.setProgress((int)(setting.beautyLevel*100));
        faceBeautyV1Bar.setProgress((int)(setting.redden*100));
        faceBeautyV2Bar.setProgress((int)(setting.whiten*100));
    }


    @Override
    public void onBackPressed() {
        String acessKey = ((WuZhaiApplication)getApplication()).getUser().getAccessKey();
        service.closeLiveRoom(acessKey,roomId);
        super.onBackPressed();
    }

    private SeekBar.OnSeekBarChangeListener seekBarChangeListener = new SeekBar.OnSeekBarChangeListener() {
        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            CameraStreamingSetting.FaceBeautySetting faceBeautySetting = cameraSetting.getFaceBeautySetting();
            switch (seekBar.getId()){
                case R.id.seekbar_one:
                    faceBeautySetting.beautyLevel = progress/100f;
                    break;
                case R.id.seekbar_two:
                    faceBeautySetting.whiten = progress/100f;
                    break;
                case R.id.seekbar_three:
                    faceBeautySetting.redden = progress/100f;
                    break;
            }
            Log.d("yue.huang","beautyLevel:"+faceBeautySetting.beautyLevel);
            Log.d("yue.huang","whiten:"+faceBeautySetting.whiten);
            Log.d("yue.huang","redden:"+faceBeautySetting.redden);
            mMediaStreamingManager.updateFaceBeautySetting(faceBeautySetting);

        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {

        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {

        }
    };

    @Override
    public void onStateChanged(StreamingState streamingState, Object o) {
        Log.i("yue.huang", "StreamingState streamingState:" + streamingState + ",extra:" + o);

        switch (streamingState) {
            case PREPARING:
                Log.d("yue.huang","living_onStateChanged:"+"PREPARING");
                break;
            case READY:
                Log.d("yue.huang","living_onStateChanged:"+"READY");
//                mIsReady = true;
//                mMaxZoom = mMediaStreamingManager.getMaxZoom();
                // start streaming when READY
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        // disable the shutter button before startStreaming
                        boolean res = mMediaStreamingManager.startStreaming();
                    }
                }).start();
                break;
            case CONNECTING:
                Log.d("yue.huang","living_onStateChanged:"+"CONNECTING");
                break;
            case STREAMING:
                Log.d("yue.huang","living_onStateChanged:"+"STREAMING");
//                setShutterButtonEnabled(true);
//                setShutterButtonPressed(true);
                break;
            case SHUTDOWN:
                Log.d("yue.huang","living_onStateChanged:"+"READY");
//                setShutterButtonEnabled(true);
//                setShutterButtonPressed(false);
//                if (mOrientationChanged) {
//                    mOrientationChanged = false;
//                    startStreaming();
//                }
                break;
            case IOERROR:
                Log.d("yue.huang","living_onStateChanged:"+"READY");
                break;
            case UNKNOWN:
                Log.d("yue.huang","living_onStateChanged:"+"READY");
                break;
            case SENDING_BUFFER_EMPTY:
                break;
            case SENDING_BUFFER_FULL:
                break;
            case AUDIO_RECORDING_FAIL:
                break;
            case OPEN_CAMERA_FAIL:
                Log.e("yue.huang", "Open Camera Fail. id:" + o);
                break;
            case DISCONNECTED:
                break;
            case INVALID_STREAMING_URL:
                Log.e("yue.huang", "Invalid streaming url:" + o);
                break;
            case UNAUTHORIZED_STREAMING_URL:
                Log.e("yue.huang", "Unauthorized streaming url:" + o);
                break;
            case CAMERA_SWITCHED:
//                mShutterButtonPressed = false;
                if (o != null) {
                    Log.i("yue.huang", "current camera id:" + (Integer) o);
                }
                Log.i("yue.huang", "camera switched");
//                final int currentCamId = (Integer)o;
//                this.runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        updateCameraSwitcherButtonText(currentCamId);
//                    }
//                });
                break;
            case TORCH_INFO:
                if (o != null) {
                    final boolean isSupportedTorch = (Boolean) o;
                    Log.i("yue.huang", "isSupportedTorch=" + isSupportedTorch);
//                    this.runOnUiThread(new Runnable() {
//                        @Override
//                        public void run() {
//                            if (isSupportedTorch) {
//                                mTorchBtn.setVisibility(View.VISIBLE);
//                            } else {
//                                mTorchBtn.setVisibility(View.GONE);
//                            }
//                        }
//                    });
                }
                break;
        }
    }
}
