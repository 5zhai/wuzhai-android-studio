package com.wuzhai.app.main.publish.widget;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.view.View;
import android.view.View.OnAttachStateChangeListener;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.OvershootInterpolator;

public class ReleaseViewManager implements OnAttachStateChangeListener{

	public interface OnAnimationEndListener{
		void onAnimationEnd();
	}
	private OnAnimationEndListener animationEndListener;
	private ReleaseView releaseView;
	private boolean isShowing = false;
	private boolean isAnimating = false;
	public ReleaseViewManager(Context context){
		releaseView = new ReleaseView(context);
	}
	@Override
	public void onViewAttachedToWindow(View v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onViewDetachedFromWindow(View v) {
		
	}
	public void toggleReleaseViewFromView(View openingView) {
	    if (!isShowing && openingView!=null){
	        showReleaseViewFromView(openingView);
	    } else {
	        hideReleaseView();
	    }
	}
	
	private void showReleaseViewFromView(final View openingView) {
		if(!isAnimating){
			isShowing = true;
			isAnimating = true;
	        releaseView.addOnAttachStateChangeListener(this);
	        ((ViewGroup) openingView.getRootView().findViewById(android.R.id.content)).addView(releaseView);
	          
	        releaseView.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
	            @Override
	            public boolean onPreDraw() {
	                releaseView.getViewTreeObserver().removeOnPreDrawListener(this);
	                setupReleaseViewInitialPosition(openingView);
	                performShowAnimation();
	                return false;
	            }
	        });
		}
	}
	private void setupReleaseViewInitialPosition(View openingView) {
	    final int[] openingViewLocation = new int[2];
	    openingView.getLocationOnScreen(openingViewLocation);
	    int additionalBottomMargin = openingView.getHeight()/2;
	    float openingViewCenterX = openingViewLocation[0]+openingView.getWidth()/2f;
	    releaseView.setTranslationX(openingViewCenterX - releaseView.getWidth()/2f);
	    releaseView.setTranslationY(openingViewLocation[1] - releaseView.getHeight() - additionalBottomMargin);
	}
	private void performShowAnimation(){
		releaseView.setPivotX(releaseView.getWidth()/2);
		releaseView.setPivotY(releaseView.getHeight());
		releaseView.setScaleX(0.1f);
		releaseView.setScaleY(0.1f);
		releaseView.animate().scaleX(1f).scaleY(1f).setDuration(150)
		.setInterpolator(new OvershootInterpolator())
		.setListener(new AnimatorListenerAdapter() {
			@Override
			public void onAnimationEnd(Animator animation) {
				isAnimating = false;
				if(animationEndListener!=null){
					animationEndListener.onAnimationEnd();
				}
			}
		});
	}
	private void hideReleaseView(){
		if(!isAnimating){
			isShowing = false;
			isAnimating = true;
			performHideAnimation();
		}
	}
	private void performHideAnimation(){
		releaseView.setPivotX(releaseView.getWidth()/2);
		releaseView.setPivotY(releaseView.getHeight());
		releaseView.animate().scaleX(0.1f).scaleY(0.1f)
		.setInterpolator(new AccelerateInterpolator())
		.setListener(new AnimatorListenerAdapter() {
			@Override
			public void onAnimationEnd(Animator animation) {
				isAnimating = false;
				releaseView.dismiss();
			}
		});
	}

	/**
	 * @return 弹出视图是否处于显示状态
	 */
	public boolean isReleaseViewShowing(){
		return isShowing;
	}

	/**
	 * @param listener 动画结束事件监听
	 */
	public void setAnimationEndListener(OnAnimationEndListener listener){
		animationEndListener = listener;
	}
}
