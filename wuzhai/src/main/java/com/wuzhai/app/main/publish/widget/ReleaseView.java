package com.wuzhai.app.main.publish.widget;

import com.wuzhai.app.R;
import com.wuzhai.app.main.publish.activity.ReleaseActivity;
import com.wuzhai.app.main.video.activity.LiveActivity;
import com.wuzhai.app.tools.Utils;
import android.content.Context;
import android.content.Intent;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

public class ReleaseView extends LinearLayout {

	private ImageView releasePic;
	private ImageView releaseVideo;
	private ImageView releaseLive;
	private Context context;

	public ReleaseView(Context context) {
		super(context);
		this.context = context;
		initView(context);
	}

	private void initView(Context context) {
		LayoutInflater.from(context).inflate(R.layout.releaseview_layout, this,true);
		setBackgroundResource(R.drawable.rounded_edittext);
		setOrientation(HORIZONTAL);
		setGravity(Gravity.CENTER);
		setLayoutParams(new LayoutParams(Utils.dpToPx(300), Utils.dpToPx(150)));
		setClickable(true);
	}
	
	@Override
	protected void onAttachedToWindow() {
		super.onAttachedToWindow();
		releasePic = (ImageView)findViewById(R.id.addPic);
		releaseVideo = (ImageView)findViewById(R.id.addVideo);
		releaseLive = (ImageView)findViewById(R.id.addLive);

		releasePic.setOnClickListener(listener);
		releaseVideo.setOnClickListener(listener);
		releaseLive.setOnClickListener(listener);
	}

	public void dismiss(){
		((ViewGroup)getParent()).removeView(this);
	}
	private OnClickListener listener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.addPic:
				Intent intentPhoto = new Intent(context,ReleaseActivity.class);
				intentPhoto.putExtra("type", "photo");
				context.startActivity(intentPhoto);
				break;

			case R.id.addVideo:
				Intent intentVideo = new Intent(context,ReleaseActivity.class);
				intentVideo.putExtra("type", "video");
				context.startActivity(intentVideo);
				break;
			case R.id.addLive:
				Intent intentLive = new Intent(context,LiveActivity.class);
				context.startActivity(intentLive);
				break;
			}
		}
	};
}
