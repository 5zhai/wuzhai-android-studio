package com.wuzhai.app.main.widget;

import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.ViewHolder;

/**
 * Created by moon.zhong on 2015/7/20.
 * time : 14:37
 */
public interface WrapperAdapter {

    public RecyclerView.Adapter<ViewHolder> getWrappedAdapter() ;
}
