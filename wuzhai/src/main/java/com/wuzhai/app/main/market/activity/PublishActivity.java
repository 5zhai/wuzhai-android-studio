package com.wuzhai.app.main.market.activity;

import java.util.ArrayList;
import java.util.HashMap;
import com.wuzhai.app.R;
import com.wuzhai.app.application.WuZhaiApplication;
import com.wuzhai.app.main.WuzhaiService;
import com.wuzhai.app.main.WuzhaiServiceCallbackAdapter;
import com.wuzhai.app.objects.Tag;
import com.wuzhai.app.tools.Utils;
import com.wuzhai.app.widget.TitleToolbarActivity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout.LayoutParams;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class PublishActivity extends TitleToolbarActivity {

	private WuzhaiService service;
	private LinearLayout publishGoodsTypeLayout;
	private TextView publishGoodsTypeTextView;
	private EditText titleEditText;
	private LinearLayout briefLayout;
	private EditText briefEditText;
	private LinearLayout tagLayout;
	private TextView publishTags;
	private EditText priceEditText;
	private LinearLayout stockLayout;
	private EditText stockEditText;
	private EditText publishCity;
	private EditText publishInfo;
	private LinearLayout pictureLayout;
	private TextView publishPicCount;
	private ImageView publishPicIcon;
	private Button publishBtn;
	private HashMap<Integer, String> goodsTypeMap;
	private String[] types;
	private ArrayList<Tag> goodsTagsList;
	private final int PHOTO_PICKED_WITH_DATA = 1881;
	private ArrayList<String> bitmapPaths = new ArrayList<String>();
	private String pageFlag;

	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_publish);
		service = ((WuZhaiApplication)getApplication()).getService();
		service.setCallBack(callbackAdapter);
		service.getType(WuzhaiService.GOODS_TYPE);
		service.getTags(WuzhaiService.TAG_GOODS);
		initView();
	}

	private void initView(){
		publishGoodsTypeLayout = (LinearLayout)findViewById(R.id.publish_type_layout);
		publishGoodsTypeLayout.setOnClickListener(onClickListener);
		publishGoodsTypeTextView = (TextView)findViewById(R.id.publish_type);
		titleEditText = (EditText)findViewById(R.id.publish_title);
		briefLayout = (LinearLayout)findViewById(R.id.brief_layout);
		briefEditText = (EditText)findViewById(R.id.publish_brief);
		tagLayout = (LinearLayout)findViewById(R.id.tags_layout);
		tagLayout.setOnClickListener(onClickListener);
		publishTags = (TextView)findViewById(R.id.publish_tags);
		priceEditText = (EditText)findViewById(R.id.publish_price);
		stockLayout = (LinearLayout)findViewById(R.id.stock_layout);
		stockEditText = (EditText)findViewById(R.id.publish_stock);
		publishCity = (EditText)findViewById(R.id.publish_city);
		publishInfo = (EditText)findViewById(R.id.publish_info);
		pictureLayout = (LinearLayout)findViewById(R.id.picture_layout);
		publishPicCount = (TextView)findViewById(R.id.publish_pic_count);
		publishPicIcon = (ImageView)findViewById(R.id.publish_pic_icon);
		publishPicIcon.setOnClickListener(onClickListener);
		publishBtn = (Button)findViewById(R.id.publish_btn);
		publishBtn.setOnClickListener(onClickListener);
		pageFlag = getIntent().getStringExtra("page_flag");
		if(pageFlag.equals("photographer")){
			setTitle("发布摄影师");
			publishGoodsTypeLayout.setVisibility(View.GONE);
			stockLayout.setVisibility(View.GONE);
		}else if(pageFlag.equals("goods")){
			setTitle("发布商品");
			briefLayout.setVisibility(View.GONE);
		}else if(pageFlag.equals("aftercase")){
			setTitle("发布后期服务");
			publishGoodsTypeLayout.setVisibility(View.GONE);
			stockLayout.setVisibility(View.GONE);
		}
	}

	private OnClickListener onClickListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.publish_type_layout:
				int checkedItem = 0;
				if(goodsTypeMap!=null){
					for(int n=0;n<types.length;n++){
						if(types[n].equals(publishGoodsTypeTextView.getText().toString())){
							checkedItem = n;
						}
					}
					publishGoodsTypeTextView.getText().toString();
					Utils.showSingleChoiceDialog(PublishActivity.this, "商品类型", publishGoodsTypeTextView, types, checkedItem);
				}
				break;
			case R.id.tags_layout:
				Utils.showTagInputDialog(PublishActivity.this,goodsTagsList,publishTags, "标签");
				break;
			case R.id.publish_pic_icon:
				pickPicture();
				break;
			case R.id.publish_btn:
				if(pageFlag.equals("goods")){
					publishGoodsPic();
				}else {
					publishPhotographerAndAftercasePic(pageFlag);
				}
			default:
				break;
			}
		}
	};

	private void pickPicture(){
		Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
		startActivityForResult(intent, PHOTO_PICKED_WITH_DATA);
	}

	@Override
	protected void onActivityResult(int arg0, int arg1, Intent arg2) {
		if(arg1 == RESULT_OK && arg0 == PHOTO_PICKED_WITH_DATA){
			addPicToPictureLayout(arg2.getData());
		}
	}

	private void addPicToPictureLayout(Uri uri){
		Bitmap bitmap = Utils.getBitmapFromURI(this, uri,true);
		ImageView goodsPic = new ImageView(this);
		goodsPic.setLayoutParams(new LayoutParams(Utils.dpToPx(60), Utils.dpToPx(60)));
		goodsPic.setScaleType(ScaleType.FIT_CENTER);
		goodsPic.setImageBitmap(bitmap);
//		goodsPic.setBackgroundColor(0xcc000000);
		pictureLayout.addView(goodsPic);
		bitmapPaths.add(Utils.uriToPath(this, uri));
	}

	private void publishGoodsPic(){
		String goodsType = publishGoodsTypeTextView.getText().toString();
		String goodsTitle = titleEditText.getText().toString();
		String goodsTags = publishTags.getText().toString();
		String goodsPrice = priceEditText.getText().toString();
		String goodsStock = stockEditText.getText().toString();
		String city = publishCity.getText().toString();
		String goodsInfo = publishInfo.getText().toString();

		if("".equals(goodsType) || "".equals(goodsTitle) || "".equals(goodsTags) ||
				"".equals(goodsPrice) || "".equals(goodsStock)|| "".equals(city)|| "".equals(goodsInfo)){
			Toast.makeText(this, "信息不完整", Toast.LENGTH_SHORT).show();
			return;
		}
		if(bitmapPaths.size()==0){
			Toast.makeText(this,"请选择图片", Toast.LENGTH_SHORT).show();
			return;
		}
		if(!Utils.isNetworkAvailable(this)){
			Toast.makeText(this, getString(R.string.no_network), Toast.LENGTH_SHORT).show();
			return;
		}
		int goods_type = Utils.getIntTypeFromString(goodsTypeMap, goodsType);
		if(goods_type==-1){
			Toast.makeText(this, "商品类型错误", Toast.LENGTH_SHORT).show();
			return;
		}
		service.publishPic(3, "goodsPublish", "goodsPublish", bitmapPaths, "goodsPublish", "35.5676,30.678");
	}

	private void publishGoods(ArrayList<String> picPathList){
		String goodsType = publishGoodsTypeTextView.getText().toString();
		String goodsTitle = titleEditText.getText().toString();
		String goodsTags = publishTags.getText().toString();
		float goodsPrice = Float.parseFloat(priceEditText.getText().toString());
		int goodsStock = Integer.parseInt(stockEditText.getText().toString());
		String city = publishCity.getText().toString();
		String goodsInfo = publishInfo.getText().toString();
		int goods_type = Utils.getIntTypeFromString(goodsTypeMap, goodsType);
		StringBuilder detialImagePath = new StringBuilder();
		for(String imagePath : picPathList){
			detialImagePath.append(imagePath+",");
		}
		service.publishGoods(goods_type, goodsTitle, city, goodsInfo,
				picPathList.get(0), detialImagePath.toString(), goodsPrice, goodsStock, goodsTags, "35.5676,30.678");
	}

	private void publishPhotographerAndAftercasePic(String pageFlag){
		String title = titleEditText.getText().toString();
		String city = publishCity.getText().toString();
		String brief = briefEditText.getText().toString();
		String info = publishInfo.getText().toString();
		String tags = publishTags.getText().toString();
		String price = priceEditText.getText().toString();

		if("".equals(title) || "".equals(tags) ||"".equals(price)
				|| "".equals(brief) || "".equals(city)|| "".equals(info)){
			Toast.makeText(this, "信息不完整", Toast.LENGTH_SHORT).show();
			return;
		}
		if(bitmapPaths.size()==0){
			Toast.makeText(this,"请选择图片", Toast.LENGTH_SHORT).show();
			return;
		}
		if(!Utils.isNetworkAvailable(this)){
			Toast.makeText(this, getString(R.string.no_network), Toast.LENGTH_SHORT).show();
			return;
		}
		if (pageFlag.equals("photographer")) {
			service.publishPic(3, "photographerPublish", "photographerPublish",bitmapPaths, "photographerPublish", "35.5676,30.678");
		}else if (pageFlag.equals("aftercase")) {
			service.publishPic(3, "aftercasePublish", "aftercasePublish",bitmapPaths, "aftercasePublish", "35.5676,30.678");
		}
	}

	private void publishPhotographerAndAftercase(ArrayList<String> picPathList,String pageFlag){
		String title = titleEditText.getText().toString();
		String city = publishCity.getText().toString();
		String brief = briefEditText.getText().toString();
		String info = publishInfo.getText().toString();
		String tags = publishTags.getText().toString();
		float price = Float.parseFloat(priceEditText.getText().toString());

		if(!Utils.isNetworkAvailable(this)){
			Toast.makeText(this, getString(R.string.no_network), Toast.LENGTH_SHORT).show();
			return;
		}

		StringBuilder detialImagePath = new StringBuilder();
		for(String imagePath : picPathList){
			detialImagePath.append(imagePath+",");
		}

		if(pageFlag.equals("photographer")){
			service.publishPhotographer(title, brief, city, info, picPathList.get(0), detialImagePath.toString(), price, tags, "35.678,32.789");
		}else if (pageFlag.equals("aftercase")) {
			service.publishAftercases(title, brief, city, info, picPathList.get(0), detialImagePath.toString(), price, tags, "35.678,32.789");
		}
	}

	private void clearInput(){
		publishGoodsTypeTextView.setText("");
		titleEditText.setText("");
		publishTags.setText("");
		priceEditText.setText("");
		stockEditText.setText("");
		publishCity.setText("");
		publishInfo.setText("");
		bitmapPaths.clear();
	}
	private WuzhaiServiceCallbackAdapter callbackAdapter = new WuzhaiServiceCallbackAdapter(){

		@Override
		public void onGetTypeCompleted(HashMap<Integer, String> typeMap) {
			if (typeMap != null) {
				goodsTypeMap = typeMap;
				types = typeMap.values().toArray(new String[typeMap.values().size()]);
			}
		}

		@Override
		public void onGetTagsCompleted(ArrayList<Tag> tagsList) {
			goodsTagsList = tagsList;
		}

		@Override
		public void onUploadFileCompleted(ArrayList<String> picPathList) {
			if(picPathList.size()==0){
				Toast.makeText(PublishActivity.this, "发布失败，请重试", Toast.LENGTH_SHORT).show();
				return;
			}

			if(pageFlag.equals("goods")){
				publishGoods(picPathList);
			}else {
				publishPhotographerAndAftercase(picPathList,pageFlag);
			}
		}

		@Override
		public void onCompleted(int result) {
			switch (result) {
			case WuzhaiService.PUBLISH_AFTERCASE_SUCC:
			case WuzhaiService.PUBLISH_PHOTOGRAPHER_SUCC:
			case WuzhaiService.PUBLISH_GOODS_SUCC:
				Toast.makeText(PublishActivity.this, "发布成功", Toast.LENGTH_SHORT).show();
//				clearInput();
				break;

			default:
				Toast.makeText(PublishActivity.this, "发布失败，请重试", Toast.LENGTH_SHORT).show();
				break;
			}
		}
	};
}
