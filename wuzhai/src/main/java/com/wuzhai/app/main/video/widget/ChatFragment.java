package com.wuzhai.app.main.video.widget;

import java.util.Locale;
import com.wuzhai.app.R;
import io.rong.imkit.RongIM;
import io.rong.imkit.fragment.ConversationFragment;
import io.rong.imkit.model.ProviderTag;
import io.rong.imkit.widget.provider.TextMessageItemProvider;
import io.rong.imlib.model.Conversation;
import io.rong.message.TextMessage;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class ChatFragment extends Fragment {

	@Override
	@Nullable
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//		TextView textView = new TextView(getContext());
//		textView.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
//		textView.setGravity(Gravity.CENTER);
//		textView.setText("聊天");
//		textView.setTextSize(20);
//		return textView;

//		RongIM.registerMessageType(CustomizeMessage.class);
//		RongIM.getInstance().registerMessageTemplate(new CustomizeMessageItemProvider());
		RongIM.getInstance().registerMessageTemplate(new MyTextMessageItemProvider());
		Intent intent = getActivity().getIntent();
//        String mTargetId = intent.getData().getQueryParameter("targetId");
//        String mTargetIds = intent.getData().getQueryParameter("targetIds");

	      View view = inflater.inflate(R.layout.conversation,container,false);
	      Conversation.ConversationType mConversationType = Conversation.ConversationType.valueOf("chatroom".toUpperCase(Locale.getDefault()));
	        ConversationFragment fragment = (ConversationFragment) getChildFragmentManager().findFragmentById(R.id.conversation);
	        Uri uri = Uri.parse("rong://" + getActivity().getApplicationInfo().packageName).buildUpon()
	                .appendPath("conversation").appendPath(mConversationType.getName().toLowerCase())
	                .appendQueryParameter("targetId", "9527").build();

	        fragment.setUri(uri);
	        return  view;
	}
	@ProviderTag ( messageContent = TextMessage.class , showPortrait = true , centerInHorizontal = false ,showSummaryWithName = true )
	private class MyTextMessageItemProvider extends TextMessageItemProvider{}


}
