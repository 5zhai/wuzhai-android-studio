package com.wuzhai.app.main.widget;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;
import com.wuzhai.app.R;
import com.wuzhai.app.tools.Utils;
import java.util.ArrayList;
import io.rong.imkit.RongIM;
import io.rong.imlib.IRongCallback;
import io.rong.imlib.RongIMClient;
import io.rong.imlib.model.Conversation;
import io.rong.imlib.model.Message;
import io.rong.message.TextMessage;

/**
 * Created by huangyue on 2016/10/25.
 */
public class ChatRoomFragment extends Fragment {
    private String roomId;
    private ListView wordsList;
    private ArrayAdapter<String> wordAdapter;
    private ArrayList<String> wordsDataList;
    private LinearLayout editLayout;
    private EditText editText;
    private Button sendMsgBtn;
    private TextMessage myTextMessage;
    private Message myMessage;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_chatroom, container);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        editLayout = (LinearLayout) view.findViewById(R.id.edit_layout);
        editText = (EditText) view.findViewById(R.id.editText);
        sendMsgBtn = (Button) view.findViewById(R.id.sendmsg_btn);
        wordsList = (ListView) view.findViewById(R.id.words_list);
        wordsDataList = new ArrayList<>();
        wordAdapter = new ArrayAdapter<>(getContext(), R.layout.layout_live_wordslist_item, wordsDataList);
        wordsList.setAdapter(wordAdapter);

        sendMsgBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String words = editText.getText().toString().trim();
                if (!TextUtils.isEmpty(words)) {
                    sendMessage(roomId, words);
                    editText.getText().clear();
                }
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        joinChatRoom(roomId);
        RongIM.setOnReceiveMessageListener(new MyReceiveMessageListener());
    }

    @Override
    public void onStop() {
        super.onStop();
        quitChatRoom(roomId);
        RongIM.setOnReceiveMessageListener(null);
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        if (hidden) {
            Utils.closeInputMethod(getContext(), editText);
        }
    }

    private void sendMessage(String roomId, String msg) {
        myTextMessage = TextMessage.obtain(msg);
        myMessage = Message.obtain(roomId, Conversation.ConversationType.CHATROOM, myTextMessage);

        RongIM.getInstance().sendMessage(myMessage, null, null, new IRongCallback.ISendMessageCallback() {


            @Override
            public void onAttached(Message message) {

            }

            @Override
            public void onSuccess(Message message) {
                addMessageToChatList(message);
            }

            @Override
            public void onError(Message message, RongIMClient.ErrorCode errorCode) {
                Log.d("yue.huang", "sendMessage_onError:" + errorCode.getMessage());
            }
        });
    }

    private class MyReceiveMessageListener implements RongIMClient.OnReceiveMessageListener {

        /**
         * 收到消息的处理。
         *
         * @param message 收到的消息实体。
         * @param left    剩余未拉取消息数目。
         * @return 收到消息是否处理完成，true 表示走自已的处理方式，false 走融云默认处理方式。
         */
        @Override
        public boolean onReceived(Message message, int left) {
            addMessageToChatList(message);
            return false;
        }
    }

    private void addMessageToChatList(Message message) {
        if(message.getContent().getUserInfo()!=null) {
            String userName = message.getContent().getUserInfo().getName();
            String msgContent = ((TextMessage) message.getContent()).getContent();
            String words = userName + ":  " + msgContent;
            wordsDataList.add(words);
            wordAdapter.notifyDataSetChanged();
            wordsList.setSelection(wordsDataList.size());
        }
    }

    private void joinChatRoom(final String roomId) {
        RongIM.getInstance().joinChatRoom(roomId, 10, new RongIMClient.OperationCallback() {

            @Override
            public void onSuccess() {
                Log.d("yue.huang", "ChatRoomFragment_joinChatRoom_onSuccess:"+roomId);
            }

            @Override
            public void onError(RongIMClient.ErrorCode errorCode) {
                Log.d("yue.huang", "ChatRoomFragment_joinChatRoom_onError:" + errorCode.getMessage());
                Toast.makeText(getContext(), "加入聊天室失败", Toast.LENGTH_LONG).show();
            }
        });
    }

    private void quitChatRoom(String roomId) {
        RongIM.getInstance().quitChatRoom(roomId, new RongIMClient.OperationCallback() {

            @Override
            public void onSuccess() {
                Log.d("yue.huang", "ChatRoomFragment_quitChatRoom_onSuccess");
            }

            @Override
            public void onError(RongIMClient.ErrorCode errorCode) {
                Log.d("yue.huang", "ChatRoomFragment_quitChatRoom_onError:" + errorCode.getMessage());
                Toast.makeText(getContext(), "退出聊天室失败", Toast.LENGTH_LONG).show();
            }
        });
    }

    public void setRoomId(String id) {
        roomId = id;
    }

    public void showEditLayout() {
        editLayout.setVisibility(View.VISIBLE);
    }

    public void hideEditLayout() {
        editLayout.setVisibility(View.INVISIBLE);
    }

    public boolean isEditLayoutShown() {
        return editLayout.getVisibility() == View.VISIBLE;
    }
}
