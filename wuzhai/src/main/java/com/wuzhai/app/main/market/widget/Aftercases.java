package com.wuzhai.app.main.market.widget;

import java.io.Serializable;

public class Aftercases implements Serializable{

	private int id;
	private String title;
	private String desc;
	private int publisherId;
	private String publisherName;
	private String publisherAvatar;
	private String publisherAccesskey;
	private String city;
	private float price;
	private String[] detialImages;
	private String mainImage;
	private String[] tags;
	private double longitude;
	private double latitude;
	private String brief;
	private int commentsCount;
	private boolean collected = false;
	private boolean followed = false;

	public Aftercases(){}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public int getPublisherId() {
		return publisherId;
	}

	public void setPublisherId(int publisherId) {
		this.publisherId = publisherId;
	}

	public String getPublisherName() {
		return publisherName;
	}

	public void setPublisherName(String publisherName) {
		this.publisherName = publisherName;
	}

	public String getPublisherAvatar() {
		return publisherAvatar;
	}

	public void setPublisherAvatar(String publisherAvatar) {
		this.publisherAvatar = publisherAvatar;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public float getPrice() {
		return price;
	}

	public void setPrice(float price) {
		this.price = price;
	}

	public String[] getDetialImages() {
		return detialImages;
	}

	public void setDetialImages(String images) {
		detialImages = images.split(",");
	}

	public String getMainImage() {
		return mainImage;
	}

	public void setMainImage(String mainImage) {
		this.mainImage = mainImage;
	}

	public String[] getTags() {
		return tags;
	}

	public void setTags(String tags) {
		this.tags = tags.split(",");
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public String getBrief() {
		return brief;
	}

	public void setBrief(String brief) {
		this.brief = brief;
	}

	public int getCommentsCount() {
		return commentsCount;
	}

	public void setCommentsCount(int commentsCount) {
		this.commentsCount = commentsCount;
	}

	public boolean isCollected() {
		return collected;
	}

	public void setCollected(boolean collected) {
		this.collected = collected;
	}

	public boolean isFollowed() {
		return followed;
	}

	public void setFollowed(boolean followed) {
		this.followed = followed;
	}

	public String getPublisherAccesskey() {
		return publisherAccesskey;
	}

	public void setPublisherAccesskey(String publisherAccesskey) {
		this.publisherAccesskey = publisherAccesskey;
	}
}
