package com.wuzhai.app.main.widget;

import java.util.ArrayList;
import java.util.List;
import com.wuzhai.app.R;
import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class NavigationMenuAdapter extends ArrayAdapter<NavigationMenuAdapter.GlobalMenuItem> {

	private static final int TYPE_MENU_ITEM = 0;
    private static final int TYPE_DIVIDER = 1;
    private final LayoutInflater inflater;
   	private final List<GlobalMenuItem> menuItems = new ArrayList<GlobalMenuItem>();
   	
	public NavigationMenuAdapter(Context context){
		super(context, 0);
		inflater = LayoutInflater.from(context);
		setupMenuItems();
	}

	 private void setupMenuItems() {
		 menuItems.add(new GlobalMenuItem(R.drawable.icon_person, "个人中心"));
		 menuItems.add(new GlobalMenuItem(R.drawable.icon_order, "我的订单"));
		 menuItems.add(new GlobalMenuItem(R.drawable.icon_wallet, "我的钱包"));
		 menuItems.add(new GlobalMenuItem(R.drawable.icon_collection, "我的收藏"));
		 menuItems.add(new GlobalMenuItem(R.drawable.icon_gifts, "礼物中心"));
//		 menuItems.add(GlobalMenuItem.dividerMenuItem());
//		 menuItems.add(new GlobalMenuItem(0, "Settings"));
//		 menuItems.add(new GlobalMenuItem(0, "About"));
		 notifyDataSetChanged();
		 }
	
	 
	 
	 @Override
	public int getCount() {
		// TODO Auto-generated method stub
		return menuItems.size();
	}

	@Override
	public GlobalMenuItem getItem(int position) {
		// TODO Auto-generated method stub
		return menuItems.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}
	
	
//
//	@Override
//	public boolean isEnabled(int position) {
//		// TODO Auto-generated method stub
//		return super.isEnabled(position);
//	}

//	@Override
//	public int getViewTypeCount() {
//		// TODO Auto-generated method stub
//		return 2;
//	}

	@SuppressLint("NewApi")
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ImageView menuIcon;
		TextView menuName;
		if(convertView == null){
			MenuItemViewHolder itemViewHolder = new MenuItemViewHolder();
			convertView = inflater.inflate(R.layout.navigation_menu_item, parent,false);
			menuIcon = (ImageView)convertView.findViewById(R.id.navigation_menu_item_icon);
			menuName = (TextView)convertView.findViewById(R.id.navigation_menu_item_name);
			itemViewHolder.imageView = menuIcon;
			itemViewHolder.textView = menuName;
			convertView.setTag(itemViewHolder);
		}else {
			menuIcon = ((MenuItemViewHolder)convertView.getTag()).imageView;
			menuName = ((MenuItemViewHolder)convertView.getTag()).textView;
		}
		Drawable menuIconDrawable = getContext().getResources().getDrawable(menuItems.get(position).iconResId);
		String menuLable = menuItems.get(position).label;
		menuIcon.setImageDrawable(menuIconDrawable);
		menuName.setText(menuLable);
		return convertView;
	}


	private static class MenuItemViewHolder{
		ImageView imageView;
		TextView textView;
	}
	

	public static class GlobalMenuItem {
		 public int iconResId;
		 public String label;
		 public boolean isDivider;
		
		 private GlobalMenuItem() {
		
		 }
		
		 public GlobalMenuItem(int iconResId, String label) {
		 this.iconResId = iconResId;
		 this.label = label;
		 this.isDivider = false;
		 }
		
		 public static GlobalMenuItem dividerMenuItem() {
		 GlobalMenuItem globalMenuItem = new GlobalMenuItem();
		 globalMenuItem.isDivider = true;
		 return globalMenuItem;
		 }
		 }
}
