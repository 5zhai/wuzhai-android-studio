package com.wuzhai.app.main.home.widget;

public class News {

	private int id;
	private String desc;
	private String iconURL;
	private String title;
	private String subtitle;
	
	public News(int id,String iconURL,String title,String subtitle,String desc){
		this.id = id;
		this.iconURL = iconURL;
		this.title = title;
		this.subtitle = subtitle;
		this.desc = desc;
	}

	public String getIconURL() {
		return iconURL;
	}

	public String getTitle() {
		return title;
	}

	public String getSubtitle() {
		return subtitle;
	}

	public int getId() {
		return id;
	}

	public String getDesc() {
		return desc;
	}

}
