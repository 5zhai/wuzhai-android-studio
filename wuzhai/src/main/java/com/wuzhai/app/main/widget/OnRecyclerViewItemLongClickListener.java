package com.wuzhai.app.main.widget;

import android.view.View;

public interface OnRecyclerViewItemLongClickListener {
	void onItemLongClick(View view,int position);
}
