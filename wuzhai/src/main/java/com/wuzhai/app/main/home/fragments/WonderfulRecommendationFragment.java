package com.wuzhai.app.main.home.fragments;

import java.util.ArrayList;

import com.squareup.picasso.Picasso;
import com.wuzhai.app.R;
import com.wuzhai.app.main.WuzhaiService;
import com.wuzhai.app.main.home.widget.News;
import com.wuzhai.app.main.home.widget.RecommendAdapter;
import com.wuzhai.app.main.market.widget.ComicConNews;
import com.wuzhai.app.main.widget.LoadMoreListView;
import com.wuzhai.app.main.widget.LoadMoreListView.LoadMoreCallBack;
import com.wuzhai.app.tools.Utils;
import com.wuzhai.app.widget.RefreshableView;
import com.wuzhai.app.widget.RefreshableView.PullToRefreshListener;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

@SuppressLint("ValidFragment")
public class WonderfulRecommendationFragment extends Fragment {
	private WuzhaiService service;
	private RefreshableView pullRefreshLayout;
	private LoadMoreListView listView;
	private ImageView head_titleIcon;
	private ImageView head_subtitleIcon1;
	private ImageView head_subtitleIcon2;
	private int nextPage = 0;
	private RecommendAdapter adapter;
	private ArrayList<News> newsList = new ArrayList<News>();
	public WonderfulRecommendationFragment(WuzhaiService service){
		this.service = service;
	}
	@Override
	@Nullable
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_home_recommendation, null);
		View listHeaderView = inflater.inflate(R.layout.header_listview_recommend, null);
		pullRefreshLayout = (RefreshableView)view.findViewById(R.id.pullRefreshLayout);
//		pullRefreshLayout.setColorSchemeResources(android.R.color.holo_blue_light,
//				android.R.color.holo_red_light, android.R.color.holo_orange_light,
//				android.R.color.holo_green_light);
		pullRefreshLayout.setOnRefreshListener(refreshListener,1);
		listView = (LoadMoreListView)view.findViewById(R.id.listview);
		listView.addHeaderView(listHeaderView);
		listView.setLoadMoreCallBack(loadMoreCallBack);
		adapter = new RecommendAdapter(getContext());
		return view;
	}

	@Override
	public void onActivityCreated(@Nullable Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		initListview();
	}

	@Override
	public void onStop() {
		newsList.clear();
		super.onStop();
	}

	private void initListview(){
		head_titleIcon = (ImageView)listView.findViewById(R.id.red_head_titleIcon);
		head_subtitleIcon1 = (ImageView)listView.findViewById(R.id.red_head_subtitleIcon1);
		head_subtitleIcon2 = (ImageView)listView.findViewById(R.id.red_head_subtitleIcon2);
        Picasso.with(getContext()).load(R.drawable.home_banner).into(head_titleIcon);
        if(Utils.isNetworkAvailable(getActivity())){
            service.getLatestComicCon();
            service.getRecommendationsList(1);
        }else {
			Toast.makeText(getActivity(), "无网络连接", Toast.LENGTH_SHORT).show();
		}
	}

	public void updateRecommendations(ArrayList<News> recommendationsList,int next_page){
		nextPage = next_page;
		if(pullRefreshLayout.isRefreshing()){
			newsList.clear();
		}
		newsList.addAll(recommendationsList);
		adapter.setRecommendList(newsList);
		listView.setAdapter(adapter);
		if(!pullRefreshLayout.isRefreshing()){
			listView.setSelection(newsList.size()-recommendationsList.size());
		}
		pullRefreshLayout.finishRefreshing();
	}

	public void updateLatestComicCon(ArrayList<ComicConNews> comicConNewsList) {
		Picasso.with(getContext()).load(comicConNewsList.get(0).getPicturePath()).into(head_subtitleIcon1);
		Picasso.with(getContext()).load(comicConNewsList.get(1).getPicturePath()).into(head_subtitleIcon2);
	}

	private PullToRefreshListener refreshListener = new PullToRefreshListener() {
		@Override
		public void onRefresh() {
			listView.setFooterMoreToLoad();
	        if(Utils.isNetworkAvailable(getActivity())){
				service.getLatestComicCon();
				service.getRecommendationsList(1);
	        }else {
				Toast.makeText(getActivity(), "无网络连接", Toast.LENGTH_SHORT).show();
				pullRefreshLayout.finishRefreshing();
			}
		}
	};

	private LoadMoreCallBack loadMoreCallBack = new LoadMoreCallBack() {
		@Override
		public void loadMore() {
			if(nextPage!=0){
		        if(Utils.isNetworkAvailable(getActivity())){
					service.getRecommendationsList(nextPage);
		        }else {
					Toast.makeText(getActivity(), "无网络连接", Toast.LENGTH_SHORT).show();
				}
			}else {
				listView.setFooterNoMoreToLoad();
			}
		}
	};
}
