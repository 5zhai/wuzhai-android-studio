package com.wuzhai.app.main.video.widget;

import com.wuzhai.app.objects.MediaObject;

public class VideoEntity extends MediaObject{

	private String url;
	private int playCount;

	public VideoEntity(){}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public int getPlayCount() {
		return playCount;
	}

	public void setPlayCount(int playCount) {
		this.playCount = playCount;
	}

	//---------------------------------------------------------------
//	private String picturePath;
	private String voideTitle;
	private String publisherAvatarPath;
//	private String publisherName;
	private String watchCount;
	private String likeCount;
	private String messageCount;
	public VideoEntity(String picturePath, String voideTitle,
			String publisherAvatarPath, String publisherName,
			String watchCount, String likeCount, String messageCount) {
		this.picturePath = picturePath;
		this.voideTitle = voideTitle;
		this.publisherAvatarPath = publisherAvatarPath;
		this.publisherName = publisherName;
		this.watchCount = watchCount;
		this.likeCount = likeCount;
		this.messageCount = messageCount;
	}

	public String getVoideTitle() {
		return voideTitle;
	}

	public String getpublisherAvatarPath() {
		return publisherAvatarPath;
	}

	public String getpublisherName() {
		return publisherName;
	}

	public String getwatchCount() {
		return watchCount;
	}

//	public String getPicturePath() {
//		return picturePath;
//	}

	public String getLikeCount() {
		return likeCount;
	}

	public String getMessageCount() {
		return messageCount;
	}
//----------------------------------------
	
}
