package com.wuzhai.app.main.widget;

import java.util.ArrayList;

import com.squareup.picasso.Picasso;
import com.wuzhai.app.R;
import com.wuzhai.app.objects.MediaObject;
import com.wuzhai.app.objects.Photo;
import com.wuzhai.app.tools.Utils;
import android.content.Context;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class WelfareListAdapter extends HyRecyclerViewAdapter implements OnClickListener{
	private Context context;
	private ArrayList<? extends MediaObject> mediaList;
	private WelfareListItemOnClickListener itemOnClickListener;
	public interface WelfareListItemOnClickListener{
		public void onFollowBtnClick(MediaObject mediaObject,View v);
		public void onLikeClick(MediaObject mediaObject,boolean isLike);
		public void onCommentClick(MediaObject mediaObject,View v,int index);
		public void onPicture(MediaObject mediaObject);
	}
	public WelfareListAdapter(Context context,ArrayList<? extends MediaObject> mediaList){
		this.context = context;
		this.mediaList = mediaList;
	}

	@Override
	public int getItemCount() {
		return mediaList.size();
	}

	@Override
	public void onBindViewHolder(ViewHolder arg0, int arg1) {
		WelfareViewHolder viewHolder = (WelfareViewHolder)arg0;
		viewHolder.username.setText(mediaList.get(arg1).getPublisherName());
		Picasso.with(context).load(mediaList.get(arg1).getPublisherAvatar()).resize(80, 80).into(viewHolder.userAvatar);
		Picasso.with(context).load(mediaList.get(arg1).getPicturePath()).resize(300, 200).into(viewHolder.showPic);
		viewHolder.info.setText(mediaList.get(arg1).getDesc());
		viewHolder.commentCount.setText(mediaList.get(arg1).getCommentsCount()+"");
		viewHolder.likeCount.setText(mediaList.get(arg1).getLikesCount()+"");
		viewHolder.time.setText(Utils.getDifferenceWithCurrentTime(mediaList.get(arg1).getCreatedAt()));
		viewHolder.followbtn.setTag(arg1);
		viewHolder.followbtn.setOnClickListener(this);
		viewHolder.like.setTag(arg1);
		viewHolder.like.setOnClickListener(this);
		viewHolder.iconLike.setTag(0);//0--没喜欢
		viewHolder.commentList.setTag(arg1);
		viewHolder.commentList.setOnClickListener(this);
		viewHolder.showPic.setTag(arg1);
		viewHolder.showPic.setOnClickListener(this);
		viewHolder.picCount.setText("("+((Photo)mediaList.get(arg1)).getPicturePaths().length+"张)");
		if(mediaList.get(arg1).isLiked()){
			viewHolder.iconLike.setImageResource(R.drawable.icon_praise_press);
			viewHolder.iconLike.setTag(1);//1--喜欢
		}else {
			viewHolder.iconLike.setImageResource(R.drawable.icon_praise);
			viewHolder.iconLike.setTag(0);//--不喜欢
		}
		if(mediaList.get(arg1).isFollowed()){
			viewHolder.followbtn.setText("已关注");
			viewHolder.followbtn.setEnabled(false);
			viewHolder.followbtn.setBackgroundColor(0xf2f2f2f2);
		}else {
			viewHolder.followbtn.setText("+关注");
			viewHolder.followbtn.setEnabled(true);
			viewHolder.followbtn.setBackgroundResource(R.drawable.rounded_button);
		}
	}

	@Override
	public ViewHolder onCreateViewHolder(ViewGroup arg0, int arg1) {
		View view = LayoutInflater.from(context).inflate(R.layout.welfare_item, arg0,false);
		
		return new WelfareViewHolder(view);
	}

	private class WelfareViewHolder extends ViewHolder{

		ImageView userAvatar;
		TextView username;
		ImageView showPic;
		TextView info;
		TextView picCount;
		LinearLayout like;
		TextView likeCount;
		ImageView iconLike;
		LinearLayout commentList;
		TextView commentCount;
		TextView time;
		Button followbtn;

		public WelfareViewHolder(View arg0) {
			super(arg0);
			userAvatar = (ImageView)arg0.findViewById(R.id.userAvatar);
			username = (TextView)arg0.findViewById(R.id.username);
			showPic = (ImageView)arg0.findViewById(R.id.showPic);
			info = (TextView)arg0.findViewById(R.id.info);
			picCount = (TextView)arg0.findViewById(R.id.pic_count);
			like = (LinearLayout)arg0.findViewById(R.id.like);
			likeCount = (TextView)arg0.findViewById(R.id.like_count);
			iconLike = (ImageView)arg0.findViewById(R.id.icon_like);
			commentList = (LinearLayout)arg0.findViewById(R.id.comment_list);
			commentCount = (TextView)arg0.findViewById(R.id.comment_count);
			time = (TextView)arg0.findViewById(R.id.time);
			followbtn = (Button)arg0.findViewById(R.id.followbtn);
		}
	}
	@Override
	public void onClick(View v) {
		int index = (Integer)v.getTag();
		MediaObject object = mediaList.get(index);
		switch (v.getId()) {
		case R.id.followbtn:
			itemOnClickListener.onFollowBtnClick(object,v);
			break;
		case R.id.like:
			ImageView likeIcon = (ImageView)v.findViewById(R.id.icon_like);
			TextView likeCount = (TextView)v.findViewById(R.id.like_count);
			int flag = (Integer)likeIcon.getTag();
			boolean isLike = false;
			if(flag == 0){
				likeCount.setText((Integer.parseInt(likeCount.getText().toString())+1)+"");
				likeIcon.setTag(1);
				likeIcon.setImageResource(R.drawable.icon_praise_press);
				isLike = true;
			}else {
				likeCount.setText(""+(Integer.parseInt(likeCount.getText().toString())-1));
				likeIcon.setTag(0);
				likeIcon.setImageResource(R.drawable.icon_praise);
				isLike = false;
			}
			itemOnClickListener.onLikeClick(object,isLike);
			break;
		case R.id.comment_list:
			itemOnClickListener.onCommentClick(object,v,index);
			break;
		case R.id.showPic:
			itemOnClickListener.onPicture(object);
			break;
		}
	}
	
	public void setItemOnClickListener(WelfareListItemOnClickListener itemOnClickListener){
		this.itemOnClickListener = itemOnClickListener;
	}
}
