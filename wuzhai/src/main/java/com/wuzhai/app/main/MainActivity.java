package com.wuzhai.app.main;

import java.io.IOException;

import com.squareup.picasso.Picasso;
import com.wuzhai.app.R;
import com.wuzhai.app.application.WuZhaiApplication;
import com.wuzhai.app.main.publish.widget.ReleaseViewManager;
import com.wuzhai.app.main.widget.StateSaveFragmentTabHost;
import com.wuzhai.app.main.widget.NavigationMenuView;
import com.wuzhai.app.tools.DrawerLayoutInstaller;
import com.wuzhai.app.tools.Utils;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.DrawerLayout.DrawerListener;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.TabHost.OnTabChangeListener;
import android.widget.TabHost.TabSpec;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

	private WuzhaiService service;
	private StateSaveFragmentTabHost mTabHost;
	private Toolbar toolbar;
	private TextView pageTitle;
    private ImageView pageTitleIcon;
	private LinearLayout releaseBtn;
	private ReleaseViewManager releaseViewManager;
	private View shadowView;
	private NavigationMenuView menuView;
	private long exitTime = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		service = ((WuZhaiApplication)getApplication()).getService();
		service.connectToRongCloud();
		service.fixedPosition();
		service.getWXInfo();
		releaseViewManager = new ReleaseViewManager(this);
		releaseViewManager.setAnimationEndListener(animationEndListener);
		pageTitle = (TextView)findViewById(R.id.pageTitle);
        pageTitleIcon = (ImageView)findViewById(R.id.pageTitle_icon);
		setUpToobar();
		setupNavigationMenu();
		initView();
	}

	@Override
	protected void onStop() {
		super.onStop();
		hideReleaseMenu();
	}

	private void setUpToobar(){
		toolbar = (Toolbar)findViewById(R.id.toolbar);
		toolbar.setTitle("");
		toolbar.setNavigationIcon(R.drawable.icon_menu_white);
		new Thread(new Runnable() {
			@Override
			public void run() {
				// TODO Auto-generated method stub
				try {
					String avatarUrl = ((WuZhaiApplication) getApplication()).getUser().getAvatarUrl();
					int picSize = Utils.dpToPx(35);
					if (!TextUtils.isEmpty(avatarUrl) && !avatarUrl.equals("null")) {
						final Bitmap avatarBmp = Picasso.with(MainActivity.this).load(avatarUrl).resize(picSize, picSize).centerInside().get();
						MainActivity.this.runOnUiThread(new Runnable() {
							@Override
							public void run() {
								toolbar.setLogo(Utils.getRoundedUserPortrait(MainActivity.this, avatarBmp));
							}
						});
					}
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					Log.d("yue.huang","IOException:"+e.toString());
				}
			}
		}).start();
		setSupportActionBar(toolbar);
	}

	private void setupNavigationMenu(){
		//可将NavigationMenuView和设置，消息放到一个xml中布局，然后xml文件生成的view设置给drawerLeftView
	    menuView = new NavigationMenuView(this);
	    DrawerLayout drawerLayout = DrawerLayoutInstaller.from(this)
	            .drawerRoot(R.layout.navigation_menu_layout)
	            .drawerLeftView(menuView)
	            .drawerLeftWidth(250)//Utils.dpToPx(300)
	            .withNavigationIconToggler(toolbar)
	            .build();
	    drawerLayout.setDrawerListener(drawerListener);
	    menuView.setDrawerLayout(drawerLayout);
	}

	private void initView(){
		shadowView = findViewById(R.id.shadow);
		shadowView.setOnClickListener(onClickListener);
		releaseBtn = (LinearLayout)findViewById(R.id.release);
		releaseBtn.setOnClickListener(onClickListener);
		mTabHost = (StateSaveFragmentTabHost)findViewById(R.id.tabhost);
		mTabHost.setup(this, getSupportFragmentManager(), R.id.realtabcontent);
		mTabHost.getTabWidget().setShowDividers(0);
		mTabHost.setOnTabChangedListener(new OnTabChangeListener() {
			@Override
			public void onTabChanged(String tabId) {
				if(!tabId.equals("福利")){
					updateTextToolBar(tabId);
				}
			}
		});
		initTabs();
	}
	
	private void initTabs(){
		MainTab[] tabs = MainTab.values();
		for (int i = 0; i < tabs.length; i++) {
			MainTab tab = tabs[i];
			TabSpec tabSpec = mTabHost.newTabSpec(getString(tab.getTabName()));
		    View indicatorView = LayoutInflater.from(this).inflate(R.layout.tab_item, null);
		    ImageView indicatorIcon = (ImageView)indicatorView.findViewById(R.id.tab_icon);
		    TextView indicatorTitle = (TextView)indicatorView.findViewById(R.id.tab_title);
		    indicatorIcon.setImageResource(tab.getTabIcon());
		    indicatorTitle.setText(getString(tab.getTabName()));
		    tabSpec.setIndicator(indicatorView);
		    mTabHost.addTab(tabSpec, tab.getTabCls(), null);
		}
	}

	public void updateTextToolBar(String title){
		pageTitle.setVisibility(View.VISIBLE);
		pageTitleIcon.setVisibility(View.GONE);
		pageTitle.setText(title);
	}

    public void updateTextToolBar(String title,int drawableRightId,OnClickListener clickListener){
        updateTextToolBar(title);
        pageTitle.setOnClickListener(clickListener);
        pageTitleIcon.setVisibility(View.VISIBLE);
        pageTitleIcon.setImageResource(drawableRightId);
    }

	private void hideReleaseMenu() {
		if (shadowView.getVisibility() != View.GONE) {
			releaseViewManager.toggleReleaseViewFromView(null);
			shadowView.setVisibility(View.GONE);
		}
	}

	private OnClickListener onClickListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.release:
				shadowView.setClickable(false);
				releaseViewManager.toggleReleaseViewFromView(releaseBtn);
				if(releaseViewManager.isReleaseViewShowing()){
					shadowView.setVisibility(View.VISIBLE);
				}else {
					shadowView.setVisibility(View.GONE);
				}
				break;
			case R.id.shadow:
				hideReleaseMenu();
				break;
			}
		}
	};

	private DrawerListener drawerListener = new DrawerListener() {

		@Override
		public void onDrawerStateChanged(int arg0) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onDrawerSlide(View arg0, float arg1) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onDrawerOpened(View arg0) {
			Log.d("yue.huang", "onDrawerOpened:getUserInfo");
			menuView.updateHeaderData(((WuZhaiApplication)getApplication()).getUser());
		}

		@Override
		public void onDrawerClosed(View arg0) {
		}
	};


	@Override
	public void onBackPressed() {
		if(System.currentTimeMillis()-exitTime>1500){
			Toast.makeText(this,"再按一次退出程序",Toast.LENGTH_SHORT).show();
			exitTime = System.currentTimeMillis();
		}else {
			super.onBackPressed();
		}
	}

	private ReleaseViewManager.OnAnimationEndListener animationEndListener = new ReleaseViewManager.OnAnimationEndListener() {
		@Override
		public void onAnimationEnd() {
			shadowView.setClickable(true);
		}
	};
}
