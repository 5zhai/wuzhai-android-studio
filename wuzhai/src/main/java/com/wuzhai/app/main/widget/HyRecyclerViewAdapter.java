package com.wuzhai.app.main.widget;

import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.ViewHolder;

public abstract class HyRecyclerViewAdapter extends RecyclerView.Adapter<ViewHolder> {

	protected OnRecyclerViewItemClickListener recyclerViewItemClickListener;
	protected OnRecyclerViewItemLongClickListener recyclerViewItemLongClickListener;

	public void setOnRecyclerViewItemClickListener(OnRecyclerViewItemClickListener recyclerViewItemClickListener) {
		this.recyclerViewItemClickListener = recyclerViewItemClickListener;
	}

	public void setOnRecyclerViewItemLongClickListener(OnRecyclerViewItemLongClickListener recyclerViewItemLongClickListener) {
		this.recyclerViewItemLongClickListener = recyclerViewItemLongClickListener;
	}
}
