package com.wuzhai.app.main.video;

import java.util.ArrayList;

import com.squareup.picasso.Picasso;
import com.viewpagerindicator.CirclePageIndicator;
import com.wuzhai.app.R;
import com.wuzhai.app.application.WuZhaiApplication;
import com.wuzhai.app.main.WuzhaiService;
import com.wuzhai.app.main.WuzhaiServiceCallbackAdapter;
import com.wuzhai.app.main.video.activity.DanceDetailActivity;
import com.wuzhai.app.main.video.activity.DanceListActivity;
import com.wuzhai.app.main.video.activity.LiveListActivity;
import com.wuzhai.app.main.video.activity.NewLiveDetialActivity;
import com.wuzhai.app.main.video.widget.LiveRoomEntity;
import com.wuzhai.app.main.video.widget.LiveRoomListAdapter;
import com.wuzhai.app.main.video.widget.VideoAdapter;
import com.wuzhai.app.main.video.widget.VideoEntity;
import com.wuzhai.app.main.widget.OnRecyclerViewItemClickListener;
import com.wuzhai.app.objects.Carousel;
import com.wuzhai.app.tools.Utils;
import com.wuzhai.app.widget.AutoScrollViewPager;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

public class VideoFragment extends Fragment implements OnClickListener{
	private WuzhaiService service;
	private TextView liveMore;
	private TextView danceMore;
	private RecyclerView liveListRV;
	private LiveRoomListAdapter liveAdapter;
	private ArrayList<LiveRoomEntity> roomList = new ArrayList<>();
	
	private RecyclerView videoListRV;
	private ArrayList<VideoEntity> videoList = new ArrayList<VideoEntity>();
	private VideoAdapter videoAdapter;

	private AutoScrollViewPager viewPager;
	private ArrayList<Carousel> carouselDataList;
	private CirclePageIndicator indicator;

	@Override
	@Nullable
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_video, null);
		return view;
	}
	@SuppressLint("NewApi") public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		service = ((WuZhaiApplication)getActivity().getApplication()).getService();
		service.setCallBack(callbackAdapter);
		service.getSelectedVideo(1);
		service.getLiveList(1);
		service.getCarousel(WuzhaiService.CAROUSEL_TYPE_VIDEO,3);
		liveMore = (TextView)view.findViewById(R.id.live_more);
		liveMore.setOnClickListener(this);

		liveListRV = (RecyclerView)view.findViewById(R.id.live_list);
		liveAdapter = new LiveRoomListAdapter(getContext(), roomList);
		liveAdapter.setOnRecyclerViewItemClickListener(new OnRecyclerViewItemClickListener() {
			@Override
			public void onItemClick(View view, int position) {
				Intent intent = new Intent(getContext(), NewLiveDetialActivity.class);
				intent.putExtra("anchor_id", roomList.get(position).getUserId());
				intent.putExtra("videoPath", roomList.get(position).getRtmpLiveUrls());
				intent.putExtra("room_id", roomList.get(position).getId());
				intent.putExtra("watch_count", roomList.get(position).getOnlineCount());
				startActivity(intent);
			}
		});
		GridLayoutManager liveGridLayoutManager = new GridLayoutManager(getContext(), 2);
		liveListRV.setLayoutManager(liveGridLayoutManager);
		liveListRV.setAdapter(liveAdapter);
		
		videoListRV = (RecyclerView)view.findViewById(R.id.video_list);
		videoAdapter = new VideoAdapter(getActivity(), videoList,true);
		videoAdapter.setOnRecyclerViewItemClickListener(new OnRecyclerViewItemClickListener() {
			@Override
			public void onItemClick(View view, int position) {
				Intent intent = new Intent(getContext(),DanceDetailActivity.class);
				intent.putExtra("id", videoList.get(position).getId());
				intent.putExtra("user_id",videoList.get(position).getPublisherId());
				startActivity(intent);
			}
		});
		GridLayoutManager videoGridLayoutManager = new GridLayoutManager(getContext(),2);
		videoListRV.setLayoutManager(videoGridLayoutManager);
		videoListRV.setAdapter(videoAdapter);
		
		danceMore = (TextView)view.findViewById(R.id.dance_more);
		danceMore.setOnClickListener(this);

		//viewpager 初始化
		viewPager = (AutoScrollViewPager)view.findViewById(R.id.viewpager);
		indicator = (CirclePageIndicator)view.findViewById(R.id.indicator);
		viewPager.getLayoutParams().height = (int)(Utils.getScreenSize(getContext())[0]/2.0);
		viewPager.startAutoScroll(1000);
		viewPager.setItemClickListener(carouselItemClickListener);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.live_more:
			startActivity(new Intent(getActivity(), LiveListActivity.class));
			break;

		case R.id.dance_more:
			startActivity(new Intent(getActivity(), DanceListActivity.class));
			break;
		}
	}

	WuzhaiServiceCallbackAdapter callbackAdapter = new WuzhaiServiceCallbackAdapter(){

		@Override
		public void onGetVideoListCompleted(ArrayList<VideoEntity> video_list,
				int next_page) {
			Log.d("yue.huang", "onGetVideoListCompleted:"+video_list);
			if(video_list!=null && video_list.size()!=0){
				videoList.addAll(video_list);
				videoAdapter.notifyDataSetChanged();
			}
		}

		@Override
		public void onGetCarouselListDone(ArrayList<Carousel> carouselList) {
			if(carouselList!=null){
				carouselDataList = carouselList;
				ArrayList<View> pages = new ArrayList<View>();
				for(Carousel carousel : carouselList){
					pages.add(createViewPagerPage(getActivity(), carousel.getTitle(), carousel.getImageUrl()));
				}
				viewPager.addPageList(pages);
		        indicator.setViewPager(viewPager);
		        indicator.setRadius(Utils.dpToPx(3));
			}
		}
		
		@Override
		public void onGetLiveRoomListDone(ArrayList<LiveRoomEntity> liveRoomList) {
			Log.d("yue.huang", "onGetLiveRoomListDone:"+liveRoomList);
			if(liveRoomList!=null){
				roomList.addAll(liveRoomList);
				liveAdapter.notifyDataSetChanged();
			}
		}
	};

	private View createViewPagerPage(Context context,String title,String picUrl){
		View view = LayoutInflater.from(context).inflate(R.layout.layout_homecarouse_layout, null);
		ImageView imageView = (ImageView)view.findViewById(R.id.pic);
		TextView textView = (TextView)view.findViewById(R.id.title);
		Picasso.with(context).load(picUrl).resize(800, 400).centerInside().into(imageView);
		textView.setText(title);
		return view;
	}

    private AutoScrollViewPager.CarouselItemClickListener carouselItemClickListener = new AutoScrollViewPager.CarouselItemClickListener() {
        @Override
		public void onItemClick(View v, int position) {
			Log.d("yue.huang", "colick position:" + position);
		}
    };
}
