package com.wuzhai.app.main.welfare;

import java.util.ArrayList;
import java.util.HashMap;

import com.wuzhai.app.R;
import com.wuzhai.app.application.WuZhaiApplication;
import com.wuzhai.app.main.MainActivity;
import com.wuzhai.app.main.WuzhaiService;
import com.wuzhai.app.main.WuzhaiServiceCallbackAdapter;
import com.wuzhai.app.main.widget.LoadMoreRecyclerView;
import com.wuzhai.app.main.widget.WelfareListAdapter;
import com.wuzhai.app.main.widget.LoadMoreRecyclerView.LoadMoreCallBack;
import com.wuzhai.app.main.widget.WelfareListAdapter.WelfareListItemOnClickListener;
import com.wuzhai.app.objects.MediaObject;
import com.wuzhai.app.objects.Photo;
import com.wuzhai.app.tools.Utils;
import com.wuzhai.app.widget.RefreshableView;
import com.wuzhai.app.widget.RefreshableView.PullToRefreshListener;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView.LayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class WelfareFragment extends Fragment {
	private WuzhaiService service;
	private Button allTrends;
	private Button followedTrends;
	private RefreshableView pullRefreshLayout;
	private LoadMoreRecyclerView trendsList;
	private WelfareListAdapter adapter;
	private HashMap<Integer, String> photoTypeMap;
	private int currentPhotoTypeId = 1;
	private int nextPage;
	private ArrayList<Photo> photoList;

	private View clickedFollowBtn;
	private View clickedCommentView;
	private int clickedCommentIndex;
	private final int PUBLISH_COMMENT = 100;
    private final int SELECTED_TYPE = 200;
    private String currentType = null;

	@Override
	@Nullable
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		service = ((WuZhaiApplication)getActivity().getApplication()).getService();
		service.setCallBack(callbackAdapter);
		service.getType(WuzhaiService.PHOTO_TYPE);
		View view = LayoutInflater.from(getContext()).inflate(R.layout.fragment_welfare, null);
		initView(view);
		return view;
	}

	private void initView(View rootView){
		allTrends = (Button)rootView.findViewById(R.id.all_trends);
		followedTrends = (Button)rootView.findViewById(R.id.followed_trends);
		pullRefreshLayout = (RefreshableView)rootView.findViewById(R.id.pullRefreshLayout);
		pullRefreshLayout.setOnRefreshListener(refreshListener, 2);
//		pullRefreshLayout.finishRefreshing();

		trendsList = (LoadMoreRecyclerView)rootView.findViewById(R.id.trends_list);
		LayoutManager layoutManager = new LinearLayoutManager(getContext());
		trendsList.setLayoutManager(layoutManager);
		trendsList.setLoadMoreCallBack(loadMoreCallBack);

		photoList = new ArrayList<Photo>();
		adapter = new WelfareListAdapter(getContext(),photoList);
		adapter.setItemOnClickListener(itemOnClickListener);

		trendsList.setAdapter(adapter);

		allTrends.setOnClickListener(onClickListener);
		followedTrends.setOnClickListener(onClickListener);
	}

	private OnClickListener onClickListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.all_trends:
				allTrends.setBackgroundResource(R.drawable.left_oval_solid_bg_pressed);
				allTrends.setTextColor(0xfff2f2f2);
				followedTrends.setBackgroundResource(R.drawable.right_oval_solid_bg_nomal);
				followedTrends.setTextColor(0xffffffff);
				break;

			case R.id.followed_trends:
				followedTrends.setBackgroundResource(R.drawable.right_oval_solid_bg_pressed);
				followedTrends.setTextColor(0xfff2f2f2);
				allTrends.setBackgroundResource(R.drawable.left_oval_solid_bg_nomal);
				allTrends.setTextColor(0xffffffff);
				break;
			}
		}
	};

	private WuzhaiServiceCallbackAdapter callbackAdapter = new WuzhaiServiceCallbackAdapter(){

		@Override
		public void onGetPhotoListDone(ArrayList<Photo> photo_list,int next_page) {
            Log.d("yue.huang","onGetPhotoListDone:"+photo_list.size());
			if(photo_list!=null){
				nextPage = next_page;
				if(pullRefreshLayout.isRefreshing()){
					photoList.clear();
				}
				photoList.addAll(photo_list);
				trendsList.getAdapter().notifyDataSetChanged();
				pullRefreshLayout.finishRefreshing();
			}
		}

		@Override
		public void onGetTypeCompleted(HashMap<Integer, String> typeMap) {
			if(typeMap!=null){
				photoTypeMap = typeMap;
                currentType = photoTypeMap.values().toArray(new String[0])[0];
                ((MainActivity)getActivity()).updateTextToolBar(currentType, R.drawable.arrow_solid_down, new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        startActivityForResult(new Intent(getContext(),WelfareTypeSelectActivity.class),SELECTED_TYPE);
                    }
                });
                /*默认显示第一类图片*/
                service.getPhotoList(currentPhotoTypeId, null, 1);
			}
		}

		@Override
		public void onCompleted(int result) {
			switch (result) {
			case WuzhaiService.FOLLOW_USER_SUCC:
				clickedFollowBtn.setEnabled(false);
				((Button)clickedFollowBtn).setText("已关注");
				clickedFollowBtn.setBackgroundColor(0xf2f2f2f2);
				Toast.makeText(getContext(), "关注成功", Toast.LENGTH_SHORT).show();
				break;
			case WuzhaiService.FOLLOW_USER_FAL:
				Toast.makeText(getContext(), "关注失败", Toast.LENGTH_SHORT).show();
				break;
			case WuzhaiService.LIKE_SUCC:
				Toast.makeText(getContext(), "喜欢成功", Toast.LENGTH_SHORT).show();
				break;
			case WuzhaiService.LIKE_FAL:
				Toast.makeText(getContext(), "喜欢失败", Toast.LENGTH_SHORT).show();
				break;
			case WuzhaiService.CANCLE_LIKABLE_SUCC:
				Toast.makeText(getContext(), "取消喜欢成功", Toast.LENGTH_SHORT).show();
				break;
			case WuzhaiService.CANCLE_LIKABLE_FAL:
				Toast.makeText(getContext(), "取消喜欢失败", Toast.LENGTH_SHORT).show();
				break;
			}
		}
	};

	private WelfareListItemOnClickListener itemOnClickListener = new WelfareListItemOnClickListener() {
		@Override
		public void onPicture(MediaObject mediaObject) {
			Intent intent = new Intent(getContext(), WelfareDetialActivity.class);
			intent.putExtra("id", mediaObject.getId());
            intent.putExtra("publisher_id", mediaObject.getPublisherId());
			startActivity(intent);
		}

		@Override
		public void onLikeClick(MediaObject mediaObject,boolean isLike) {
			if(isLike){
				service.like("Photo", mediaObject.getId());
			}else {
				service.cancleLike("Photo", mediaObject.getId());
			}
		}

		@Override
		public void onFollowBtnClick(MediaObject mediaObject,View v) {
			service.followUser(mediaObject.getPublisherId());
			clickedFollowBtn = v;
		}

		@Override
		public void onCommentClick(MediaObject mediaObject,View v,int index) {
			clickedCommentView = v;
			clickedCommentIndex = index;
			Intent intent = new Intent(getContext(), CommentActivity.class);
			intent.putExtra("type", "Photo");
			intent.putExtra("id", mediaObject.getId());
			startActivityForResult(intent, PUBLISH_COMMENT);
		}
	};
	
	private PullToRefreshListener refreshListener = new PullToRefreshListener() {
		@Override
		public void onRefresh() {
			Log.d("yue.huang", "onRefresh");
			trendsList.removeFooterView();
			if (Utils.isNetworkAvailable(getContext())) {
				service.getPhotoList(currentPhotoTypeId, null, 1);
			} else {
				Toast.makeText(getContext(), "无网络连接",Toast.LENGTH_SHORT).show();
				pullRefreshLayout.finishRefreshing();
			}
		}
	};

	private LoadMoreCallBack loadMoreCallBack = new LoadMoreCallBack() {
		@Override
		public void loadMore() {
			if (nextPage != 0) {
				if (Utils.isNetworkAvailable(getContext())) {
					trendsList.setFooterViewLoaderMore();
					service.getPhotoList(currentPhotoTypeId, null,nextPage);
				} else {
					Toast.makeText(getContext(), "无网络连接",Toast.LENGTH_SHORT).show();
				}
			}else {
				trendsList.setFooterNoMoreToLoad();
			}
		}
	};

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case PUBLISH_COMMENT:
                Log.d("yue.huang", "PUBLISH_COMMENT:" + data.getIntExtra("comment_count", 0));
                int currentCommentCount = photoList.get(clickedCommentIndex).getCommentsCount();
                Log.d("yue.huang", "currentCommentCount:" + currentCommentCount);
                int commentCount = data.getIntExtra("comment_count", 0);
                photoList.get(clickedCommentIndex).setCommentsCount(currentCommentCount + commentCount);
                ((TextView) clickedCommentView.findViewById(R.id.comment_count)).setText(currentCommentCount + commentCount + "");
                break;
            case SELECTED_TYPE:
				service.setCallBack(callbackAdapter);
				if(data!=null){
					ArrayList<Integer> types = data.getIntegerArrayListExtra("types");
                    ArrayList<String> names = data.getStringArrayListExtra("names");
                    currentType = names.get(0);
                    updateTitle();
					if(types!=null && types.size()!=0){
						if (types != null && types.size() != 0) {
							photoList.clear();
							service.getPhotoList(types.get(0), null, 1);
						}
					}
				}
				break;
        }
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if(!hidden){
            updateTitle();
        }
    }

    private void updateTitle(){
        if(currentType!=null){
            ((MainActivity)getActivity()).updateTextToolBar(currentType, R.drawable.arrow_solid_down, new OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivityForResult(new Intent(getContext(),WelfareTypeSelectActivity.class),SELECTED_TYPE);
                }
            });
        }
    }
}
