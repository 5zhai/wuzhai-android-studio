package com.wuzhai.app.main.market.widget;

import java.util.ArrayList;

import com.wuzhai.app.R;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class AddressListAdapter extends BaseAdapter {

	private ArrayList<DeliveryAddress> addressList;
	private Context context;

	public AddressListAdapter(Context context,ArrayList<DeliveryAddress> addressList) {
		this.context = context;
		this.addressList = addressList;
	}

	@Override
	public int getCount() {
		return addressList.size();
	}

	@Override
	public Object getItem(int position) {
		return addressList.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder viewHolder;
		if (convertView == null) {
			convertView = LayoutInflater.from(context).inflate(R.layout.address_list_item, null);
			viewHolder = new ViewHolder();
			viewHolder.consignee = (TextView) convertView.findViewById(R.id.consignee);
			viewHolder.tel = (TextView) convertView.findViewById(R.id.tel);
			viewHolder.address = (TextView) convertView.findViewById(R.id.address);
			viewHolder.checkFlag = (ImageView) convertView.findViewById(R.id.check_flag);
			convertView.setTag(viewHolder);
		}
		DeliveryAddress address = addressList.get(position);
		viewHolder = (ViewHolder) convertView.getTag();
		viewHolder.consignee.setText(address.getReceiverName());
		viewHolder.tel.setText(address.getTel());
		viewHolder.address.setText(address.getArea() + address.getStreet());
		return convertView;
	}

	public static class ViewHolder {
		public TextView consignee;
		public TextView tel;
		public TextView address;
		public ImageView checkFlag;
	}
}
