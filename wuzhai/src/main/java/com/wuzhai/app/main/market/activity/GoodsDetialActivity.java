package com.wuzhai.app.main.market.activity;

import java.util.ArrayList;
import com.squareup.picasso.Picasso;
import com.viewpagerindicator.TabPageIndicator;
import com.wuzhai.app.R;
import com.wuzhai.app.application.WuZhaiApplication;
import com.wuzhai.app.main.WuzhaiService;
import com.wuzhai.app.main.WuzhaiServiceCallbackAdapter;
import com.wuzhai.app.main.market.widget.Goods;
import com.wuzhai.app.main.widget.CommentListAdapter;
import com.wuzhai.app.objects.Comment;
import com.wuzhai.app.person.activity.PersonalCenterActivity;
import com.wuzhai.app.tools.Utils;
import com.wuzhai.app.widget.TitleToolbarActivity;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import io.rong.imkit.RongIM;

public class GoodsDetialActivity extends TitleToolbarActivity implements
		OnClickListener {
	// 遗留 scrollview viewpager冲突问题，商量是否可以改成淘宝那样，viewpager中的三个page
	private WuzhaiService service;
	private ViewPager viewpager;
	private TabPageIndicator indicator;
	private TextView toolbarTitle;
	private ImageView mainPic;
	private TextView price;
	private TextView saleCount;
	private TextView publisherPlace;
	private ImageView publisherAvatar;
	private TextView goodsSummary;
	private TextView serviceCollect;
	private TextView serviceShare;
	private TextView serviceComment;
	private Button buyBtn;
	private int goodsId;
	private Goods goods;
	private boolean isCollected = false;
	private Drawable collectDrawable;
	private Drawable unCollectDrawable;
	private Drawable connectionDrawable;
	private Drawable shareDrawable;
	private int commentsCount = 0;
	private ViewPagerAdapter adapter;
	private CommentListAdapter commentListAdapter;
	private ArrayList<Comment> commentList = new ArrayList<>();

	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_goods_detial);
		service = ((WuZhaiApplication)getApplication()).getService();
		service.setCallBack(callbackAdapter);
		goodsId = getIntent().getIntExtra("id", 0);
		service.getGoodsDetial(goodsId);
		service.Iscollected(WuzhaiService.TYPE_COMMODITY, goodsId);
		service.getCommentList(WuzhaiService.TYPE_COMMODITY, goodsId);
		setTitle("详情");
		initView();
	}

	private void initView() {
		collectDrawable = Utils.getDrawableFromResources(this,R.drawable.video_icon_not_collect,25,25);
		unCollectDrawable = Utils.getDrawableFromResources(this,R.drawable.icon_collect_footer,25,25);
		connectionDrawable = Utils.getDrawableFromResources(this, R.drawable.comment_list_big, 25, 25);
		shareDrawable = Utils.getDrawableFromResources(this, R.drawable.icon_share_footer, 25, 25);
		mainPic = (ImageView)findViewById(R.id.main_pic);
		price = (TextView)findViewById(R.id.price);
		saleCount = (TextView)findViewById(R.id.bought_count);
		publisherPlace = (TextView)findViewById(R.id.publisherPlace);
		publisherAvatar = (ImageView)findViewById(R.id.publisherAvatar);
		goodsSummary = (TextView)findViewById(R.id.goodsSummary);
		serviceCollect = (TextView)findViewById(R.id.service_collect);
		serviceCollect.setOnClickListener(this);
		serviceCollect.setCompoundDrawables(null, unCollectDrawable, null, null);
		serviceComment = (TextView)findViewById(R.id.service_comment);
		serviceComment.setCompoundDrawables(null, connectionDrawable, null, null);
		serviceComment.setOnClickListener(this);
		serviceShare = (TextView)findViewById(R.id.service_share);
		serviceShare.setCompoundDrawables(null, shareDrawable, null, null);
		serviceShare.setOnClickListener(this);
		buyBtn = (Button) findViewById(R.id.buyNow);
		buyBtn.setOnClickListener(this);

		// 具体实现时考虑两个view详情和评论是写在Fragment中还是写在activity中
		View view = LayoutInflater.from(this).inflate(
				R.layout.viewpager_goods_detial, null);
		ListView listView = new ListView(this);
		listView.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT,
				LayoutParams.MATCH_PARENT));
		commentListAdapter = new CommentListAdapter(this);
		commentListAdapter.setCommentList(commentList);
		listView.setAdapter(commentListAdapter);

		ArrayList<View> viewList = new ArrayList<View>();
		viewList.add(view);
		viewList.add(listView);
		indicator = (TabPageIndicator) findViewById(R.id.goods_detial_indicator);
		viewpager = (ViewPager) findViewById(R.id.goods_detial_viewpager);
		adapter = new ViewPagerAdapter(this, viewList);
	}

	private void initData(Goods goods){
		if(goods!=null){
			this.goods = goods;
			String mainImagePath = goods.getMainImage();
			if(!TextUtils.isEmpty(mainImagePath)){
				Picasso.with(this).load(mainImagePath).resize(300, 200).centerInside().into(mainPic);
			}
			price.setText(""+goods.getPrice());
			saleCount.setText(""+goods.getOrderCount());
			publisherPlace.setText(goods.getCity());
			Picasso.with(this).load(goods.getPublisherAvatar()).resize(80, 80).centerInside().into(publisherAvatar);
			goodsSummary.setText(goods.getTitle());
			commentsCount = goods.getCommentsCount();
			if(goods.isCollected()){
				isCollected = true;
				serviceCollect.setCompoundDrawables(null, collectDrawable, null, null);
			}else {
				isCollected = false;
				serviceCollect.setCompoundDrawables(null, unCollectDrawable, null, null);
			}
			//刷新评论数
			viewpager.setAdapter(adapter);
			indicator.setViewPager(viewpager);
		}

	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.buyNow:
			//以后可以传一个goods对象或goodsdetial对象过去
			Intent intent = new Intent(this, GoodsPaymentActivity.class);
			intent.putExtra("goods", goods);
			startActivity(intent);
			break;
		case R.id.service_collect:
			if (!isCollected) {
				service.collection(WuzhaiService.TYPE_COMMODITY,goods.getId());
			} else {
				service.cancleCollection(WuzhaiService.TYPE_COMMODITY,goods.getId());
			}
			break;
		case R.id.service_share:
			Utils.onKeyShareOperation(this, goods.getTitle(),
					"https://www.5yuzhai.com/",goods.getMainImage(),goods.getDesc(), null, "好厉害", "吾宅", null);
			break;
		case R.id.service_comment:
            RongIM.getInstance().startPrivateChat(GoodsDetialActivity.this, goods.getPublisherAccessKey(), "title");
            break;

		}
	}

    private WuzhaiServiceCallbackAdapter callbackAdapter = new WuzhaiServiceCallbackAdapter(){

		@Override
		public void onGetGoodsDetialCompleted(Goods goods) {
			Log.d("yue.huang", "onGetGoodsDetialCompleted:"+goods);
			initData(goods);
		}

		@Override
		public void onCompleted(int result) {
			switch (result) {
			case WuzhaiService.COLLECTE_SUCC:
				isCollected = true;
				serviceCollect.setCompoundDrawables(null, collectDrawable, null, null);
				Toast.makeText(GoodsDetialActivity.this, "收藏成功", Toast.LENGTH_SHORT).show();
				break;
			case WuzhaiService.CANCLE_COLLECTE_SUCC:
				isCollected = false;
				serviceCollect.setCompoundDrawables(null, unCollectDrawable, null, null);
				Toast.makeText(GoodsDetialActivity.this, "取消收藏成功", Toast.LENGTH_SHORT).show();
				break;
			}
		}

//		@Override
//		public void onGetCollectionableListCompleted(
//				ArrayList<Collectionable> collectionable_List, int next_page) {
//			for(Collectionable collectionable : collectionable_List){
//				if(collectionable.getCollectionableId() == goods.getId() && collectionable.getCollectionableType().equals(WuzhaiService.TYPE_COMMODITY)){
//					isCollected = true;
//					serviceCollect.setCompoundDrawables(null, collectDrawable, null, null);
//					return;
//				}
//			}
//			isCollected = false;
//			serviceCollect.setCompoundDrawables(null, unCollectDrawable, null, null);
//		}

		public void onGetCommentListDone(ArrayList<Comment> comments) {
			commentList.addAll(comments);
			commentListAdapter.setCommentList(commentList);
		}

		@Override
		public void onCheckIsCollectedDone(boolean isCollected) {
			if(isCollected){
				isCollected = true;
				serviceCollect.setCompoundDrawables(null, collectDrawable, null, null);
			}else {
				isCollected = false;
				serviceCollect.setCompoundDrawables(null, unCollectDrawable, null, null);
			}
		};
    };

	private class ViewPagerAdapter extends PagerAdapter {

		private ArrayList<View> viewList;
		private Context context;

		public ViewPagerAdapter(Context context, ArrayList<View> viewList) {
			this.context = context;
			this.viewList = viewList;
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return viewList.size();
		}

		@Override
		public boolean isViewFromObject(View arg0, Object arg1) {
			// TODO Auto-generated method stub
			return arg0 == arg1;
		}

		@Override
		public Object instantiateItem(ViewGroup container, int position) { // 这个方法用来实例化页卡
			container.addView(viewList.get(position), 0);// 添加页卡
			return viewList.get(position);
		}

		@Override
		public CharSequence getPageTitle(int position) {
			if (position == 0) {
				return "商品详情";
			} else {
				return "评论("+commentsCount+")";// 以后可以先获取评论数，然后再这里返回评论数
			}
		}

	}
}
