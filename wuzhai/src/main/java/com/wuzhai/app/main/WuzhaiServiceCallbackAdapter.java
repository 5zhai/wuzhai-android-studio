package com.wuzhai.app.main;

import java.util.ArrayList;
import java.util.HashMap;

import com.wuzhai.app.main.WuzhaiService.CallBack;
import com.wuzhai.app.main.home.widget.News;
import com.wuzhai.app.main.market.widget.Aftercases;
import com.wuzhai.app.main.market.widget.ComicConNews;
import com.wuzhai.app.main.market.widget.DeliveryAddress;
import com.wuzhai.app.main.market.widget.Goods;
import com.wuzhai.app.main.video.widget.LiveRoomEntity;
import com.wuzhai.app.main.video.widget.VideoEntity;
import com.wuzhai.app.objects.Carousel;
import com.wuzhai.app.objects.Collectionable;
import com.wuzhai.app.objects.Comment;
import com.wuzhai.app.objects.Gift;
import com.wuzhai.app.objects.GiftCategory;
import com.wuzhai.app.objects.Likable;
import com.wuzhai.app.objects.MediaObject;
import com.wuzhai.app.objects.Order;
import com.wuzhai.app.objects.Photo;
import com.wuzhai.app.objects.Photographer;
import com.wuzhai.app.objects.Tag;
import com.wuzhai.app.objects.UserCenterInfo;
import com.wuzhai.app.person.widget.FollowerFans;

public class WuzhaiServiceCallbackAdapter implements CallBack{

	@Override
	public void onCompleted(int result) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onGetDeliveryAddressCompleted(
			ArrayList<DeliveryAddress> addressList) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onGetComicConNewsCompleted(
			ArrayList<ComicConNews> comicConNewsList,int next_page) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onGetLatestComicConNewsCompleted(
			ArrayList<ComicConNews> comicConNewsList) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onGetRecommendationsCompleted(
			ArrayList<News> recommendationsList,int nextPage) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onUploadFileCompleted(ArrayList<String> picPathList) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onGetGoodsCompleted(ArrayList<Goods> goodsList, int nextPage) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onGetTagsCompleted(ArrayList<Tag> tagsList) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onGetTypeCompleted(HashMap<Integer, String> typeMap) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onGetPhotographerCompleted(
			ArrayList<Photographer> photographerList, int next_page) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onGetCollectionableListCompleted(
			ArrayList<Collectionable> collectionable_List, int next_page) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onGetAftercasesCompleted(ArrayList<Aftercases> aftercasesList,
			int next_page) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onGetGoodsDetialCompleted(Goods goods) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onGetDefaultDeliveryAddressCompleted(DeliveryAddress address) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onGetVideoListCompleted(ArrayList<VideoEntity> videoList,
			int next_page) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onCreateOrderCompleted(int order_id) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onPayWithBalanceCompleted(String msg) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onGetOrderListCompleted(ArrayList<Order> order_list) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onGetLikableListCompleted(ArrayList<Likable> likable_List,
			int next_page) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onCheckIsLikeDone(boolean isLike) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onCheckIsCollectedDone(boolean isCollected) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onGetCommentListDone(ArrayList<Comment> comments) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onGetAboutUsDone(String result) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onGetFollowersCompleted(ArrayList<FollowerFans> followersList) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onGetGiftListDone(ArrayList<Gift> gifts, int nextPage) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onGetGiftCategoryListDone(ArrayList<GiftCategory> giftCategorys) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onGetUserCenterInfoDone(UserCenterInfo userCenterInfo) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onGetHotVideosDone(ArrayList<VideoEntity> videoList) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onGetHomeRecommendationsDone(ArrayList<News> recommendationsList) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onGetPhotoListDone(ArrayList<Photo> photoList,int nextPage) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onGetCarouselListDone(ArrayList<Carousel> carouselList) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onGetLiveRoomListDone(ArrayList<LiveRoomEntity> liveRoomList) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onGetMediaDetialDone(MediaObject entity) {
	}

	@Override
	public void onGetServiceDetailDone(Aftercases aftercases) {
	}

	@Override
	public void onSendGiftDone(boolean result) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onGetVerificationCodeDone(boolean isSucc, String msg) {

	}

	@Override
	public void onCheckFollowedDone(boolean isFollowed) {

	}
}
