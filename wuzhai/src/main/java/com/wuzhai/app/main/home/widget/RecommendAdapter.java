package com.wuzhai.app.main.home.widget;

import java.util.ArrayList;

import com.squareup.picasso.Picasso;
import com.wuzhai.app.R;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class RecommendAdapter extends BaseAdapter {

	private ArrayList<News> newsList;
	private Context context;
	public RecommendAdapter(Context context,ArrayList<News> newsList){
		this.context = context;
		this.newsList = newsList;
	}
	public RecommendAdapter(Context context){
		this.context = context;
	}
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return newsList.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return newsList.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if(convertView == null){
			ViewHolder holder = new ViewHolder();
			convertView = LayoutInflater.from(context).inflate(R.layout.listview_recommend, null);
			holder.icon = (ImageView)convertView.findViewById(R.id.red_icon);
			holder.title = (TextView)convertView.findViewById(R.id.red_title);
			holder.subtitle = (TextView)convertView.findViewById(R.id.red_subtitle);
			convertView.setTag(holder);
		}
		ViewHolder holder = (ViewHolder)convertView.getTag();
		Picasso.with(context).load(newsList.get(position).getIconURL()).into(holder.icon);
		holder.title.setText(newsList.get(position).getTitle());
		holder.subtitle.setText(newsList.get(position).getSubtitle());
		return convertView;
	}

	private static class ViewHolder{
		ImageView icon;
		TextView title;
		TextView subtitle;
	}

	public void setRecommendList(ArrayList<News> newsList){
		this.newsList = newsList;
		notifyDataSetChanged();
	}
}
