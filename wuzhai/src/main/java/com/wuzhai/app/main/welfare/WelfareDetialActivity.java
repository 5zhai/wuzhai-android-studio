package com.wuzhai.app.main.welfare;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AbsListView.LayoutParams;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView.ScaleType;

import com.squareup.picasso.Picasso;
import com.wuzhai.app.R;
import com.wuzhai.app.application.WuZhaiApplication;
import com.wuzhai.app.main.WuzhaiService;
import com.wuzhai.app.main.WuzhaiServiceCallbackAdapter;
import com.wuzhai.app.objects.MediaObject;
import com.wuzhai.app.objects.Photo;
import com.wuzhai.app.person.activity.GiftGiveActivity;
import com.wuzhai.app.tools.Utils;
import com.wuzhai.app.widget.TitleToolbarActivity;

import static com.mob.tools.gui.BitmapProcessor.start;

public class WelfareDetialActivity extends TitleToolbarActivity {
	private ImageView userAvatar;
	private TextView username;
	private TextView publishTime;
	private Button followBtn;
	private TextView picInfo;
	private LinearLayout lableContainer;
	private ListView detailPicList;
	private WuzhaiService service;
	private ImageView collectBtn;
	private ImageView shareBtn;
	private boolean isFollowed = false;
	private boolean isCollected = false;
	private boolean isLiked = false;
	private Photo photo;
	private int id;
    private int publisherId;
	private RelativeLayout likeBtn;
	private ImageView likeIcon;
	private TextView likeCountTV;
	private RelativeLayout commentBtn;
	private TextView commentCountTV;
	private int likeCount;
	private int commentCount = 0;
	private ImageView bigImg;
	private ImageView giftBtn;
	
	private final int PUBLISH_COMMENT = 100;
	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_welfaredetial);
		initView();
        service = ((WuZhaiApplication)getApplication()).getService();
        service.setCallBack(callbackAdapter);
        id = getIntent().getIntExtra("id",-1);
        publisherId = getIntent().getIntExtra("publisher_id",-1);
        service.getMediaDetial(WuzhaiService.TYPE_PHOTO,id);
	}
	
	private void initView(){
		userAvatar = (ImageView)findViewById(R.id.userAvatar);
		username = (TextView)findViewById(R.id.username);
		publishTime = (TextView)findViewById(R.id.publish_time);
		followBtn = (Button)findViewById(R.id.followbtn);
		followBtn.setOnClickListener(clickListener);
		picInfo = (TextView)findViewById(R.id.pic_info);
		lableContainer = (LinearLayout)findViewById(R.id.lable_container);
		detailPicList = (ListView)findViewById(R.id.detail_pic_list);
		collectBtn = (ImageView)findViewById(R.id.welfare_collect);
		collectBtn.setOnClickListener(clickListener);
		shareBtn = (ImageView)findViewById(R.id.welfare_share);
		shareBtn.setOnClickListener(clickListener);
		likeBtn = (RelativeLayout)findViewById(R.id.welfare_like);
		likeBtn.setOnClickListener(clickListener);
		likeIcon = (ImageView)findViewById(R.id.like_icon);
		likeCountTV = (TextView)findViewById(R.id.like_count);
		commentBtn = (RelativeLayout)findViewById(R.id.welfare_comment);
		commentBtn.setOnClickListener(clickListener);
		commentCountTV = (TextView)findViewById(R.id.comment_count);
		bigImg = (ImageView)findViewById(R.id.big_img);
		bigImg.setOnClickListener(clickListener);
        giftBtn = (ImageView)findViewById(R.id.welfare_gift);
        giftBtn.setOnClickListener(clickListener);
	}

    private void initData(Photo photo){
        Picasso.with(this).load(photo.getPublisherAvatar()).resize(80, 80).centerInside().into(userAvatar);
        username.setText(photo.getPublisherName());
        publishTime.setText(Utils.getDifferenceWithCurrentTime(photo.getCreatedAt()));
        picInfo.setText(photo.getDesc());
        setTags(photo.getTags());
        if(photo.isFollowed()){
            followBtn.setText("已关注");
            followBtn.setEnabled(false);
        }
        if(photo.isCollected()){
            isCollected = true;
            collectBtn.setImageResource(R.drawable.icon_collection_black_press);
        }else {
            isCollected = false;
            collectBtn.setImageResource(R.drawable.icon_collection_black_normal);
        }
        if(photo.isLiked()){
            isLiked = true;
            likeIcon.setImageResource(R.drawable.icon_praise_black_press);
        }else {
            isLiked = false;
            likeIcon.setImageResource(R.drawable.icon_praise_black_normal);
        }
        DetailPicAdapter adapter = new DetailPicAdapter(this,photo.getPicturePaths());
        detailPicList.setAdapter(adapter);
        detailPicList.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                String imageUrl = parent.getAdapter().getItem(position).toString();
                Picasso.with(WelfareDetialActivity.this).load(imageUrl).into(bigImg);
                bigImg.setBackgroundColor(0x00000000);
                bigImg.setVisibility(View.VISIBLE);
                bigImg.setScaleX(0.5f);
                bigImg.setScaleY(0.5f);
                bigImg.animate().scaleX(1).scaleY(1).setDuration(200).setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        bigImg.setBackgroundColor(0xff000000);
                    }
                }).start();

            }
        });
        likeCount = photo.getLikesCount();
        likeCountTV.setText(likeCount+"");
        commentCount = photo.getCommentsCount();
        commentCountTV.setText(commentCount+"");
    }
	
	private OnClickListener clickListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			if(!Utils.isNetworkAvailable(WelfareDetialActivity.this)){
				Toast.makeText(WelfareDetialActivity.this, "无网络连接", Toast.LENGTH_SHORT).show();
				return;
			}
            if(photo == null){
                return;
            }
			switch (v.getId()) {
			case R.id.followbtn:
				if(!isFollowed){
					service.followUser(photo.getPublisherId());
				}
				break;
			case R.id.welfare_collect:
				if(!isCollected){
					service.collection(WuzhaiService.TYPE_PHOTO, photo.getId());
				}else {
					service.cancleCollection(WuzhaiService.TYPE_PHOTO, photo.getId());
				}
				break;
			case R.id.welfare_share:
				Utils.onKeyShareOperation(WelfareDetialActivity.this, photo.getTitle(),
						"https://www.5yuzhai.com/",photo.getPicturePaths()[0],photo.getDesc(), null, "好厉害", "吾宅", null);
				break;
			case R.id.welfare_like:
				likeOperation(isLiked);
				break;
				
			case R.id.welfare_comment:
				gotoComment();
				break;
            case R.id.welfare_gift:
                Intent intent = new Intent(WelfareDetialActivity.this, GiftGiveActivity.class);
                intent.putExtra("receiver_id",publisherId);
                startActivity(intent);
                break;
			case R.id.big_img:
				bigImg.setVisibility(View.GONE);
				break;
			}
		}
	};
	
	private class DetailPicAdapter extends BaseAdapter{

		private Context context;
		private String[] picPaths;
		public DetailPicAdapter(Context context,String[] picPaths){
			this.context = context;
			this.picPaths = picPaths;
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return picPaths.length;
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return picPaths[position];
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			if(convertView == null){
				ImageView imageView = new ImageView(context);
				imageView.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT,Utils.dpToPx(200)));
				imageView.setScaleType(ScaleType.FIT_XY);
				convertView = imageView;
			}
			Picasso.with(context).load(picPaths[position]).resize(300, 150).centerCrop().into((ImageView)convertView);
			return convertView;
		}
	}
	
	private void setTags(String[] tags){
		for(String tag: tags){
			TextView tagTextView = new TextView(this);
			tagTextView.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT));
			tagTextView.setPadding(Utils.dpToPx(4), Utils.dpToPx(4), Utils.dpToPx(4), Utils.dpToPx(4));
			tagTextView.setTextColor(0xffff7e9c);
			tagTextView.setTextSize(12);
			tagTextView.setBackgroundResource(R.drawable.photographer_lable_bg);
			tagTextView.setText(tag);
			lableContainer.addView(tagTextView);
		}
	}
	
	private void gotoComment(){
		Intent intent = new Intent(this, CommentActivity.class);
		intent.putExtra("type", "Photo");
		intent.putExtra("id", photo.getId());
		startActivityForResult(intent, PUBLISH_COMMENT);
	}
	
	private void likeOperation(boolean isLiked){
		if(!isLiked){
			service.like("Photo", photo.getId());
		}else {
			service.cancleLike("Photo", photo.getId());
		}
	}
	
	private WuzhaiServiceCallbackAdapter callbackAdapter = new WuzhaiServiceCallbackAdapter(){
		@Override
		public void onCompleted(int result) {
			switch (result) {
			case WuzhaiService.FOLLOW_USER_SUCC:
				followBtn.setText("已关注");
				followBtn.setEnabled(false);
				Toast.makeText(WelfareDetialActivity.this, "关注成功", Toast.LENGTH_SHORT).show();
				break;
			case WuzhaiService.FOLLOW_USER_FAL:
				Toast.makeText(WelfareDetialActivity.this, "关注失败", Toast.LENGTH_SHORT).show();
				break;
			case WuzhaiService.LIKE_SUCC:
				isLiked = true;
				likeCount += 1;
				likeCountTV.setText(likeCount+"");
				likeIcon.setImageResource(R.drawable.icon_praise_black_press);
				Toast.makeText(WelfareDetialActivity.this, "喜欢成功", Toast.LENGTH_SHORT).show();
				break;
			case WuzhaiService.LIKE_FAL:
				Toast.makeText(WelfareDetialActivity.this, "喜欢失败", Toast.LENGTH_SHORT).show();
				break;
			case WuzhaiService.CANCLE_LIKABLE_SUCC:
				isLiked = false;
				likeCount -= 1;
				likeCountTV.setText(likeCount+"");
				likeIcon.setImageResource(R.drawable.icon_praise_black_normal);
				Toast.makeText(WelfareDetialActivity.this, "取消喜欢成功", Toast.LENGTH_SHORT).show();
				break;
			case WuzhaiService.CANCLE_LIKABLE_FAL:
				Toast.makeText(WelfareDetialActivity.this, "取消喜欢失败", Toast.LENGTH_SHORT).show();
				break;
			case WuzhaiService.COLLECTE_SUCC:
				isCollected = true;
				collectBtn.setImageResource(R.drawable.icon_collection_black_press);
				Toast.makeText(WelfareDetialActivity.this, "收藏成功", Toast.LENGTH_SHORT).show();
				break;
			case WuzhaiService.COLLECTE_FAL:
				Toast.makeText(WelfareDetialActivity.this, "收藏失败", Toast.LENGTH_SHORT).show();
				break;
			case WuzhaiService.CANCLE_COLLECTE_SUCC:
				isCollected = false;
				collectBtn.setImageResource(R.drawable.icon_collection_black_normal);
				Toast.makeText(WelfareDetialActivity.this, "取消收藏成功", Toast.LENGTH_SHORT).show();
				break;
			case WuzhaiService.CANCLE_COLLECTE_FAL:
				Toast.makeText(WelfareDetialActivity.this, "取消收藏出错", Toast.LENGTH_SHORT).show();
				break;
			}
		}

        @Override
        public void onGetMediaDetialDone(MediaObject entity) {
            if(entity!=null){
                photo = (Photo)entity;
                initData(photo);
            }else {
                Toast.makeText(WelfareDetialActivity.this,"获取详情出错",Toast.LENGTH_SHORT).show();
            }
        }
    };
	
	protected void onActivityResult(int arg0, int arg1, Intent arg2) {
		switch (arg0) {
		case PUBLISH_COMMENT:
			commentCount = arg2.getIntExtra("comment_count", 0);
			commentCountTV.setText(commentCount+"");
			break;
		}
	};
}
