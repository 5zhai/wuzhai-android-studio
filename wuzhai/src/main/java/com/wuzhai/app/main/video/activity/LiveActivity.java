package com.wuzhai.app.main.video.activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.wuzhai.app.R;
import com.wuzhai.app.application.WuZhaiApplication;
import com.wuzhai.app.main.WuzhaiService;
import com.wuzhai.app.main.WuzhaiServiceCallbackAdapter;
import com.wuzhai.app.objects.User;
import com.wuzhai.app.tools.Utils;

public class LiveActivity extends Activity {
    private WuzhaiService service;
    private EditText titleET;
    private Button startBtn;
    private ImageView bgImg;
    private ImageView closeBtn;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_liveactivity);
        service = ((WuZhaiApplication) getApplication()).getService();
        service.setCallBack(callbackAdapter);
        titleET = (EditText) findViewById(R.id.title_Ed);
        bgImg = (ImageView) findViewById(R.id.bg_pic);
        startBtn = (Button) findViewById(R.id.start_live);
        closeBtn = (ImageView) findViewById(R.id.close_pic);

        Bitmap bitmap = Utils.getScaleCompressedBitmap(getResources(), R.drawable.setpassword_background, 200);
        bgImg.setImageBitmap(Utils.blur(LiveActivity.this, bitmap, 15));
        startBtn.setOnClickListener(clickListener);
        closeBtn.setOnClickListener(clickListener);
    }

    private OnClickListener clickListener = new OnClickListener() {

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.start_live:
                    String title = titleET.getText().toString().trim();
                    if (TextUtils.isEmpty(title)) {
                        Toast.makeText(LiveActivity.this, "标题不能为空", Toast.LENGTH_SHORT).show();
                    } else {
                        User.LiveRoom liveRoom = ((WuZhaiApplication) getApplication()).getUser().getLiveRoom();
                        if (TextUtils.isEmpty(liveRoom.getStreamJson())) {
                            service.createLiveRoom(title, "往后看", "因为UI设计中没有添加brief、desc和tags的UI,所以先写在程序中", "不想多说，看前一个参数描述");
                        } else {
                            String acessKey = ((WuZhaiApplication)getApplication()).getUser().getAccessKey();
                            int roomId = ((WuZhaiApplication)getApplication()).getUser().getLiveRoom().getId();
                            service.openLiveRoom(acessKey,roomId+"",title);
                        }
                    }
                    break;
                case R.id.close_pic:
                    onBackPressed();
                    break;
            }
        }
    };

    private WuzhaiServiceCallbackAdapter callbackAdapter = new WuzhaiServiceCallbackAdapter() {

        @Override
        public void onCompleted(int result) {
            if (result == WuzhaiService.CREATE_LIVE_ROOM_SUCC || result == WuzhaiService.OPEN_LIVE_ROOM_SUCC) {
                Log.d("yue.huang","创建打开房间成功");
                startLiving();
            }else if(result == WuzhaiService.CREATE_LIVE_ROOM_FAL || result == WuzhaiService.OPEN_LIVE_ROOM_FAL){
                Toast.makeText(LiveActivity.this, "打开房间失败", Toast.LENGTH_SHORT).show();
            }
        }
    };


    /**
     * 开启直播
     */
    private void startLiving(){
        String streamJson = ((WuZhaiApplication) getApplication()).getUser().getLiveRoom().getStreamJson();
        int liveRoomId = ((WuZhaiApplication) getApplication()).getUser().getLiveRoom().getId();
        if (TextUtils.isEmpty(streamJson)) {
            Toast.makeText(LiveActivity.this, "打开房间失败，streamJson为空", Toast.LENGTH_SHORT).show();
        } else {
            Intent intent = new Intent(LiveActivity.this, SWCameraStreamingActivity.class);
            intent.putExtra("stream_json_str", streamJson);
            intent.putExtra("roomId", liveRoomId + "");
            startActivity(intent);
        }
    }
}
