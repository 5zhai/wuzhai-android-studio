package com.wuzhai.app.main.widget;

import io.rong.imkit.RongIM;

import com.squareup.picasso.Picasso;
import com.wuzhai.app.R;
import com.wuzhai.app.application.WuZhaiApplication;
import com.wuzhai.app.objects.User;
import com.wuzhai.app.person.activity.FollowFansActivity;
import com.wuzhai.app.person.activity.GiftCenterActivity;
import com.wuzhai.app.person.activity.MyCollectionActivity;
import com.wuzhai.app.person.activity.MyOrderActivity;
import com.wuzhai.app.person.activity.MyWalletActivity;
import com.wuzhai.app.person.activity.PersonalCenterActivity;
import com.wuzhai.app.person.activity.SettingActivity;
import android.content.Context;
import android.content.Intent;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;

public class NavigationMenuView extends ListView implements View.OnClickListener,android.widget.AdapterView.OnItemClickListener{

	private ImageView userProfile;
	private TextView username;
	private TextView followerCount;
	private TextView fansCount;
	private LinearLayout menuSetting;
	private LinearLayout menuMessage;
	private Context context;
	private DrawerLayout drawerLayout;
	
	public NavigationMenuView(Context context) {
		super(context);
		this.context = context;
		init();
	}

	
	private void init(){
		setChoiceMode(CHOICE_MODE_SINGLE);
        setDivider(getResources().getDrawable(android.R.color.white));
        setDividerHeight(1);
//		setBackgroundColor(0xFFFF82AB);
        setBackgroundResource(R.drawable.bg_drawer);
		setupHeader();
		setupFooter();
		setupAdapter();
		setOnItemClickListener(this);
	}

	private void setupHeader(){
		setHeaderDividersEnabled(true);
		View headerView = LayoutInflater.from(getContext()).inflate(R.layout.navigation_menu_header, null);
		userProfile = (ImageView)headerView.findViewById(R.id.navigation_userProfile);
		userProfile.setOnClickListener(this);
		username = (TextView)headerView.findViewById(R.id.navigation_username);
		followerCount = (TextView)headerView.findViewById(R.id.navigation_follow_count);
		followerCount.setOnClickListener(this);
		fansCount = (TextView)headerView.findViewById(R.id.navigation_fans_count);
		fansCount.setOnClickListener(this);
		updateHeaderData(((WuZhaiApplication)context.getApplicationContext()).getUser());
		addHeaderView(headerView);
	}

	public void updateHeaderData(User user){
		Log.d("yue.huang", "updateHeaderData");
		Picasso.with(context).load(user.getAvatarUrl()).resize(150, 150).centerInside().into(userProfile);
		username.setText(user.getName());
		followerCount.setText("关注 "+user.getFollowersCount());
		fansCount.setText("粉丝 "+user.getFollowedsCount());
	}
	private void setupFooter(){
		setFooterDividersEnabled(false);
		View footerView = LayoutInflater.from(getContext()).inflate(R.layout.navigation_menu_footer, null);
		menuSetting = (LinearLayout)footerView.findViewById(R.id.navigation_menu_setting);
		menuSetting.setOnClickListener(this);
		menuMessage = (LinearLayout)footerView.findViewById(R.id.navigation_menu_message);
		menuMessage.setOnClickListener(this);
		addFooterView(footerView);
	}

	private void setupAdapter(){
		NavigationMenuAdapter adapter = new NavigationMenuAdapter(getContext());
		setAdapter(adapter);
	}
	@Override
	public void onClick(View arg0) {
		Intent intent;
		switch (arg0.getId()) {
		case R.id.navigation_userProfile:
			intent = new Intent(context, PersonalCenterActivity.class);
			User user = ((WuZhaiApplication)context.getApplicationContext()).getUser();
			intent.putExtra("userId", user.getId());
			context.startActivity(intent);
			break;
		case R.id.navigation_follow_count:
			intent = new Intent(context, FollowFansActivity.class);
			intent.putExtra("type", "follow");
			context.startActivity(intent);
			break;
		case R.id.navigation_fans_count:
			intent = new Intent(context, FollowFansActivity.class);
			intent.putExtra("type", "fans");
			context.startActivity(intent);
			break;
		case R.id.navigation_menu_setting:
			context.startActivity(new Intent(context, SettingActivity.class));
			break;
		case R.id.navigation_menu_message:
			RongIM.getInstance().startConversationList(context);
		default:
			break;
		}
		if(drawerLayout!=null){
			drawerLayout.closeDrawers();
		}
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		switch (arg2) {
		case 1:
			Intent intent = new Intent(context, PersonalCenterActivity.class);
			User user = ((WuZhaiApplication)context.getApplicationContext()).getUser();
			intent.putExtra("userId", user.getId());
			context.startActivity(intent);
			break;
		case 2:
			context.startActivity(new Intent(context, MyOrderActivity.class));
			break;
		case 3:
			context.startActivity(new Intent(context, MyWalletActivity.class));
			break;
		case 4:
			context.startActivity(new Intent(context, MyCollectionActivity.class));
			break;
		case 5:
			context.startActivity(new Intent(context, GiftCenterActivity.class));
			break;
		}
		if(drawerLayout!=null){
			drawerLayout.closeDrawers();
		}
	}

	public void setDrawerLayout(DrawerLayout drawerLayout){
		this.drawerLayout = drawerLayout;
	}
}
