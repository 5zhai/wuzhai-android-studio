package com.wuzhai.app.main.market.widget;

import java.util.ArrayList;
import com.squareup.picasso.Picasso;
import com.wuzhai.app.R;
import com.wuzhai.app.main.market.activity.PhotographerActivity;
import com.wuzhai.app.main.widget.HyRecyclerViewAdapter;
import com.wuzhai.app.tools.Utils;
import android.content.Context;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class AftercaseAdapter extends HyRecyclerViewAdapter implements OnClickListener{
	private Context context;
	private ArrayList<? extends Aftercases> aftercasesList;
	private boolean isShowLable = false;

	public AftercaseAdapter(Context context,ArrayList<Aftercases> aftercasesList){
		this.context = context;
		this.aftercasesList = aftercasesList;
		if(context instanceof PhotographerActivity){
			isShowLable = true;
		}
	}

	public AftercaseAdapter(Context context){
		this.context = context;
		if(context instanceof PhotographerActivity){
			isShowLable = true;
		}
	}

	@Override
	public int getItemCount() {
		// TODO Auto-generated method stub
		return aftercasesList.size();
	}

	@Override
	public void onBindViewHolder(ViewHolder arg0, int arg1) {
		Aftercases aftercases = aftercasesList.get(arg1);
		AftercasesViewHolder viewHolder = (AftercasesViewHolder)arg0;
		Picasso.with(context).load(aftercases.getPublisherAvatar()).resize(80, 80).centerInside().into(viewHolder.userAvatar);
		Picasso.with(context).load(aftercases.getMainImage()).resize(300, 200).centerInside().into(viewHolder.showPic);
		viewHolder.username.setText(aftercases.getPublisherName());
		viewHolder.userplace.setText(aftercases.getCity());
		viewHolder.service.setText(aftercases.getTitle());
		viewHolder.price.setText(aftercases.getPrice()+"");
		viewHolder.info.setText(aftercases.getBrief());
		viewHolder.setTags(aftercases.getTags());
		viewHolder.itemView.setTag(arg1);
	}

	@Override
	public ViewHolder onCreateViewHolder(ViewGroup arg0, int arg1) {
		View view = LayoutInflater.from(context).inflate(R.layout.market_aftercare_item, arg0,false);
		view.setOnClickListener(this);
		return new AftercasesViewHolder(view,isShowLable);
	}

	private class AftercasesViewHolder extends ViewHolder{

		ImageView userAvatar;
		TextView username;
		TextView userplace;
		ImageView showPic;
		TextView service;
		TextView price;
		TextView info;
		LinearLayout lable_layout;

		public AftercasesViewHolder(View arg0,boolean isShowLable) {
			super(arg0);
			userAvatar = (ImageView)arg0.findViewById(R.id.userAvatar);
			username = (TextView)arg0.findViewById(R.id.username);
			userplace = (TextView)arg0.findViewById(R.id.userplace);
			showPic = (ImageView)arg0.findViewById(R.id.showPic);
			service = (TextView)arg0.findViewById(R.id.service);
			price = (TextView)arg0.findViewById(R.id.price);
			info = (TextView)arg0.findViewById(R.id.info);
			lable_layout = (LinearLayout)arg0.findViewById(R.id.lable_layout);
			if(!isShowLable){
				lable_layout.setVisibility(View.GONE);
			}
		}

		public void setTags(String[] tags){
			//因为每次adapter.notifyDataSetChanged都会重新bind下数据，此方法都会执行，造成label重复;
			//此处先判断是否已将添加了label，添加过就不再添加
			if(lable_layout.getChildCount() != 0){
				return;
			}
			for(String tag: tags){
				TextView tagTextView = new TextView(context);
				tagTextView.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT));
				tagTextView.setPadding(Utils.dpToPx(4), Utils.dpToPx(4), Utils.dpToPx(4), Utils.dpToPx(4));
				tagTextView.setTextColor(0xffff7e9c);
				tagTextView.setTextSize(12);
				tagTextView.setBackgroundResource(R.drawable.photographer_lable_bg);
				tagTextView.setText(tag);
				lable_layout.addView(tagTextView);
			}
		}
	}

	@Override
	public void onClick(View v) {
		recyclerViewItemClickListener.onItemClick(v, (Integer)(v.getTag()));
	}

	public void setAftercaseList(ArrayList<? extends Aftercases> aftercasesList){
		this.aftercasesList = aftercasesList;
		notifyDataSetChanged();
	}
}
