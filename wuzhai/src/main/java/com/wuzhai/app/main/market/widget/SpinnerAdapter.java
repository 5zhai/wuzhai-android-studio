package com.wuzhai.app.main.market.widget;

import com.wuzhai.app.R;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

public class SpinnerAdapter extends ArrayAdapter<String> {
	private Context context;
	private Spinner spinner;

	public SpinnerAdapter(Context context, int resource, String[] objects) {
		super(context, resource, objects);
		this.context = context;
	}

	@Override
	public View getDropDownView(int position, View convertView, ViewGroup parent) {
		convertView = LayoutInflater.from(context).inflate(R.layout.spinner_item, parent, false);
		TextView title = (TextView) convertView.findViewById(R.id.spinner_title);
		title.setText(getItem(position));
		ImageView tick = (ImageView) convertView.findViewById(R.id.spinner_tick);
		convertView.setTag(tick);
		if (spinner.getSelectedItemPosition() == position) {
			tick.setVisibility(View.VISIBLE);
		}
		return convertView;
	}

	public void setSpinner(Spinner spinner) {
		this.spinner = spinner;
	}
}
