package com.wuzhai.app.main.video.widget;

import java.util.ArrayList;

import android.content.Context;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.wuzhai.app.R;
import com.wuzhai.app.main.widget.HyRecyclerViewAdapter;

public class VideoAdapter extends HyRecyclerViewAdapter implements OnClickListener{

	private Context context;
	private ArrayList<VideoEntity> videoList;
	private boolean isDance;
	public VideoAdapter(Context context,ArrayList<VideoEntity> videoList,boolean isDance){
		this.context = context;
		this.videoList = videoList;
		this.isDance = isDance;
	}
	@Override
	public int getItemCount() {
		// TODO Auto-generated method stub
		return videoList.size();
	}

	@Override
	public void onBindViewHolder(ViewHolder arg0, int arg1) {
		VideoEntity video = videoList.get(arg1);
		VideoViewHolder viewHolder = (VideoViewHolder)arg0;
		try {
			Picasso.with(context).load(video.getPicturePath()).resize(320, 240).centerInside().into(viewHolder.pic);
			Picasso.with(context).load(video.getPublisherAvatar()).resize(50, 50).into(viewHolder.publisherAvatar);
		} catch (Exception e) {
			e.printStackTrace();
		}
		viewHolder.title.setText(video.getTitle());
		viewHolder.publisherName.setText(video.getPublisherName());
		viewHolder.watcherCount.setText(video.getPlayCount()+"");
		viewHolder.likeCount.setText(video.getLikesCount()+"");
		viewHolder.messageCount.setText(video.getCommentsCount()+"");
		viewHolder.itemView.setTag(arg1);
	}

	@Override
	public ViewHolder onCreateViewHolder(ViewGroup arg0, int arg1) {
		View view = LayoutInflater.from(context).inflate(R.layout.video_item, arg0,false);
		view.setOnClickListener(this);
		return new VideoViewHolder(view,isDance);
	}

	private static class VideoViewHolder extends ViewHolder{
		ImageView pic;
		TextView title;
		ImageView publisherAvatar;
		TextView publisherName;
		TextView watcherCount;
		TextView likeCount;
		TextView messageCount;

		public VideoViewHolder(View view,boolean isDance) {
			super(view);
			pic = (ImageView) view.findViewById(R.id.video_pic);
			publisherAvatar = (ImageView) view.findViewById(R.id.publisher_avatar);
			title = (TextView) view.findViewById(R.id.video_title);
			publisherName = (TextView) view.findViewById(R.id.publisher_name);
			watcherCount = (TextView) view.findViewById(R.id.watcher_count);
			likeCount = (TextView) view.findViewById(R.id.like_count);
			messageCount = (TextView) view.findViewById(R.id.message_count);
			if(isDance){
				likeCount.setVisibility(View.VISIBLE);
				messageCount.setVisibility(View.VISIBLE);
			}else {
				watcherCount.setVisibility(View.VISIBLE);
			}
		}
	}

	@Override
	public void onClick(View v) {
		recyclerViewItemClickListener.onItemClick(v, (Integer)v.getTag());
	}
}
