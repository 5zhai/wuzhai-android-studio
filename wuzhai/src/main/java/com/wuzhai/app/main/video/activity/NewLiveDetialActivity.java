package com.wuzhai.app.main.video.activity;

import java.util.ArrayList;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.Activity;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.view.animation.AccelerateInterpolator;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.pili.pldroid.player.AVOptions;
import com.pili.pldroid.player.PLMediaPlayer;
import com.pili.pldroid.player.widget.PLVideoTextureView;
import com.wuzhai.app.R;
import com.wuzhai.app.application.WuZhaiApplication;
import com.wuzhai.app.main.WuzhaiService;
import com.wuzhai.app.main.WuzhaiServiceCallbackAdapter;
import com.wuzhai.app.main.video.widget.LiveGiftListAdapter;
import com.wuzhai.app.main.video.widget.MediaController;
import com.wuzhai.app.main.widget.ChatRoomFragment;
import com.wuzhai.app.main.widget.OnRecyclerViewItemClickListener;
import com.wuzhai.app.objects.Gift;
import com.wuzhai.app.tools.Utils;

public class NewLiveDetialActivity extends AppCompatActivity {
	private WuzhaiService service;
    private MediaController mMediaController;
    private PLVideoTextureView mVideoView;
    private Toast mToast = null;
    private String mVideoPath = null;
    private int roomId = -1;
    private int watchCount = 0;
    private int anchorId = -1;
    private int mRotation = 0;
    private int mDisplayAspectRatio = PLVideoTextureView.ASPECT_RATIO_FIT_PARENT; //default
    private ImageView closeBtn;
    private TextView watchCountTV;
    private RecyclerView giftListRV;
    private ArrayList<Gift> giftList = new ArrayList<>();
    private LiveGiftListAdapter giftAdapter;
    private LinearLayout giftListTagBtn;
    private LinearLayout menuLayout;
    private LinearLayout menuContentLayout;
    private ImageView menuCloseBtn;
    private ChatRoomFragment chatRoomFragment;
    private ImageView wordsMenu;
    private ImageView chatMenu;
    //用来保存用户通过点击按钮设置聊天面板的显示状态，用来用户点击礼物列表时保存用户操作
    private boolean isUserSetChatPannelShow = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setContentView(R.layout.activity_newlivedetial);
        roomId = getIntent().getIntExtra("room_id", -1);
        anchorId = getIntent().getIntExtra("anchor_id", -1);
        mVideoPath = getIntent().getStringExtra("videoPath");
//        mVideoPath = "rtmp://live.hkstv.hk.lxdns.com/live/hks";
        watchCount = getIntent().getIntExtra("watch_count", 0)+1;//加一算上自己
        service = (WuzhaiService)((WuZhaiApplication)getApplication()).getService();
        service.setCallBack(callback);
        initView();
        service.joinLiveRoom(roomId);
        service.getGift(2, 1);
    }

    @Override
    protected void onPause() {
        super.onPause();
        mToast = null;
        mVideoView.pause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mVideoView.start();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mVideoView.stopPlayback();
        service.leaveLiveRoom(roomId);
    }

	private void initView() {
		giftListTagBtn = (LinearLayout)findViewById(R.id.gift_list_tag);
		menuLayout = (LinearLayout)findViewById(R.id.menu_layout);
        menuContentLayout = (LinearLayout)findViewById(R.id.menu_content_layout);
		closeBtn = (ImageView) findViewById(R.id.close_pic);
		watchCountTV = (TextView) findViewById(R.id.watch_count);
		giftListRV = (RecyclerView)findViewById(R.id.gift_list);
        menuCloseBtn = (ImageView)findViewById(R.id.menu_close_pic);
        wordsMenu = (ImageView)findViewById(R.id.words_pic);
        chatMenu = (ImageView)findViewById(R.id.chat_pic);
        chatRoomFragment = (ChatRoomFragment) getSupportFragmentManager().findFragmentById(R.id.conversation);
        giftAdapter = new LiveGiftListAdapter(this, giftList);
		GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 5);

        chatRoomFragment.setRoomId(roomId+"");
		giftListTagBtn.setOnClickListener(clickListener);
		closeBtn.setOnClickListener(clickListener);
        wordsMenu.setOnClickListener(clickListener);
        chatMenu.setOnClickListener(clickListener);
        menuCloseBtn.setOnClickListener(clickListener);
		watchCountTV.setText(watchCount + "");
		giftListRV.setLayoutManager(gridLayoutManager);
        giftAdapter.setOnRecyclerViewItemClickListener(giftItemClickListener);
		giftListRV.setAdapter(giftAdapter);


		initVideoView();
	}

    private void initVideoView(){
        mVideoView = (PLVideoTextureView) findViewById(R.id.VideoView);

        View loadingView = findViewById(R.id.LoadingView);
        mVideoView.setBufferingIndicator(loadingView);

        // If you want to fix display orientation such as landscape, you can use the code show as follow
        //
        // if (this.getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
        //     mVideoView.setPreviewOrientation(0);
        // }
        // else if (this.getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
        //     mVideoView.setPreviewOrientation(270);
        // }

        AVOptions options = new AVOptions();

//        int isLiveStreaming = getIntent().getIntExtra("liveStreaming", 1);
        int isLiveStreaming = 1;
        // the unit of timeout is ms
        options.setInteger(AVOptions.KEY_PREPARE_TIMEOUT, 10 * 1000);
        options.setInteger(AVOptions.KEY_GET_AV_FRAME_TIMEOUT, 10 * 1000);
        // Some optimization with buffering mechanism when be set to 1
        options.setInteger(AVOptions.KEY_LIVE_STREAMING, isLiveStreaming);
        if (isLiveStreaming == 1) {
            options.setInteger(AVOptions.KEY_DELAY_OPTIMIZATION, 1);
        }

        // 1 -> hw codec enable, 0 -> disable [recommended]
        int codec = getIntent().getIntExtra("mediaCodec", 0);
        options.setInteger(AVOptions.KEY_MEDIACODEC, codec);

        // whether start play automatically after prepared, default value is 1
        options.setInteger(AVOptions.KEY_START_ON_PREPARED, 0);

        mVideoView.setAVOptions(options);

        // You can mirror the display
        // mVideoView.setMirror(true);

        // You can also use a custom `MediaController` widget
//        mMediaController = new MediaController(this, false, isLiveStreaming == 1);
//        mVideoView.setMediaController(mMediaController);

        mVideoView.setOnCompletionListener(mOnCompletionListener);
        mVideoView.setOnErrorListener(mOnErrorListener);

        mVideoView.setVideoPath(mVideoPath);
        mVideoView.start();
    }

    public void onClickRotate(View v) {
        mRotation = (mRotation + 90) % 360;
        mVideoView.setDisplayOrientation(mRotation);
    }

    public void onClickSwitchScreen(View v) {
        mDisplayAspectRatio = (mDisplayAspectRatio + 1) % 5;
        mVideoView.setDisplayAspectRatio(mDisplayAspectRatio);
        switch (mVideoView.getDisplayAspectRatio()) {
            case PLVideoTextureView.ASPECT_RATIO_ORIGIN:
                showToastTips("Origin mode");
                break;
            case PLVideoTextureView.ASPECT_RATIO_FIT_PARENT:
                showToastTips("Fit parent !");
                break;
            case PLVideoTextureView.ASPECT_RATIO_PAVED_PARENT:
                showToastTips("Paved parent !");
                break;
            case PLVideoTextureView.ASPECT_RATIO_16_9:
                showToastTips("16 : 9 !");
                break;
            case PLVideoTextureView.ASPECT_RATIO_4_3:
                showToastTips("4 : 3 !");
                break;
            default:
                break;
        }
    }

    private PLMediaPlayer.OnErrorListener mOnErrorListener = new PLMediaPlayer.OnErrorListener() {
        @Override
        public boolean onError(PLMediaPlayer mp, int errorCode) {
            switch (errorCode) {
                case PLMediaPlayer.ERROR_CODE_INVALID_URI:
                    showToastTips("Invalid URL !");
                    break;
                case PLMediaPlayer.ERROR_CODE_404_NOT_FOUND:
                    showToastTips("404 resource not found !");
                    break;
                case PLMediaPlayer.ERROR_CODE_CONNECTION_REFUSED:
                    showToastTips("Connection refused !");
                    break;
                case PLMediaPlayer.ERROR_CODE_CONNECTION_TIMEOUT:
                    showToastTips("Connection timeout !");
                    break;
                case PLMediaPlayer.ERROR_CODE_EMPTY_PLAYLIST:
                    showToastTips("Empty playlist !");
                    break;
                case PLMediaPlayer.ERROR_CODE_STREAM_DISCONNECTED:
                    showToastTips("Stream disconnected !");
                    break;
                case PLMediaPlayer.ERROR_CODE_IO_ERROR:
                    showToastTips("Network IO Error !");
                    break;
                case PLMediaPlayer.ERROR_CODE_UNAUTHORIZED:
                    showToastTips("Unauthorized Error !");
                    break;
                case PLMediaPlayer.ERROR_CODE_PREPARE_TIMEOUT:
                    showToastTips("Prepare timeout !");
                    break;
                case PLMediaPlayer.ERROR_CODE_READ_FRAME_TIMEOUT:
                    showToastTips("Read frame timeout !");
                    break;
                case PLMediaPlayer.MEDIA_ERROR_UNKNOWN:
                default:
                    showToastTips("unknown error !");
                    break;
            }
            // Todo pls handle the error status here, retry or call finish()
            finish();
            // If you want to retry, do like this:
            // mVideoView.setVideoPath(mVideoPath);
            // mVideoView.start();
            // Return true means the error has been handled
            // If return false, then `onCompletion` will be called
            return true;
        }
    };

    private PLMediaPlayer.OnCompletionListener mOnCompletionListener = new PLMediaPlayer.OnCompletionListener() {
        @Override
        public void onCompletion(PLMediaPlayer plMediaPlayer) {
            showToastTips("Play Completed !");
            finish();
        }
    };

    private void showToastTips(final String tips) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (mToast != null) {
                    mToast.cancel();
                }
                mToast = Toast.makeText(NewLiveDetialActivity.this, tips, Toast.LENGTH_SHORT);
                mToast.show();
            }
        });
    }

    private OnClickListener clickListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.close_pic:
				onBackPressed();
				break;

			case R.id.gift_list_tag:
				if(menuLayout.getVisibility() == View.VISIBLE){
					menuLayout.setVisibility(View.GONE);
					giftListRV.setVisibility(View.VISIBLE);
				}else {
                    giftListRV.setVisibility(View.GONE);
                    menuLayout.setVisibility(View.VISIBLE);
				}
                if(isUserSetChatPannelShow){
                    switchChatPanel();
                }
				break;
                case R.id.menu_close_pic:
                    switchMenuLayout(v);
                    break;
                case R.id.words_pic:
                    switchChatPanel();
                    isUserSetChatPannelShow = !isUserSetChatPannelShow;
                    break;
                case R.id.chat_pic:
                    switchChatInputET();
                    break;
			}
		}
	};

	private WuzhaiServiceCallbackAdapter callback = new WuzhaiServiceCallbackAdapter(){
		@Override
		public void onGetGiftListDone(ArrayList<Gift> gifts,int next_page) {
			if(gifts!=null){
				giftList.addAll(gifts);
				giftAdapter.notifyDataSetChanged();
				Log.d("yue.huang", "notifyDataSetChanged");
			}else {
				Toast.makeText(NewLiveDetialActivity.this, "获取数据失败", Toast.LENGTH_SHORT).show();
			}
		}

		public void onSendGiftDone(boolean result) {
			if(result){
				Toast.makeText(NewLiveDetialActivity.this, "礼物赠送成功", Toast.LENGTH_SHORT).show();
			}else {
				Toast.makeText(NewLiveDetialActivity.this, "礼物赠送失败", Toast.LENGTH_SHORT).show();
			}
		};
	};

	private OnRecyclerViewItemClickListener giftItemClickListener = new OnRecyclerViewItemClickListener() {
		@Override
		public void onItemClick(View view, int position) {
			final Gift gift = giftList.get(position);
			AlertDialog.Builder builder = new AlertDialog.Builder(NewLiveDetialActivity.this);
			builder.setTitle("礼物赠送");
			builder.setMessage("将此礼物送给主播");
			builder.setPositiveButton("确定", new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
//					Toast.makeText(NewLiveDetialActivity.this, "赠送"+gift.getName(), Toast.LENGTH_SHORT).show();
					service.sendGiftWithType(anchorId, gift.getId(), "live_room", roomId,1);
				}
			});
			builder.setNegativeButton("取消", new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					dialog.dismiss();
				}
			});
			builder.create().show();
		}
	};
    /**
     * 切换聊天页的显示状态
     */
    private void switchChatPanel(){
        if(chatRoomFragment.isHidden()){
            getSupportFragmentManager().beginTransaction().show(chatRoomFragment).commit();
        }else {
            getSupportFragmentManager().beginTransaction().hide(chatRoomFragment).commit();
        }
    }

    /**
     * 切换聊天输入框的显示状态
     */
    private void switchChatInputET(){
        if(chatRoomFragment.isEditLayoutShown()){
            chatRoomFragment.hideEditLayout();
        }else {
            chatRoomFragment.showEditLayout();
        }
    }
    /**
     * 切换右下角菜单布局的显示状态
     */
    private void switchMenuLayout(View switchBtn) {
        float targetRotation = switchBtn.getRotation();
        float targetY = 0;
        if (targetRotation == 0) {
            targetRotation = 360;
            targetY = menuContentLayout.getHeight();
        } else {
            targetRotation = 0;
            targetY = 0;
        }
        switchBtn.animate().rotation(targetRotation).setDuration(500).start();
        menuContentLayout.animate().translationY(targetY).setInterpolator(new AccelerateInterpolator(1))
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationStart(Animator animation) {
                        menuContentLayout.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onAnimationEnd(Animator animation) {
                        if (menuContentLayout.getTranslationY() != 0) {
                            menuContentLayout.setVisibility(View.GONE);
                        }

                    }
                }).setDuration(500).start();
    }
}
