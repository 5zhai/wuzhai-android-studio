package com.wuzhai.app.main.market.activity;

import java.util.ArrayList;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;
import android.widget.Toast;
import com.wuzhai.app.R;
import com.wuzhai.app.application.WuZhaiApplication;
import com.wuzhai.app.main.WuzhaiService;
import com.wuzhai.app.main.WuzhaiServiceCallbackAdapter;
import com.wuzhai.app.main.market.widget.ComicConAdapter;
import com.wuzhai.app.main.market.widget.ComicConNews;
import com.wuzhai.app.main.widget.LoadMoreRecyclerView;
import com.wuzhai.app.main.widget.OnRecyclerViewItemClickListener;
import com.wuzhai.app.main.widget.LoadMoreRecyclerView.LoadMoreCallBack;
import com.wuzhai.app.tools.Utils;
import com.wuzhai.app.widget.TitleToolbarActivity;

public class ComicConActivity extends TitleToolbarActivity implements OnRecyclerViewItemClickListener{

	private WuzhaiService service;
	private SwipeRefreshLayout pullRefreshLayout;
	private LoadMoreRecyclerView recyclerView;
    private int nextPage = 0;
    private ComicConAdapter adapter;
    private ArrayList<ComicConNews> newsList = new ArrayList<ComicConNews>();

	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_comic_con);
		setTitle("漫展信息");
		service = ((WuZhaiApplication)getApplication()).getService();
		service.setCallBack(callbackAdapter);
		initView();
	}

	private void initView(){
		pullRefreshLayout = (SwipeRefreshLayout)findViewById(R.id.pullRefreshLayout);
		pullRefreshLayout.setColorSchemeResources(android.R.color.holo_blue_light,
				android.R.color.holo_red_light, android.R.color.holo_orange_light,
				android.R.color.holo_green_light);
		pullRefreshLayout.setOnRefreshListener(refreshListener);
		recyclerView = (LoadMoreRecyclerView)findViewById(R.id.rvFeed);
		LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
		recyclerView.setLayoutManager(linearLayoutManager);
		recyclerView.setLoadMoreCallBack(loadMoreCallBack);
		adapter = new ComicConAdapter(this);
		adapter.setOnRecyclerViewItemClickListener(this);
		adapter.setComicConNewsList(newsList);
		recyclerView.setAdapter(adapter);
		service.getComicCon(1);
	}
	@Override
	protected void onResume() {
		service.setCallBack(callbackAdapter);
		super.onResume();
	}
	private WuzhaiServiceCallbackAdapter callbackAdapter = new WuzhaiServiceCallbackAdapter(){
		@Override
		public void onGetComicConNewsCompleted(
				ArrayList<ComicConNews> comicConNewsList,int next_page) {
			if(comicConNewsList!=null){
				nextPage = next_page;
				//如果正在下拉刷新则情况原来的数据
				if(pullRefreshLayout.isRefreshing()){
					newsList.clear();
				}
				newsList.addAll(comicConNewsList);
				//WrapRecyclerView对RecyclerView进行了继承，当有header或footerview时内部对adapter进行了包装替换，
				//所以要不能直接使用adapter的notifyDataSetChanged()
				recyclerView.getAdapter().notifyDataSetChanged();
				pullRefreshLayout.setRefreshing(false);
			}else {
				Toast.makeText(ComicConActivity.this, "获取数据失败", Toast.LENGTH_SHORT).show();
			}
		}
	};

	private OnRefreshListener refreshListener = new OnRefreshListener() {
		@Override
		public void onRefresh() {
			recyclerView.removeFooterView();
	        if(Utils.isNetworkAvailable(ComicConActivity.this)){
				service.getComicCon(1);
	        }else {
				Toast.makeText(ComicConActivity.this, "无网络连接", Toast.LENGTH_SHORT).show();
				pullRefreshLayout.setRefreshing(false);
			}
		}
	};

	private LoadMoreCallBack loadMoreCallBack = new LoadMoreCallBack() {
		@Override
		public void loadMore() {
			if (nextPage != 0) {
				if (Utils.isNetworkAvailable(ComicConActivity.this)) {
					recyclerView.setFooterViewLoaderMore();
					service.getComicCon(nextPage);
				} else {
					Toast.makeText(ComicConActivity.this, "无网络连接",
							Toast.LENGTH_SHORT).show();
				}
			} else {
				recyclerView.setFooterNoMoreToLoad();
				// Toast.makeText(ComicConActivity.this, "无更多数据",
				// Toast.LENGTH_SHORT).show();
			}
		}
	};

	@Override
	public void onItemClick(View view, int position) {
		Intent intent = new Intent(this, ComicConDetailActivity.class);
		intent.putExtra("comicConNews", newsList.get(position));
		startActivity(intent);
	}
}
