package com.wuzhai.app.main.video.activity;

import java.util.ArrayList;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;

import com.viewpagerindicator.TabPageIndicator;
import com.wuzhai.app.R;
import com.wuzhai.app.main.video.widget.AnchorFragment;
import com.wuzhai.app.main.video.widget.ChatFragment;
import com.wuzhai.app.main.widget.ViewPagerAdapter;
import com.wuzhai.app.widget.TitleToolbarActivity;

public class LiveDetialActivity extends TitleToolbarActivity {
	private ViewPager viewPager;
	private TabPageIndicator indicator;
	private ArrayList<Fragment> pageList = new ArrayList<Fragment>();
	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setTitle("九尾阿狸的一天");//以后会通过intent传递
		setBackText("视频");
		setContentView(R.layout.activity_live_detail);
		initView();
	}
	private void initView(){
		viewPager = (ViewPager)findViewById(R.id.live_detial_viewpager);
		indicator = (TabPageIndicator)findViewById(R.id.live_detial_indicator);
		initPageList();
		ViewPagerAdapter vpAdapter= new ViewPagerAdapter(getSupportFragmentManager(), pageList);
		vpAdapter.setPageTitle(new String[]{"主播","聊天"});
		viewPager.setAdapter(vpAdapter);
		indicator.setViewPager(viewPager);
	}
	private void initPageList(){
		AnchorFragment anchorFragment = new AnchorFragment();
		ChatFragment chatFragment = new ChatFragment();
		pageList.add(anchorFragment);
		pageList.add(chatFragment);
	}
}
