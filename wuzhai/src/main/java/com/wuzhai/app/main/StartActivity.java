package com.wuzhai.app.main;

import com.wuzhai.app.R;
import com.wuzhai.app.application.WuZhaiApplication;
import com.wuzhai.app.login.LoginActivity;
import com.wuzhai.app.tools.Utils;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.util.Log;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.Toast;
import org.json.JSONObject;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import javax.net.ssl.HttpsURLConnection;

public class StartActivity extends Activity{
	private Handler handler;
	private final int GO_LOGIN = 100;
    private final int GO_MAIN = 200;
    private final int GO_ERR = 300;
    /*6.0权限检测*/
    private ArrayList<String> permissionList = new ArrayList<>();
    private final int REQUEST_CODE_ASK_PERMISSION = 100;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Log.d("yue.huang","startActivity");
		initView();
		handler = new Handler(){
			@Override
			public void handleMessage(Message msg) {
				switch (msg.what){
					case GO_LOGIN:
						startActivity(new Intent(StartActivity.this,LoginActivity.class));
						break;
					case GO_MAIN:
                        startActivity(new Intent(StartActivity.this,MainActivity.class));
						break;
                    case GO_ERR:
                        Toast.makeText(StartActivity.this,"网络错误",Toast.LENGTH_SHORT).show();
                        break;
				}
                StartActivity.this.finish();
			}
		};
        competenceCheck();
	}

	private void initView(){
		ImageView imageView = new ImageView(this);
		imageView.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
		imageView.setScaleType(ScaleType.FIT_XY);
		imageView.setImageResource(R.drawable.start_page);
		setContentView(imageView);
	}

	//6.0以上系统，敏感权限检查
	private void competenceCheck(){
		if(Build.VERSION.SDK_INT < 23){
			preWorking();
			return;
		}
		if(!Utils.hasPermission(this,"android.permission.CAMERA")){
			permissionList.add("android.permission.CAMERA");
		}
		if(!Utils.hasPermission(this,"android.permission.WRITE_EXTERNAL_STORAGE")){
			permissionList.add("android.permission.WRITE_EXTERNAL_STORAGE");
		}
		if(!Utils.hasPermission(this,"android.permission.ACCESS_FINE_LOCATION")){
			permissionList.add("android.permission.ACCESS_FINE_LOCATION");
		}
		if(!Utils.hasPermission(this,"android.permission.ACCESS_COARSE_LOCATION")){
			permissionList.add("android.permission.ACCESS_COARSE_LOCATION");
		}

		if(permissionList.size() == 0){
			preWorking();
		}else {
			ActivityCompat.requestPermissions(this,permissionList.toArray(new String[permissionList.size()]),REQUEST_CODE_ASK_PERMISSION);
		}
	}

	@Override
	public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
		switch (requestCode){
			case REQUEST_CODE_ASK_PERMISSION:
				for(int n=0;n<grantResults.length;n++){
					if(grantResults[n] != PackageManager.PERMISSION_GRANTED){
						//只要有一个权限被拒绝则提醒用户大开权限并退出应用
						String tipString = getOPenPermissionTipString(permissionList.get(n));
						showTipDialog(tipString);
						return;
					}
				}
				preWorking();
		}
	}

	private void preWorking(){
        String accesskeyString = Utils.getAccessKeyString(this);
        if(TextUtils.isEmpty(accesskeyString)){
            //跳到登录
            handler.sendEmptyMessageDelayed(GO_LOGIN, 1500);
        }else {
            if (Utils.isNetworkAvailable(this)) {
                /*用户已经登录的情况下，获取用户信息，以供调到主页后使用*/
                getUserInfo(accesskeyString);
            } else {
                Toast.makeText(this,"网络不可用",Toast.LENGTH_SHORT).show();
            }
        }

	}

	private String getOPenPermissionTipString(String permissionString){
		String resultString = null;
		switch (permissionString){
			case "android.permission.CAMERA":
				resultString = "请到设置中打开相机权限";
				break;
			case "android.permission.WRITE_EXTERNAL_STORAGE":
				resultString = "请到设置中打开读写存储权限";
				break;
            case "android.permission.ACCESS_FINE_LOCATION":
            case "android.permission.ACCESS_COARSE_LOCATION":
                resultString = "请到设置中打开位置权限";
                break;
		}
		return resultString;
	}

	private void showTipDialog(String tipString){
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle("权限被拒绝");
		builder.setMessage(tipString);
		builder.setCancelable(false);
		builder.setPositiveButton("去设置", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				Intent intent = new Intent(Settings.ACTION_MANAGE_APPLICATIONS_SETTINGS);
				StartActivity.this.startActivityForResult(intent,0);
				StartActivity.this.finish();
			}
		});
		builder.setNegativeButton("取消", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				onBackPressed();
			}
		});
		builder.create().show();
	}

    private void getUserInfo(final String access_key){
        new Thread(new Runnable() {
            @Override
            public void run() {
                if(getUserInfoFromServer(access_key)){
                    //调到主页
                    handler.sendEmptyMessageDelayed(GO_MAIN, 1500);
                }else {
                    //网络错误
                    handler.sendEmptyMessageDelayed(GO_ERR, 1500);
                }
            }
        }).start();
    }


    private boolean getUserInfoFromServer(String access_key){
		String url = Address.USER_INFO+access_key;
        try {
            JSONObject resultJson = Utils.getHttpResponseJson(url,"get",null);
            return Utils.pauseUserInfo(((WuZhaiApplication) getApplication()).getUser(),resultJson);
        }catch (Exception e){
            e.printStackTrace();
        }
        return false;
    }
}
