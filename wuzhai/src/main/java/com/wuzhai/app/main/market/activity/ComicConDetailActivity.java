package com.wuzhai.app.main.market.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;
import com.squareup.picasso.Picasso;
import com.wuzhai.app.R;
import com.wuzhai.app.main.market.widget.ComicConNews;
import com.wuzhai.app.tools.Utils;
import com.wuzhai.app.widget.TitleToolbarActivity;

public class ComicConDetailActivity extends TitleToolbarActivity {

	private ImageView poster;
	private TextView title;
	private TextView time;
	private TextView place;
	private WebView detailInfo;
	private ComicConNews news;

	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_comic_con_detial);
		setTitle("详情");
		setTextMenuString("分享");
		setTextMenuClickListener(clickListener);
		initView();
	}

	private void initView(){
		poster = (ImageView)findViewById(R.id.poster);
		title = (TextView)findViewById(R.id.title);
		time = (TextView)findViewById(R.id.time);
		place = (TextView)findViewById(R.id.place);
		detailInfo = (WebView)findViewById(R.id.detail_info);
		news = (ComicConNews)getIntent().getSerializableExtra("comicConNews");
		Picasso.with(this).load(news.getPicturePath()).into(poster);
		title.setText(news.getTitle());
		time.setText(news.getTime());
		place.setText(news.getPlace());
		detailInfo.loadDataWithBaseURL(null,news.getDesc(), "text/html", "UTF-8",null);
	}

	private OnClickListener clickListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
//			 ShareSDK.initSDK(ComicConDetailActivity.this);
//			 OnekeyShare oks = new OnekeyShare();
//			 //关闭sso授权
//			 oks.disableSSOWhenAuthorize();
//			 // 分享时Notification的图标和文字  2.5.9以后的版本不调用此方法
//			 //oks.setNotification(R.drawable.ic_launcher, getString(R.string.app_name));
//			 // title标题，印象笔记、邮箱、信息、微信、人人网和QQ空间使用
//			 oks.setTitle(news.getTitle());
//			 // titleUrl是标题的网络链接，仅在人人网和QQ空间使用
//			 oks.setTitleUrl("https://www.5yuzhai.com/");
//			 // text是分享文本，所有平台都需要这个字段
//			 oks.setText("");
//			 // imagePath是图片的本地路径，Linked-In以外的平台都支持此参数
//			 //oks.setImagePath("/sdcard/test.jpg");//确保SDcard下面存在此张图片
//			 // url仅在微信（包括好友和朋友圈）中使用
//			 oks.setUrl("https://www.5yuzhai.com/");
//			 // comment是我对这条分享的评论，仅在人人网和QQ空间使用
//			 oks.setComment("我是测试评论文本");
//			 // site是分享此内容的网站名称，仅在QQ空间使用
//			 oks.setSite(getString(R.string.app_name));
//			 // siteUrl是分享此内容的网站地址，仅在QQ空间使用
//			 oks.setSiteUrl("https://www.5yuzhai.com/");

//			 // 启动分享GUI
//			 oks.show(ComicConDetailActivity.this);
			Utils.onKeyShareOperation(ComicConDetailActivity.this,news.getTitle(),"https://www.5yuzhai.com/",
					news.getPicturePath(),"","https://www.5yuzhai.com/","我是测试评论文本",getString(R.string.app_name),"https://www.5yuzhai.com/");
		}
	};
}
