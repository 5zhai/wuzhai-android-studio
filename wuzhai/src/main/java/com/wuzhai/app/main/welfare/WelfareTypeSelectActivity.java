package com.wuzhai.app.main.welfare;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.wuzhai.app.R;
import com.wuzhai.app.application.WuZhaiApplication;
import com.wuzhai.app.main.WuzhaiService;
import com.wuzhai.app.main.WuzhaiServiceCallbackAdapter;
import com.wuzhai.app.main.market.widget.SpinnerAdapter;
import com.wuzhai.app.objects.Tag;
import com.wuzhai.app.tools.Utils;
import com.wuzhai.app.widget.FlowLayout;

import java.util.ArrayList;
import java.util.HashMap;


/**
 * Created by huangyue on 2016/12/6.
 */

public class WelfareTypeSelectActivity extends AppCompatActivity {
    private WuzhaiService service;
    private Spinner spinner;
    private FlowLayout typeContainer;
    /*被选中种类id列表*/
    private ArrayList<Integer> picTypeList;
    /*被选中种类name列表*/
    private ArrayList<String> picNameList;
    private HashMap<Integer, String> photoTypeMap;
    private FlowLayout tagContainer;
    private Button cancleBtn;
    private Button okBtn;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welfaretypeselecte);

        service = ((WuZhaiApplication)getApplication()).getService();
        service.setCallBack(callbackAdapter);

        spinner = (Spinner)findViewById(R.id.spinner);
        typeContainer = (FlowLayout)findViewById(R.id.type_container_layout);
        tagContainer = (FlowLayout)findViewById(R.id.tag_container_layout);
        cancleBtn = (Button)findViewById(R.id.cancle_btn);
        okBtn = (Button)findViewById(R.id.ok_btn);
        cancleBtn.setOnClickListener(clickListener);
        okBtn.setOnClickListener(clickListener);
        initData();
    }

    private void initData(){
        picTypeList = new ArrayList<>();
        picNameList = new ArrayList<>();
        updateSpinnerToolBar(new String[]{"绅士同盟","淑女大道"});
        service.getType(WuzhaiService.PHOTO_TYPE);
        service.getTags(WuzhaiService.TAG_PHOTO);
    }
    private void updateSpinnerToolBar(final String[] spinnerItem){
        spinner.setVisibility(View.VISIBLE);
        if(spinnerItem!=null && spinner.getChildCount()==0){
            SpinnerAdapter spinnerAdapter = new SpinnerAdapter(this, R.layout.spinner_checked_text,spinnerItem);
            spinnerAdapter.setSpinner(spinner);
            spinner.setAdapter(spinnerAdapter);
            spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    Toast.makeText(WelfareTypeSelectActivity.this,spinnerItem[position],Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        }
    }

    private WuzhaiServiceCallbackAdapter callbackAdapter = new WuzhaiServiceCallbackAdapter(){

        @Override
        public void onGetTypeCompleted(HashMap<Integer, String> typeMap) {
            if(typeMap!=null){
                photoTypeMap = typeMap;
                addPicTypeToContainer(typeMap.values().toArray(new String[typeMap.size()]));
            }
        }

        @Override
        public void onGetTagsCompleted(ArrayList<Tag> tagsList) {
            if(tagsList!=null){
                addPicTagToContainer(tagsList);
            }
        }
    };

    private void addPicTypeToContainer(String[] types){
        LinearLayout.LayoutParams paramsLayoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,Utils.dpToPx(26));
        paramsLayoutParams.leftMargin = Utils.dpToPx(5);
        paramsLayoutParams.rightMargin = Utils.dpToPx(5);
        paramsLayoutParams.bottomMargin = Utils.dpToPx(5);
        for(int n=0;n<types.length;n++){
            final TextView paramTV = new TextView(this);
            paramTV.setLayoutParams(paramsLayoutParams);
            paramTV.setPadding(Utils.dpToPx(6),0,Utils.dpToPx(6),0);
            paramTV.setText(types[n]);
            paramTV.setTextColor(0xffff7e9c);
            paramTV.setBackgroundResource(R.drawable.photographer_lable_bg);
            paramTV.setTextSize(TypedValue.COMPLEX_UNIT_SP,14);
            paramTV.setGravity(Gravity.CENTER);
            paramTV.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(paramTV.getTag() == null){
                        picTypeList.add(Utils.getIntTypeFromString(photoTypeMap, ((TextView)v).getText().toString()));
                        picNameList.add(((TextView)v).getText().toString());
                        v.setBackgroundResource(R.drawable.photographer_lable_checked_bg);
                        ((TextView)v).setTextColor(0xffffffff);
                        paramTV.setTag(true);
                    }else {
                        picTypeList.remove((Integer) Utils.getIntTypeFromString(photoTypeMap, ((TextView)v).getText().toString()));
                        picNameList.remove(((TextView)v).getText().toString());
                        v.setBackgroundResource(R.drawable.photographer_lable_bg);
                        ((TextView)v).setTextColor(0xffff7e9c);
                        paramTV.setTag(null);
                    }
                }
            });
            typeContainer.addView(paramTV);
        }
    }


    private void addPicTagToContainer(ArrayList<Tag> tagsList){
        LinearLayout.LayoutParams paramsLayoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,Utils.dpToPx(26));
        paramsLayoutParams.leftMargin = Utils.dpToPx(5);
        paramsLayoutParams.rightMargin = Utils.dpToPx(5);
        paramsLayoutParams.bottomMargin = Utils.dpToPx(5);
        for(int n=0;n<tagsList.size();n++){
            final TextView paramTV = new TextView(this);
            paramTV.setLayoutParams(paramsLayoutParams);
            paramTV.setPadding(Utils.dpToPx(6),0,Utils.dpToPx(6),0);
            paramTV.setText(tagsList.get(n).getDesc());
            paramTV.setTextColor(0xffff7e9c);
            paramTV.setBackgroundResource(R.drawable.photographer_lable_bg);
            paramTV.setTextSize(TypedValue.COMPLEX_UNIT_SP,14);
            paramTV.setGravity(Gravity.CENTER);
            paramTV.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(paramTV.getTag() == null){
                        v.setBackgroundResource(R.drawable.photographer_lable_checked_bg);
                        ((TextView)v).setTextColor(0xffffffff);
                        paramTV.setTag(true);
                    }else {
                        v.setBackgroundResource(R.drawable.photographer_lable_bg);
                        ((TextView)v).setTextColor(0xffff7e9c);
                        paramTV.setTag(null);
                    }
                }
            });
            tagContainer.addView(paramTV);
        }
    }

    private View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.cancle_btn:
                    onBackPressed();
                    break;
                case R.id.ok_btn:
                    Intent intent = new Intent();
                    intent.putIntegerArrayListExtra("types",picTypeList);
                    intent.putStringArrayListExtra("names",picNameList);
                    setResult(RESULT_OK,intent);
                    onBackPressed();
                    break;
            }
        }
    };

//    @Override
//    public void onBackPressed() {
//        super.onBackPressed();
//    }
}
