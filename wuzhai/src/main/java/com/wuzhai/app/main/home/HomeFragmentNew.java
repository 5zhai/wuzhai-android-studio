package com.wuzhai.app.main.home;

import java.util.ArrayList;
import com.squareup.picasso.Picasso;
import com.viewpagerindicator.CirclePageIndicator;
import com.wuzhai.app.R;
import com.wuzhai.app.application.WuZhaiApplication;
import com.wuzhai.app.main.WuzhaiService;
import com.wuzhai.app.main.WuzhaiServiceCallbackAdapter;
import com.wuzhai.app.main.home.widget.News;
import com.wuzhai.app.main.market.activity.ComicConActivity;
import com.wuzhai.app.main.market.activity.ComicConDetailActivity;
import com.wuzhai.app.main.market.widget.ComicConNews;
import com.wuzhai.app.main.video.activity.DanceDetailActivity;
import com.wuzhai.app.main.video.activity.DanceListActivity;
import com.wuzhai.app.main.video.widget.VideoEntity;
import com.wuzhai.app.objects.Carousel;
import com.wuzhai.app.tools.Utils;
import com.wuzhai.app.widget.AutoScrollViewPager;
import com.wuzhai.app.widget.CircleImageView;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class HomeFragmentNew extends Fragment {
	private WuzhaiService service;
	private CircleImageView head_subtitleIcon1;
	private CircleImageView head_subtitleIcon2;
	private ImageView videoOnePic;
	private TextView videoOneTitle;
	private ImageView videoTwoPic;
	private TextView videoTwoTitle;
	private ImageView videoThreePic;
	private TextView videoThreeTitle;
	private ImageView videoFourPic;
	private TextView videoFourTitle;
	private View comicConOne;
	private View comicConTwo;
	private View comicConThree;
	private TextView activityMore;
	private TextView videoMore;
	private AutoScrollViewPager viewPager;
	private CirclePageIndicator indicator;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_home, null);
		initView(view);
		return view;
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		service = ((WuZhaiApplication)getActivity().getApplication()).getService();
		service.setCallBack(callbackAdapter);
        if(Utils.isNetworkAvailable(getActivity())){
        	service.getHomeRecommendations(2);
            service.getLatestComicCon();
            service.getHotVideos();
            service.getCarousel(WuzhaiService.CAROUSEL_TYPE_HOME,3);
        }else {
			Toast.makeText(getActivity(), "无网络连接", Toast.LENGTH_SHORT).show();
		}
	}

	private void initView(View view){
		head_subtitleIcon1 = (CircleImageView)view.findViewById(R.id.red_head_subtitleIcon1);
		head_subtitleIcon2 = (CircleImageView)view.findViewById(R.id.red_head_subtitleIcon2);
		videoOnePic = (ImageView)view.findViewById(R.id.video_one_pic);
		videoOneTitle = (TextView)view.findViewById(R.id.video_one_title);
		videoTwoPic = (ImageView)view.findViewById(R.id.video_tow_pic);
		videoTwoTitle = (TextView)view.findViewById(R.id.video_tow_title);
		videoThreePic = (ImageView)view.findViewById(R.id.video_three_pic);
		videoThreeTitle = (TextView)view.findViewById(R.id.video_three_title);
		videoFourPic = (ImageView)view.findViewById(R.id.video_four_pic);
		videoFourTitle = (TextView)view.findViewById(R.id.video_four_title);
		comicConOne = view.findViewById(R.id.comiccon_one);
		comicConTwo = view.findViewById(R.id.comiccon_two);
		comicConThree = view.findViewById(R.id.comiccon_three);
		activityMore = (TextView)view.findViewById(R.id.activity_more);
		videoMore = (TextView)view.findViewById(R.id.video_more);
		viewPager = (AutoScrollViewPager)view.findViewById(R.id.viewpager);
		indicator = (CirclePageIndicator)view.findViewById(R.id.indicator);

		head_subtitleIcon1.setCornerRadius(Utils.dpToPx(6));
		head_subtitleIcon2.setCornerRadius(Utils.dpToPx(6));
		viewPager.getLayoutParams().height = (int)(Utils.getScreenSize(getContext())[0]/2.0);
		viewPager.startAutoScroll(1000);
		viewPager.setItemClickListener(carouselItemClickListener);
		videoMore.setOnClickListener(clickListener);
		activityMore.setOnClickListener(clickListener);
	}

	private WuzhaiServiceCallbackAdapter callbackAdapter = new WuzhaiServiceCallbackAdapter(){
		@Override
		public void onGetLatestComicConNewsCompleted(
				ArrayList<ComicConNews> comicConNewsList) {
			if(comicConNewsList.size()==0){return;}
			initComicConInfo(comicConNewsList.get(0),comicConOne);
			initComicConInfo(comicConNewsList.get(1),comicConTwo);
			initComicConInfo(comicConNewsList.get(2),comicConThree);
		}

		@Override
		public void onGetHotVideosDone(ArrayList<VideoEntity> videoList) {
			initHotVideo(videoList);
		}

		@Override
		public void onGetHomeRecommendationsDone(ArrayList<News> recommendationsList) {
			if(recommendationsList!=null && recommendationsList.size()!=0){
				Picasso.with(getContext()).load(recommendationsList.get(0).getIconURL()).into(head_subtitleIcon1);
				Picasso.with(getContext()).load(recommendationsList.get(1).getIconURL()).into(head_subtitleIcon2);
			}
		}

		@Override
		public void onGetCarouselListDone(ArrayList<Carousel> carouselList) {
			if(carouselList!=null){
				ArrayList<View> pages = new ArrayList<View>();
				for(Carousel carousel : carouselList){
					pages.add(createViewPagerPage(getActivity(), carousel.getTitle(), carousel.getImageUrl()));
				}
				viewPager.addPageList(pages);
		        indicator.setViewPager(viewPager);
		        indicator.setRadius(Utils.dpToPx(3));
			}
		}
	};

	private View createViewPagerPage(Context context,String title,String picUrl){
		View view = LayoutInflater.from(context).inflate(R.layout.layout_homecarouse_layout, null);
		ImageView imageView = (ImageView)view.findViewById(R.id.pic);
		TextView textView = (TextView)view.findViewById(R.id.title);
		Picasso.with(context).load(picUrl).resize(800, 400).centerInside().into(imageView);
		textView.setText(title);
		return view;
	}

	private void initHotVideo(ArrayList<VideoEntity> videoList){
		Log.d("yue.huang", "videoList:"+videoList);
		if(videoList==null){
			return;
		}
		VideoClickListener clickListener = new VideoClickListener(videoList);
		Picasso.with(getContext()).load(videoList.get(0).getPicturePath()).into(videoOnePic);
		videoOneTitle.setText(videoList.get(0).getTitle());
		videoOnePic.setOnClickListener(clickListener);
		Picasso.with(getContext()).load(videoList.get(1).getPicturePath()).into(videoTwoPic);
		videoTwoTitle.setText(videoList.get(1).getTitle());
		videoTwoPic.setOnClickListener(clickListener);
		Picasso.with(getContext()).load(videoList.get(2).getPicturePath()).into(videoThreePic);
		videoThreeTitle.setText(videoList.get(2).getTitle());
		videoThreePic.setOnClickListener(clickListener);
		Picasso.with(getContext()).load(videoList.get(3).getPicturePath()).into(videoFourPic);
		videoFourTitle.setText(videoList.get(3).getTitle());
		videoFourPic.setOnClickListener(clickListener);
	}

	private class VideoClickListener implements OnClickListener {
		private ArrayList<VideoEntity> videoList;
		private VideoEntity videoEntity;

		public VideoClickListener(ArrayList<VideoEntity> videoList) {
			this.videoList = videoList;
		}

		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.video_one_pic:
				videoEntity = videoList.get(0);
				break;
			case R.id.video_tow_pic:
				videoEntity = videoList.get(1);
				break;
			case R.id.video_three_pic:
				videoEntity = videoList.get(2);
				break;
			case R.id.video_four_pic:
				videoEntity = videoList.get(3);
				break;
			}
			Intent intent = new Intent(getContext(),DanceDetailActivity.class);
			intent.putExtra("id", videoEntity.getId());
			intent.putExtra("user_id",videoEntity.getPublisherId());
			startActivity(intent);
		}
	}

	private OnClickListener clickListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.activity_more:
				startActivity(new Intent(getContext(), ComicConActivity.class));
				break;

			case R.id.video_more:
				startActivity(new Intent(getContext(), DanceListActivity.class));
				break;
			}
		}
	};
	private void initComicConInfo(final ComicConNews comicConNews,View comicConView){
		ImageView imageView = (ImageView)comicConView.findViewById(R.id.red_icon);
		TextView titleTv = (TextView)comicConView.findViewById(R.id.red_title);
		TextView subTitleTv = (TextView)comicConView.findViewById(R.id.red_subtitle);
		TextView category = (TextView)comicConView.findViewById(R.id.category);
		Picasso.with(getContext()).load(comicConNews.getPicturePath()).into(imageView);
		titleTv.setText(comicConNews.getTitle());
		subTitleTv.setText(comicConNews.getSummary());
		category.setText(comicConNews.getCategory());
		comicConView.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(getContext(), ComicConDetailActivity.class);
				intent.putExtra("comicConNews", comicConNews);
				startActivity(intent);
			}
		});
	}

    private AutoScrollViewPager.CarouselItemClickListener carouselItemClickListener = new AutoScrollViewPager.CarouselItemClickListener() {
        @Override
		public void onItemClick(View v, int position) {
			Log.d("yue.huang", "click position:" + position);
		}
    };
}
