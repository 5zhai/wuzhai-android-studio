package com.wuzhai.app.main.market;

import java.util.ArrayList;
import com.wuzhai.app.R;
import com.wuzhai.app.application.WuZhaiApplication;
import com.wuzhai.app.main.WuzhaiService;
import com.wuzhai.app.main.WuzhaiServiceCallbackAdapter;
import com.wuzhai.app.main.market.activity.AftercareActivity;
import com.wuzhai.app.main.market.activity.ComicConActivity;
import com.wuzhai.app.main.market.activity.GoodsDetialActivity;
import com.wuzhai.app.main.market.activity.PhotographerActivity;
import com.wuzhai.app.main.market.activity.TradingMarketActivity;
import com.wuzhai.app.main.market.widget.Goods;
import com.wuzhai.app.main.market.widget.GoodsAdapter;
import com.wuzhai.app.main.widget.OnRecyclerViewItemClickListener;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

public class MarketFragment extends Fragment implements OnClickListener,OnRecyclerViewItemClickListener{
	private WuzhaiService service;
	private RecyclerView recyclerView;
	private LinearLayout tradingMarket;
	private LinearLayout photographer;
	private LinearLayout aftercare;
	private LinearLayout comic_con;
	private int nextPage = 0;
	private ArrayList<Goods> goodsList = new ArrayList<Goods>();
	private GoodsAdapter adapter;
//	private RefreshableView pullRefreshLayout;

	@Override
	@Nullable
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_market_main, null);
		return view;
	}
	@Override
	public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		service = ((WuZhaiApplication)getActivity().getApplication()).getService();
		service.setCallBack(callbackAdapter);
		tradingMarket = (LinearLayout)view.findViewById(R.id.tradingmarket);
		photographer = (LinearLayout)view.findViewById(R.id.photographer);
		aftercare = (LinearLayout)view.findViewById(R.id.aftercare);
		comic_con = (LinearLayout)view.findViewById(R.id.comic_con);
		recyclerView = (RecyclerView)view.findViewById(R.id.rvFeed_marketMain);
		GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(), 2);
		recyclerView.setLayoutManager(gridLayoutManager);
		tradingMarket.setOnClickListener(this);
		photographer.setOnClickListener(this);
		aftercare.setOnClickListener(this);
		comic_con.setOnClickListener(this);
		adapter = new GoodsAdapter(getActivity());
		adapter.setOnRecyclerViewItemClickListener(this);
		service.getGoods(null, null, null, 1,null);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.tradingmarket:
			startActivity(new Intent(getActivity(),TradingMarketActivity.class));
			break;
		case R.id.photographer:
			startActivity(new Intent(getActivity(),PhotographerActivity.class));
			break;
		case R.id.aftercare:
			startActivity(new Intent(getActivity(),AftercareActivity.class));
			break;
		case R.id.comic_con:
			startActivity(new Intent(getActivity(),ComicConActivity.class));
			break;
		}
	}

	WuzhaiServiceCallbackAdapter callbackAdapter = new WuzhaiServiceCallbackAdapter(){
		@Override
		public void onGetGoodsCompleted(ArrayList<Goods> goods_list, int next_page) {
			if(goods_list!=null){
				nextPage = next_page;
//				if(pullRefreshLayout.isRefreshing()){
//					goodsList.clear();
//				}
				goodsList.addAll(goods_list);
				adapter.setGoodsList(goodsList);
				recyclerView.setAdapter(adapter);
//				if(!pullRefreshLayout.isRefreshing()){
//					recyclerView.scrollToPosition(goodsList.size()-goods_list.size());
//				}
//				pullRefreshLayout.finishRefreshing();
//				if(next_page==0){
//				  recyclerView.setFooterNoMoreToLoad();
//				}else {
//					recyclerView.setFooterViewLoaderMore();
//				}
			}else {
				Toast.makeText(getActivity(), "获取数据失败", Toast.LENGTH_SHORT).show();
			}
		}
	};

	@Override
	public void onItemClick(View view, int position) {
		Intent intent = new Intent(getActivity(), GoodsDetialActivity.class);
		intent.putExtra("id", goodsList.get(position).getId());
		startActivity(intent);
	}
}
