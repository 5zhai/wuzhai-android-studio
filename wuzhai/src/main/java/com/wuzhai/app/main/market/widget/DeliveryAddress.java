package com.wuzhai.app.main.market.widget;

import java.io.Serializable;

public class DeliveryAddress implements Serializable{
	private String id;
	private String userId;
	private String receiverName;
	private String area;
	private String street;
	private String detial;
	private String tel;
	private String zipCode;

	public DeliveryAddress(String id, String userId, String receiverName,
			String area, String street, String detial,String tel, String zipCode) {
		this.id = id;
		this.userId = userId;
		this.receiverName = receiverName;
		this.area = area;
		this.street = street;
		this.detial = detial;
		this.tel = tel;
		this.zipCode = zipCode;
	}

	public String getId() {
		return id;
	}

	public String getUserId() {
		return userId;
	}

	public String getReceiverName() {
		return receiverName;
	}

	public String getArea() {
		return area;
	}

	public String getStreet() {
		return street;
	}

	public String getDetial(){
		return detial;
	}

	public String getTel() {
		return tel;
	}

	public String getZipCode() {
		return zipCode;
	}
}
