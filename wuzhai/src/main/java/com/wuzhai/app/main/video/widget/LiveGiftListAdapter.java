package com.wuzhai.app.main.video.widget;

import java.util.ArrayList;

import com.squareup.picasso.Picasso;
import com.wuzhai.app.R;
import com.wuzhai.app.main.widget.HyRecyclerViewAdapter;
import com.wuzhai.app.objects.Gift;

import android.content.Context;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

public class LiveGiftListAdapter extends HyRecyclerViewAdapter {
	private ArrayList<Gift> giftList;
	private Context context;

	public LiveGiftListAdapter(Context context,ArrayList<Gift> giftList){
		this.context = context;
		this.giftList = giftList;
	}

	@Override
	public int getItemCount() {
		// TODO Auto-generated method stub
		return giftList.size();
	}

	@Override
	public void onBindViewHolder(ViewHolder arg0, int arg1) {
		Gift gift = giftList.get(arg1);
		GiftViewHolder holder = (GiftViewHolder)arg0;
		Picasso.with(context).load(gift.getImagePath()).resize(100, 100).centerInside().into(holder.giftIconIV);
		holder.giftNameTV.setText(gift.getName());
		holder.giftCountTV.setText(gift.getAmount()+"");
		holder.itemView.setTag(arg1);
	}

	@Override
	public ViewHolder onCreateViewHolder(ViewGroup arg0, int arg1) {
		View view = LayoutInflater.from(context).inflate(R.layout.layout_livegiftlistitem, arg0,false);
		view.setOnClickListener(clickListener);
		return new GiftViewHolder(view);
	}

	private class GiftViewHolder extends android.support.v7.widget.RecyclerView.ViewHolder{

		public ImageView giftIconIV;
		public TextView giftNameTV;
		public TextView giftCountTV;

		public GiftViewHolder(View arg0) {
			super(arg0);
			giftIconIV = (ImageView)arg0.findViewById(R.id.gift_icon);
			giftNameTV = (TextView)arg0.findViewById(R.id.gift_name);
			giftCountTV = (TextView)arg0.findViewById(R.id.gift_count);
		}
		
	}

	private OnClickListener clickListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			if(recyclerViewItemClickListener != null){
				recyclerViewItemClickListener.onItemClick(v,(Integer)v.getTag());
			}
		}
	};
}
