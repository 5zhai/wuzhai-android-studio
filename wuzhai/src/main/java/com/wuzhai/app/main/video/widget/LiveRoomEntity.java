package com.wuzhai.app.main.video.widget;

public class LiveRoomEntity {

	private int userId;
	private String userName;
	private String avatar;
	private String snapshot;
	private String rtmpLiveUrls;
	private String hlsLiveUrls;
	private String httpFlvLiveUrls;
	private int id;
	private String key;
	private String title;
	private String brief;
	private String desc;
	private String state;
	private int onlineCount;
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getAvatar() {
		return avatar;
	}
	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}
	public String getSnapshot() {
		return snapshot;
	}
	public void setSnapshot(String snapshot) {
		this.snapshot = snapshot;
	}
	public String getRtmpLiveUrls() {
		return rtmpLiveUrls;
	}
	public void setRtmpLiveUrls(String rtmpLiveUrls) {
		this.rtmpLiveUrls = rtmpLiveUrls;
	}
	public String getHlsLiveUrls() {
		return hlsLiveUrls;
	}
	public void setHlsLiveUrls(String hlsLiveUrls) {
		this.hlsLiveUrls = hlsLiveUrls;
	}
	public String getHttpFlvLiveUrls() {
		return httpFlvLiveUrls;
	}
	public void setHttpFlvLiveUrls(String httpFlvLiveUrls) {
		this.httpFlvLiveUrls = httpFlvLiveUrls;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getBrief() {
		return brief;
	}
	public void setBrief(String brief) {
		this.brief = brief;
	}
	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public int getOnlineCount() {
		return onlineCount;
	}
	public void setOnlineCount(int onlineCount) {
		this.onlineCount = onlineCount;
	}

	
}
