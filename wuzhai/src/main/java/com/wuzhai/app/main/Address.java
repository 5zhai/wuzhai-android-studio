package com.wuzhai.app.main;

import static org.xmlpull.v1.XmlPullParser.COMMENT;

/**
 * Created by huangyue on 2017/3/7.
 */

public class Address {
    public static final String BASE_URL = "https://www.5yuzhai.com:443/api/v1";
    /*七牛图片上传地址*/
    public static final String IMG_QN = "http://ob23p88ai.bkt.clouddn.com/";
    /*七牛视频上传地址*/
    public static final String MV_QN = "http://ob23p0kvz.bkt.clouddn.com/";
    /*获取七牛上传token地址*/
    public static final String TOKEN_QN = BASE_URL+"/config/qiniu?";
    /*将图片在七牛地址上传到服务器的地址*/
    public static final String IMG_SERVER = BASE_URL+"/photos?access_key=";
    /*图片删除接口地址*/
    public static final String IMG_DELETE = BASE_URL+"/photos/delete";
    /*视频删除接口地址*/
    public static final String MV_DELETE = BASE_URL+"/videos/delete";
    /*获取收货地址接口*/
    public static final String SHIP_ADDRESS = BASE_URL + "/contacts?access_key=";
    /*获取默认收货地址接口*/
    public static final String SHIP_DEF_ADDRESS = BASE_URL + "/contacts/default_contact?access_key=";
    /*获取漫展信息列表*/
    public static final String ACTIVITY_LIST = BASE_URL + "/activities?";
    /*获取最新的漫展活动*/
    public static final String NEWEST_ACTIVITY = BASE_URL + "/activities/latest?count=3";
    /*获取用户基本信息*/
    public static final String USER_INFO = BASE_URL + "/users/base_info?access_key=";
    /*关注用户接口*/
    public static final String FOLLOW = BASE_URL + "/following_relationships?access_key=";
    /*取消关注接口*/
    public static final String FOLLOW_CANCEL = BASE_URL + "/following_relationships/cancel?access_key=";
    /*获取已关注的列表*/
    public static final String FOLLOWED_LIST = BASE_URL + "/following_relationships/followeds?access_key=";
    /*获取粉丝列表*/
    public static final String FOLLOWER_LIST = BASE_URL + "/following_relationships/followers?access_key=";
    /*获取精彩推荐*/
    public static final String RECOMMENDATION_LIST = BASE_URL + "/recommendations?";
    /*获取最新推荐*/
    public static final String LATEST_RECOMMENDATION_LIST = BASE_URL + "/recommendations/latest?count=";
    /*上传用户信息接口*/
    public static final String UPLOAD_USER_INFO = BASE_URL + "/users/setting?access_key=";
    /*获取商品列表*/
    public static final String GOODS_LIST = BASE_URL + "/commodities?";
    /*获取商品列表*/
    public static final String GOODS_LIST_SEARCH = BASE_URL + "/commodities/search?";
    /*获取商品列表*/
    public static final String GOODS_DETAIL = BASE_URL + "/commodities/do_view?";
    /*上传商品*/
    public static final String GOODS_UPLOAD = BASE_URL + "/commodities?access_key=";
    /*获取标签*/
    public static final String TAGS = BASE_URL + "/tags?category=";
    /*获取类型（商品类型，图片类型等）*/
    public static final String TYPES = BASE_URL + "/selects/";
    /*获取后期服务列表*/
    public static final String AFTERCASE_LIST = BASE_URL + "/processing_services?";
    /*发布后期服务*/
    public static final String PUBLISH_AFTERCASE = BASE_URL + "/processing_services?access_key=";
    /*获取摄影师列表*/
    public static final String PHOTOGRAPHER_LIST = BASE_URL + "/photography_services?";
    /*发布摄影师*/
    public static final String PUBLISH_PHOTOGRAPHER = BASE_URL + "/photography_services?access_key=";
    /*获取后期服务详情*/
    public static final String AFTERCASE_DETAIL = BASE_URL + "/processing_services/detail?";
    /*获取摄影师详情*/
    public static final String PHOTOGRAPHER_DETAIL = BASE_URL + "/photography_services/detail?";
    /*收藏接口*/
    public static final String COLLECT = BASE_URL + "/collections?access_key=";
    /*取消收藏接口*/
    public static final String COLLECT_CANCEL = BASE_URL + "/collections/cancel?access_key=";
    /*检测是否收藏*/
    public static final String COLLECT_CHECK = BASE_URL + "/collections/check?access_key=";
    /*获取收藏列表*/
    public static final String COLLECTION_LIST = BASE_URL + "/collections?access_key=";
    /*发布视频*/
    public static final String PUBLISH_MV = BASE_URL + "/videos?access_key=";
    /*获取精选视频*/
    public static final String SELECTED_VIDEO_LIST = BASE_URL + "/videos/best?";
    /*获取最热视频*/
    public static final String HOT_VIDEO_LIST = BASE_URL + "/videos/hot?access_key=";
    /*获取视频列表*/
    public static final String VIDEO_LIST = BASE_URL + "/videos?";
    /*获取视频详情*/
    public static final String VIDEO_DETAIL = BASE_URL + "/videos/detail?";
    /*获取图片列表*/
    public static final String PIC_LIST = BASE_URL + "/photos?";
    /*获取图片详情*/
    public static final String PIC_DETAIL = BASE_URL + "/photos/detail?";
    /*创建订单*/
    public static final String ORDER_CREATE = BASE_URL + "/orders?access_key=";
    /*取消订单*/
    public static final String ORDER_CANCEL = BASE_URL + "/orders/cancel_order?access_key=";
    /*账户支付接口*/
    public static final String BALANCE_PAY = BASE_URL + "/orders/buyer_pay?access_key=";
    /*获取订单列表*/
    public static final String ORDER_LIST = BASE_URL + "/orders?";
    /*确认收货*/
    public static final String CONFIRM_ORDER = BASE_URL + "/orders/buyer_receive?access_key=";
    /*订单评论*/
    public static final String ORDER_COMMENT = BASE_URL + "/orders/score_comment?access_key=";
    /*获取评论列表*/
    public static final String LIST_COMMENT = BASE_URL + "/comments?";
    /*添加评论*/
    public static final String COMMENT = BASE_URL + "/comments?access_key=";
    /*喜欢*/
    public static final String LIKE = BASE_URL + "/likes?access_key=";
    /*取消喜欢*/
    public static final String LIKE_CANCEL = BASE_URL + "/likes/cancel?access_key=";
    /*检查是否喜欢*/
    public static final String LIKE_CHECK = BASE_URL + "/likes/check?access_key=";
    /*获取礼物列表*/
    public static final String GIFT_LIST = BASE_URL + "/gifts?";
    /*获取礼物列表（按种类分）*/
    public static final String GIFT_LIST_TYPE = BASE_URL + "/gifts/group_by_category?access_key=";
    /*获取礼物种类(全部，恋爱，萌宠。。。)下对应的item列表*/
    public static final String GIFT_TYPE_LIST = BASE_URL + "/gift_categories?";
    /*获取验证码（找回密码）*/
    public static final String VERIFICATION_CODE_FP = BASE_URL + "/find_password_code";
    /*获取验证码（注册）*/
    public static final String VERIFICATION_CODE_RE = BASE_URL + "/sign_up_code";
    /*修改密码*/
    public static final String PASSWORD_CHANGE = BASE_URL + "/reset_password";
    /*获取关于我们*/
    public static final String ABOUT_US = BASE_URL + "/platforms/about_us";
    /*赠送礼物*/
    public static final String GIFT_SEND = BASE_URL + "/gifts/transfer?access_key=";
    /*赠送礼物（按种类）*/
    public static final String GIFT_SEND_BY_TYPE = BASE_URL + "/gifts/transfer_by_category?access_key=";
    /*用户中心信息*/
    public static final String USER_CNETER_INFO = BASE_URL + "/users/person_center";
    /*意见反馈*/
    public static final String FEEDBACK = BASE_URL + "/suggestions?access_key=";
    /*微信账号信息*/
    public static final String WXINFO = BASE_URL + "/config/wx_pay?access_key=";
    /*获取prepay_id*/
    public static final String PREPAY_ID = BASE_URL + "/deposit_orders/deposit_by_wx?access_key=";
    /*首页轮播*/
    public static final String CAROUSELS_HOME = BASE_URL + "/carousels/home";
    /*视频轮播*/
    public static final String CAROUSELS_VIDOE = BASE_URL + "/carousels/video";
    /*创建直播间*/
    public static final String CREATE_LIVE_ROOM = BASE_URL + "/live_rooms";
    /*打开直播间*/
    public static final String OPEN_LIVE_ROOM = BASE_URL + "/live_rooms/do_open";
    /*关闭直播间*/
    public static final String CLOSE_LIVE_ROOM = BASE_URL + "/live_rooms/do_close";
    /*直播间列表*/
    public static final String LIVE_ROOM_LIST = BASE_URL + "/live_rooms?";
    /*进入直播间*/
    public static final String LIVE_ROOM_JOIN = BASE_URL + "/live_rooms/join?access_key=";
    /*离开直播间*/
    public static final String LIVE_ROOM_LEAVE = BASE_URL + "/live_rooms/leave?access_key=";
    /*登录*/
    public static final String LOGIN = BASE_URL+ "/sign_in";
    /*上传三方平台信息*/
    public static final String UPLOAD_OTHER_PLF_INFO = BASE_URL + "/check_oauth";
    /*注册*/
    public static final String REGISTER = BASE_URL + "/sign_up";
    /*检查是否关注*/
    public static final String FOLLOW_CHECK = BASE_URL + "/following_relationships/check";

}
