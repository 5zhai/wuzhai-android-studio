package com.wuzhai.app.main.widget;

import android.content.Context;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.wuzhai.app.R;

public class AddSubEditView extends LinearLayout implements OnClickListener{

	private ImageView addBtn;
	private EditText editText;
	private ImageView subBtn;
	private int count = 1;
	public AddSubEditView(Context context, AttributeSet attrs) {
		super(context, attrs);
		setupView(context);
	}

	public AddSubEditView(Context context) {
		super(context);
		setupView(context);
	}

	private void setupView(Context context){
		this.setOrientation(LinearLayout.HORIZONTAL);
		setBackgroundResource(R.drawable.addsubview_bg);
		setPadding(2, 2, 2, 2);
		setFocusable(true);
		setFocusableInTouchMode(true);
		initSubView(context);
		addSubView();
	}

	private void initSubView(Context context){
		LayoutParams params = new LayoutParams(0, LayoutParams.MATCH_PARENT);
		params.weight = 1;
		addBtn = new ImageView(context);
		addBtn.setImageResource(R.drawable.icon_add);
		addBtn.setBackgroundColor(getResources().getColor(R.color.common_bg_color));
		addBtn.setLayoutParams(params);
		addBtn.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
		addBtn.setTag(1);
		addBtn.setOnClickListener(this);
		editText = new EditText(context);
		editText.setHint("1");
		editText.setHintTextColor(0xffafb5c1);
		editText.setTextColor(0xff626f85);
		editText.setPadding(1, 1, 1, 1);
		editText.setGravity(Gravity.CENTER);
		editText.setBackgroundResource(R.drawable.addsubview_edit_bg);
		editText.setLayoutParams(params);
		editText.setCursorVisible(false);
		editText.setEnabled(false);
		subBtn = new ImageView(context);
		subBtn.setBackgroundColor(getResources().getColor(R.color.common_bg_color));
		subBtn.setLayoutParams(params);
		subBtn.setImageResource(R.drawable.icon_sub);
		subBtn.setBackgroundColor(getResources().getColor(R.color.common_bg_color));
		subBtn.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
		subBtn.setTag(-1);
		subBtn.setOnClickListener(this);
	}
	private void addSubView(){
		this.addView(subBtn);
		this.addView(editText);
		this.addView(addBtn);
	}

	@Override
	protected void onLayout(boolean changed, int l, int t, int r, int b) {
		super.onLayout(changed, l, t, r, b);
	}

	@Override
	public void onClick(View v) {
		switch ((Integer)v.getTag()){
		case 1:
			editText.setText(++count+"");
			break;
		case -1:
			if(count!=1)
			editText.setText(--count+"");
			break;
		}
	}

	public int getCount(){
		return count;
	}

	public void setOnCountChanged(TextWatcher textWatcher){
		editText.addTextChangedListener(textWatcher);
	}
}
