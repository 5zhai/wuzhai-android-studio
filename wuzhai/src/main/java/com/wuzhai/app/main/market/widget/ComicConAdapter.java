package com.wuzhai.app.main.market.widget;

import java.util.ArrayList;

import android.content.Context;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.wuzhai.app.R;
import com.wuzhai.app.main.widget.HyRecyclerViewAdapter;

public class ComicConAdapter extends HyRecyclerViewAdapter implements OnClickListener {

	private Context context;
	private ArrayList<ComicConNews> comicConNewsList;

	public ComicConAdapter(Context context,ArrayList<ComicConNews> comicConNewsList){
		this.context = context;
		this.comicConNewsList = comicConNewsList;
	}
	public ComicConAdapter(Context context){
		this.context = context;
	}
	@Override
	public int getItemCount() {
		return comicConNewsList.size();
	}

	@Override
	public void onBindViewHolder(ViewHolder arg0, int arg1) {
		CommicConViewHolder viewHolder = (CommicConViewHolder)arg0;
		viewHolder.itemView.setTag(arg1);
		ComicConNews comicConNews = comicConNewsList.get(arg1);
		Picasso.with(context).load(comicConNews.getPicturePath()).into(viewHolder.showPic);
		viewHolder.title.setText(comicConNews.getTitle());
		viewHolder.time.setText(comicConNews.getTime());
		viewHolder.place.setText(comicConNews.getPlace());
	}

	@Override
	public ViewHolder onCreateViewHolder(ViewGroup arg0, int arg1) {
		View view = LayoutInflater.from(context).inflate(R.layout.market_comic_con_item, arg0,false);
		view.setOnClickListener(this);
		return new CommicConViewHolder(view);
	}

	@Override
	public void onClick(View v) {
		if(recyclerViewItemClickListener!=null)
		recyclerViewItemClickListener.onItemClick(v, (Integer)(v.getTag()));
	}

	public void setComicConNewsList(ArrayList<ComicConNews> comicConNewsList){
		this.comicConNewsList = comicConNewsList;
		notifyDataSetChanged();
	}
	private static class CommicConViewHolder extends ViewHolder{

		ImageView showPic;
		TextView title;
		TextView time;
		TextView place;

		public CommicConViewHolder(View arg0) {
			super(arg0);
			showPic = (ImageView)arg0.findViewById(R.id.showPic);
			title = (TextView)arg0.findViewById(R.id.title);
			time = (TextView)arg0.findViewById(R.id.time);
			place = (TextView)arg0.findViewById(R.id.place);
		}
	}
}
